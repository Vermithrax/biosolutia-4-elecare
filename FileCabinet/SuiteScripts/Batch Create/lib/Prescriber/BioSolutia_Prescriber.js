define([ "../UtilityBelt2.0" ], function (utility) {
    /**
     * Contains the functions related to the Prescriber record
     *
     * @exports getPrescriberId
     *
     * @copyright 2018 BioSolutia
     * @author Gerrom V. Infante <gerrom.infante@gmail.com>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     */
    var exports = {};

    function getPrescriberId (data) {
        log.debug({ title: "getPrescriberId", details: "-------------------- Finding Prescriber Record --------------------" });
        log.debug({ title: "getPrescriberId", details: "data:" + JSON.stringify(data) });

        // search for the Prescriber with matching first name, last name and date of birth
        var prescriberSearchResult = [];
        require([ "N/search" ], function (search) {
            var filters = [];
            filters.push(search.createFilter({
                name: "firstname",
                operator: search.Operator.IS,
                values: [ data.prescriber_first ]
            }));
            filters.push(search.createFilter({
                name: "lastname",
                operator: search.Operator.IS,
                values: [ data.prescriber_last ]
            }));
            filters.push(search.createFilter({
                name: "custentity_bio_phys_npi",
                operator: search.Operator.IS,
                values: [ data.prescriber_npi ]
            }));

            var prescriberSearch = search.create({ type: "partner", filters: filters });

            prescriberSearchResult = utility.executeSearch(prescriberSearch);
        });

        return (utility.isEmpty(prescriberSearchResult) === true) ? createPrescriber(data) : prescriberSearchResult[0].id;
    }

    function createPrescriber (data) {
        log.debug({ title: "createPrescriber", details: "-------------------- Creating Prescriber Record --------------------" });
        log.debug({ title: "createPrescriber", details: "data:" + JSON.stringify(data) });

        var prescriber;
        require([ "N/record" ], function (record) {
            prescriber = record.create({
                type: "partner",
                isDynamic: true
            });
        });

        prescriber.setValue({ fieldId: "firstname", value: data.prescriber_first });
        log.debug({ title: "createPrescriber", details: "-------------------- Set firstname --------------------" });
        prescriber.setValue({ fieldId: "lastname", value: data.prescriber_last });
        log.debug({ title: "createPrescriber", details: "-------------------- Set lastname --------------------" });
        prescriber.setValue({ fieldId: "custentity_bio_phys_npi", value: data.prescriber_npi });
        log.debug({ title: "createPrescriber", details: "-------------------- Set custentity_bio_phys_npi --------------------" });
        prescriber.setValue({ fieldId: "fax", value: data.prescriber_fax });
        log.debug({ title: "createPrescriber", details: "-------------------- Set fax --------------------" });
        prescriber.setValue({ fieldId: "phone", value: data.prescriber_phone });
        log.debug({ title: "createPrescriber", details: "-------------------- Set phone --------------------" });

        log.debug({ title: "createPrescriber", details: "-------------------- Creating Address Subrecord --------------------" });
        // Create a line in the Address sublist.
        prescriber.selectNewLine({
            sublistId: "addressbook"
        });

        // Set an optional field on the sublist line.

        prescriber.setCurrentSublistValue({
            sublistId: "addressbook",
            fieldId: "label",
            value: "Primary Address"
        });

        // Create an address subrecord for the line.

        var addressSubRec = prescriber.getCurrentSublistSubrecord({
            sublistId: "addressbook",
            fieldId: "addressbookaddress"
        });

        // Set body fields on the subrecord. Because the script uses
        // dynamic mode, you should set the country value first. The country
        // value determines which address form is to be used, so by setting
        // this value first, you ensure that the values for the rest
        // of the form's fields will be set properly.

        addressSubRec.setValue({
            fieldId: "country",
            value: "US"
        });
        log.debug({ title: "createPrescriber", details: "-------------------- Set Country to US --------------------" });
        addressSubRec.setValue({
            fieldId: "city",
            value: data.prescriber_city
        });
        log.debug({ title: "createPrescriber", details: "-------------------- Set City --------------------" });
        addressSubRec.setValue({
            fieldId: "state",
            value: data.prescriber_state
        });
        log.debug({ title: "createPrescriber", details: "-------------------- Set State --------------------" });
        addressSubRec.setValue({
            fieldId: "zip",
            value: data.prescriber_zip
        });
        log.debug({ title: "createPrescriber", details: "-------------------- Set Zip --------------------" });
        addressSubRec.setValue({
            fieldId: "addr1",
            value: data.prescriber_address
        });
        log.debug({ title: "createPrescriber", details: "-------------------- Set Addr1 --------------------" });
        // Save the sublist line.
        prescriber.commitLine({
            sublistId: "addressbook"
        });

        // contains hard coded values for the prescriber record
        prescriber.setValue({ fieldId: "custentity_bio_prescr_spec", value: "7" }); // Hardcoded to Other

        // save the record
        return prescriber.save();
    }

    exports.getPrescriberId = getPrescriberId;

    return exports;
});
