/**
 * UtilityBelt2.0.js
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 */
define(function () {
    /**
     * This function checks if the passed value is Null or Undefined.
     *
     * @param  {String|Number|Array}  value The value that will be checked.
     * @return {Boolean}       True if value is null or undefined, false if otherwise.
     */
    function nullUndefined (value) {
        if (value === null) {
            return true;
        }
        return value === undefined;
    }

    /**
     * This function removes extra specified character from the left of the String.
     *
     * @param  {String} str   String to process.
     * @param  {String} chars Optional. The character to be removed from the left side.
     *                        Defaults to space if blank.
     * @return {String}       Processed string.
     */
    function leftTrim (str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
    }

    /**
     * This function removes extra specified character from the right of the String.
     *
     * @param  {String} str   String to process.
     * @param  {String} chars Optional. The character to be removed from the right side.
     *                        Defaults to space if blank.
     * @return {String}       Processed string.
     */
    function rightTrim (str, chars) {
        chars = chars || "\\s";
        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
    }

    /**
     * This function removes extra specified character from the left and right of the String.
     *
     * @param  {String} str   String to process.
     * @param  {String} chars Optional. The character to be removed from the left and right side.
     *                        Defaults to space if blank.
     * @return {String}       Processed string.
     */
    function trim (str, chars) {
        return leftTrim(rightTrim(str, chars), chars);
    }

    /**
     * This function executes the NetSuite search.
     *
     * @param  {search} searchObj The NetSuite search object.
     * @return {Array}            Array contain the search result objects.
     */
    function executeSearch (searchObj) {
        var pagedData = searchObj.runPaged({ pageSize: 1000 });

        var searchResult = [];
        pagedData.pageRanges.forEach(function (pageRange) {
            pagedData.fetch({ index: pageRange.index }).data.forEach(function (result) {
                searchResult.push(result);
                return true;
            });

            return true;
        });

        log.debug({
            title: "Execute Search",
            details: "Number of Records Found:=" + searchResult.length
        });

        return searchResult;
    }

    /**
     * This function checks if the string or array is empty.
     *
     * @param  {String|Array}  stValue The value that will be checked.
     * @return {Boolean}         True is the value is empty, false if otherwise.
     */
    function isEmpty (stValue) {
        if (nullUndefined(stValue) === true) {
            return true;
        } else {
            if (isString(stValue) === true) {
                return isBlank(stValue) || (!stValue || stValue.length === 0);
            } else if (isArray(stValue) === true) {
                return (stValue.length === 0);
            }
        }

        return false;
    }

    function forceParseInt (variable, base) {
        base = (isEmpty(base) === true) ? 10 : base;
        var value = !isEmpty(variable) ? parseInt(("" + variable)
            .replace(/[^\d\.-]/gi, ""), base) : 0;

        return !isNaN(value) ? value : 0;
    }

    function forceParseFloat (variable) {
        var value = !isEmpty(variable) ? parseFloat(("" + variable)
            .replace(/[^\d\.-]/gi, "")) : 0;

        return !isNaN(value) ? value : 0;
    }

    /**
     * Wrapper for SS2.0 N/format.format method. Converts a Date object into a date string
     * in the date format specified in the current NetSuite account.
     * For client side scripts, the string returned is based on the user’s system time.
     * For server-side scripts, if you do not provide a timeZone parameter the string returned
     * is based on the system time of the server your NetSuite system is running on.
     *
     * @param  {Date} date       Date object to be converted into date string format
     * @param  {enum} formatType Holds the string values for the supported field types.
     * @param  {enum} timeZone   Holds the valid time zone names in Olson Value, the date will be converted to this time zone.
     *                           If a time zone is not specified, the time zone is set based on the server your NetSuite system is running on.
     *                           If the time zone is invalid, the time zone is set to GMT.
     * @return {string}          The formatted value as a string.
     */
    function dateToString (date, formatType, timeZone) {
        var value = "";

        require(["N/format"], function (format) {
            value = format.format({
                value: date,
                type: formatType,
                timezone: format.Timezone[timeZone]
            });
        });

        return value;
    }

    /**
     * Wrapper for SS2.0 N/format.parse method. Converts a string date into a Date object.
     *
     * @param  {Date} date       Date object to be converted into date string format
     * @param  {enum} formatType Holds the string values for the supported field types.
     * @return {Date}
     */
    function stringToDate (date, formatType) {
        var value = "";

        require(["N/format"], function (format) {
            value = format.parse({
                value: date,
                type: formatType
            });
        });

        return value;
    }

    function closeSuitelet () {
        var html = [];
        html.push("<html>");
        html.push("<head>");
        html.push("<script>");
        html.push("window.close()");
        html.push("</script>");
        html.push("</head>");
        html.push("<body>");
        html.push("</body>");
        html.push("</html>");

        html = html.join("");

        return html;
    }

    /**
     * Converts 'T' or 'F' to its Boolean counterpart
     *
     * @param  {String|Boolean} val 'T' or 'F'
     * @return {Boolean}     true or false
     */
    function toBoolean (val) {
        if (!nullUndefined(val)) {
            if (typeof (val) === "boolean") {
                return val;
            } else {
                if (val === "T") {
                    return true;
                }
            }
        }
        return false;
    }

    function isNumber (value) {
        return typeof value === "number" && isFinite(value);
    }

    function isString (value) {
        return typeof value === "string" || value instanceof String;
    }

    function isArray (value) {
        return value && typeof value === "object" && value.constructor === Array;
    }

    function isBlank (str) {
        return (!str || /^\s*$/.test(str));
    }

    function resultsToJson (results) {
        var jsonResult = [];

        results.forEach(function (result) {
            var columns = result.columns;

            var jsonData = {
                internalid: result.id,
                recordType: result.recordType
            };

            columns.forEach(function (column) {
                // get the data of the column
                var data = {
                    value: result.getValue(column),
                    text: result.getText(column)
                };

                var index = "";
                if (isEmpty(column.join) === false) {
                    index = column.name + "." + column.join;
                } else {
                    index = column.name;
                }

                jsonData[index] = data;
                return true;
            });
            jsonResult.push(jsonData);

            return true;
        });

        return jsonResult;
    }

    var exports = {};

    exports.leftTrim = leftTrim;
    exports.rightTrim = rightTrim;
    exports.trim = trim;
    exports.executeSearch = executeSearch;
    exports.isNullUndefined = nullUndefined;
    exports.isEmpty = isEmpty;
    exports.forceInteger = forceParseInt;
    exports.forceFloat = forceParseFloat;
    exports.dateToString = dateToString;
    exports.stringToDate = stringToDate;
    exports.closeSuitelet = closeSuitelet;
    exports.resultsToJson = resultsToJson;
    exports.toBoolean = toBoolean;
    exports.isNumber = isNumber;
    exports.isArray = isArray;
    exports.isString = isString;
    exports.isBlank = isBlank;
    return exports;
});
