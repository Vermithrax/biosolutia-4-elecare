define(["./UtilityBelt2.0"], function (utility) {
    /**
     * Module Description...
     *
     * @exports encryptField
     * @exports decryptField
     * @exports decryptFile
     *
     * @copyright 2018 BioSolutia
     * @author Gerrom V. Infante <gerrom.infante@gmail.com>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     */
    var exports = {};

    // URL for the REST interface
    const REST_URL = "https://auth.secure.biosolutia.com:8460/webservice/rest/cryptor";

    // HTTP headers sent with the request
    const HEADERS = {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "text/html,text/plain,application/json"
    };

    const CREDENTIALS = {
        "user": "rest_user_1",
        "pass": "a9!Eaf8orBB*jFG$Ki26"
    };

    function createAttachmentDecryptionRequest (contents) {
        var decryptionRequest = {
            "values": contents,
            "configurations": [
                {
                    "type": "Base64DecodeModifier",
                    "options": {
                        "output-data-mode": "binary"
                    }
                },
                {
                    "type": "AESDecryptor"
                },
                {
                    "type": "Base64EncodeModifier",
                    "options": {
                        "output-data-mode": "text"
                    }
                }
            ]
        };

        return decryptionRequest;
    }

    function sendAttachmentDecryptionRequest (configurations) {
        var funcName = "sendToWS";

        // Construct the POST of original value to decrypt, and the operations to perform. Must be JSON format.
        var decPOST = {
            "credentials": CREDENTIALS,
            "crypto": configurations
        };

        // Send POST for decryption
        var response;
        require([ "N/https" ], function (https) {
            response = https.post({ url: REST_URL, body: JSON.stringify(decPOST), headers: HEADERS });
        });

        // Convert the response into a JSON object containing the decrypted values
        var decryptedValue = response.body;

        return decryptedValue;
    }

    function createFieldDecryptionRequest (value, fieldName) {
        // ***SK version
        var SSE = [ "searchable",
            "custpage_patient_lastname",
            "custpage_patient_firstname",
            "company"
        ];
        var FPE = [ "custpage_home_addresss",
            "custpage_home_city",
            "custpage_home_zip"
        ];
        var PH = [ "custpage_primary_phone" ];

        var DOB = [ "custpage_date_of_birth",
            "custpage_dob",
            "custevent_bio_date_of_birth"
        ];
        var AES = [ "custrecord_bio_fax_insur_primary_bpm_id",
            "custrecord_bio_fax_insur_bpm_group",
            "custrecord_bio_fax_insur_id_no",
            "custrecord_bio_fax_insur_grp_no",
            "custpage_medical_id_number",
            "custpage_medical_group_number",
            "custpage_pbm_cardholder_id",
            "custpage_rx_group"
        ];

        var decryptionRequest;

        if (SSE.indexOf(fieldName) > -1) {
            decryptionRequest = {
                "values": value,
                "configurations": [
                    {
                        "type": "EncryptionMatcher"
                    },
                    {
                        "type": "SearchSortDecryptor",
                        "options": {
                            "leak-length": "0"
                        }
                    }
                ]
            };
        } else if (DOB.indexOf(fieldName) > -1) {
            decryptionRequest = {
                "values": value,
                "configurations": [
                    {
                        "type": "DateDecryptor",
                        "options": {
                            "date-format": "MM/dd/YYYY",
                            "epoch-start": "01/01/1800",
                            "epoch-end": "12/31/3000",
                            "translation-offset": "1"
                        }
                    }
                ]
            };
        } else if (AES.indexOf(fieldName) > -1) {
            decryptionRequest = {
                "values": value,
                "configurations": [
                    {
                        "type": "EncryptionMatcher"
                    },
                    {
                        "type": "AESDecryptor"
                    }
                ]
            };
        } else if (PH.indexOf(fieldName) > -1) {
            decryptionRequest = {
                "values": value,
                "alphabets": [
                    {
                        "name": "digit",
                        "categories": [
                            "decimaldigitnumber"
                        ],
                        "blocks": [
                            "basiclatin"
                        ]
                    }
                ],
                "configurations": [
                    {
                        "type": "EncryptionMatcher",
                        "options": {
                            "sentinel": "^"
                        }
                    },
                    {
                        "type": "FormatPreservingDecryptor",
                        "alphabet": "digit",
                        "options": {
                            "algorithm": "ff1",
                            "tweak-length": "0",
                            "sentinel": "^"
                        }
                    }
                ]
            };
        } else {
            decryptionRequest = {
                "values": value,
                "configurations": [
                    {
                        "type": "EncryptionMatcher"
                    },
                    {
                        "type": "FormatPreservingDecryptor",
                        "options": {
                            "algorithm": "ff1",
                            "tweak-length": "2"
                        }
                    }
                ]
            };
        }

        return decryptionRequest;
    }

    function sendFieldDecryptionRequest (configurations) {
        var funcName = "sendFieldDecryptionRequest";

        // Construct the POST of original value to decrypt, and the operations to perform. Must be JSON format.
        var decPOST = {
            "credentials": CREDENTIALS,
            "crypto": configurations
        };

        // Send POST for decryption
        var response;
        require([ "N/https" ], function (https) {
            response = https.post({ url: REST_URL, body: JSON.stringify(decPOST), headers: HEADERS });
        });

        // Convert the response into a JSON object containing the decrypted values
        var decryptedValue = response.body;

        return decryptedValue;
    }

    function createFieldEncryptionRequest (value, fieldName) {
        // ***SK version
        var SSE = [ "lastname", "firstname" ];
        var PH = [ "phone" ];
        var FPE = [ "addr1", "city", "zip" ];
        var DOB = [ "custentity_bio_date_of_birth" ];

        var encryptionRequest;

        if (SSE.indexOf(fieldName) > -1) {
            encryptionRequest = {
                "values": value,
                "configurations": [
                    {
                        "type": "NonEncryptionMatcher"
                    },
                    {
                        "type": "SearchSortEncryptor",
                        "options": {
                            "leak-length": "0"
                        }
                    }
                ]
            };
        } else if (DOB.indexOf(fieldName) > -1) {
            encryptionRequest = {
                "values": value,
                "configurations": [
                    {
                        "type": "DateEncryptor",
                        "options": {
                            "date-format": "MM/dd/YYYY",
                            "epoch-start": "01/01/1800",
                            "epoch-end": "12/31/3000",
                            "translation-offset": "1"
                        }
                    }
                ]
            };
        } else if (PH.indexOf(fieldName) > -1) {
            encryptionRequest = {
                "values": value,
                "alphabets": [
                    {
                        "name": "digit",
                        "categories": [
                            "decimaldigitnumber"
                        ],
                        "blocks": [
                            "basiclatin"
                        ]
                    }
                ],
                "configurations": [
                    {
                        "type": "NonEncryptionMatcher"
                    },
                    {
                        "type": "FormatPreservingEncryptor",
                        "alphabet": "digit",
                        "options": {
                            "algorithm": "ff1",
                            "tweak-length": "0",
                            "sentinel": "^"
                        }
                    }
                ]
            };
        } else {
            encryptionRequest = {
                "values": value,
                "configurations": [
                    {
                        "type": "NonEncryptionMatcher"
                    },
                    {
                        "type": "FormatPreservingEncryptor",
                        "options": {
                            "algorithm": "ff1",
                            "tweak-length": "2"
                        }
                    }
                ]
            };
        }

        return encryptionRequest;
    }

    function sendFieldEncryptionRequest (configurations) {
        // Construct the POST of original value to decrypt, and the operations to perform. Must be JSON format.
        var encPOST = {
            "credentials": CREDENTIALS,
            "crypto": configurations
        };

        // Send POST for decryption
        var response;
        require([ "N/https" ], function (https) {
            response = https.post({ url: REST_URL, body: JSON.stringify(encPOST), headers: HEADERS });
        });

        // Convert the response into a JSON object containing the decrypted values
        var encryptedValue = response.body;

        return encryptedValue;
    }

    function encryptField (field, value) {
        var funcName = "encryptField";

        log.debug({ title: funcName, details: "field:" + field + " | value:" + value });

        var encryptedValue = sendFieldEncryptionRequest(createFieldEncryptionRequest(value, field));
        log.debug({ title: funcName, details: "encryptedValue:" + encryptedValue });

        return (utility.isEmpty(encryptedValue) === true) ? value : encryptedValue;
    }

    function decryptField (value, fieldName) {
        var funcName = "decryptField";

        log.debug({ title: funcName, details: "fieldName:" + fieldName + " | value:" + value });

        var decryptedValue = sendFieldDecryptionRequest(createFieldDecryptionRequest(value, fieldName));
        log.debug({ title: funcName, details: "decryptedValue:" + decryptedValue });

        if (decryptedValue.indexOf("'") === 0) {
            decryptedValue = decryptedValue.substr(1);
        }

        return decryptedValue;
    }

    function decryptFile (contents) {
        return sendAttachmentDecryptionRequest(createAttachmentDecryptionRequest(contents));
    }
    exports.encryptField = encryptField;
    exports.decryptField = decryptField;
    exports.decryptFile = decryptFile;

    return exports;
});
