define(["../Patient/BioSolutia_Patient", "../Prescriber/BioSolutia_Prescriber", "N/runtime", "../UtilityBelt2.0", "../Insurance/BioSolutia_CaseInsurance"], function (patient, prescriber, runtime, utility, caseInsurance) {
    /**
     * Contains the functions related to the Referral record
     *
     * @exports createReferral
     *
     * @copyright 2018 BioSolutia
     * @author Gerrom V. Infante <gerrom.infante@gmail.com>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     */
    var exports = {};

    function createReferral (value) {
        var logTitle = "createReferral";

        // get the values of the patient fields
        var patientData = {
            firstname: value.first_name,
            lastname: value.last_name,
        };

        var patientRecord = patient.getPatientId(patientData);
        log.debug({ title: logTitle, details: "patientRecord:" + patientRecord });

        var referralRecord;
        require(["N/record"], function (record) {
            referralRecord = record.create({
                type: "supportcase"
            });
        });

        log.debug({ title: logTitle, details: "-------------------- Creating Referral record --------------------" });

        referralRecord.setValue({
            fieldId: "company",
            value: patientRecord.id
        });
        referralRecord.setValue({
            fieldId: "custevent_bio_physician",
            value: patientRecord.prescriber
        });
        // TODO: change this to use a custom record for easy maintenance
        referralRecord.setValue({
            fieldId: "custevent_bio_referral_status",
            value: runtime.getCurrentScript().getParameter({ name: "custscript_batch_create_referral_stat" })
        });
        referralRecord.setValue({
            fieldId: "custevent_bio_referral_desc",
            value: runtime.getCurrentScript().getParameter({ name: "custscript_batch_create_referral_desc" })
        });
        referralRecord.setValue({
            fieldId: "title",
            value: runtime.getCurrentScript().getParameter({ name: "custscript_default_referral_subject" })
        });
        referralRecord.setValue({
            fieldId: "status",
            value: runtime.getCurrentScript().getParameter({ name: "custscript_bs_created_status" })
        });
        referralRecord.setValue({
            fieldId: "custeventbio_ref_origin",
            value: runtime.getCurrentScript().getParameter({ name: "custscript_default_referral_origin" })
        });
        referralRecord.setValue({
            fieldId: "custeventbio_ref_info",
            value: runtime.getCurrentScript().getParameter({ name: "custscript_default_referral_info" })
        });
        referralRecord.setValue({
            fieldId: "custeventbio_ref_info",
            value: runtime.getCurrentScript().getParameter({ name: "custscript_default_referral_info" })
        });
        referralRecord.setValue({
            fieldId: "custevent_bio_send_fax_to_physician",
            value: false
        });

        // Hard coded fields are below
        referralRecord.setValue({
            fieldId: "custevent_bio_phys_md_pref_referral",
            value: 2
        }); // Hardcoded to Fax

        // field mapping for fields that are not Date or Select fields
        var referralSimpleField = {
            "Product #": "custeventcustevent_bio_sp_product_ship",
            "Product Code": "custeventbio_disp_tips_sp",
            "Coupon Name": "custevent_bio_promo_code",
        };

        for (var field in referralSimpleField) {
            if (referralSimpleField.hasOwnProperty(field)) {
                log.debug({ title: logTitle, details: "field:" + field });

                referralRecord.setValue({ fieldId: referralSimpleField[field], value: value[field] });
            }
        }

        var referralSelectFields = {
            specialty_pharmacy: "custevent_bio_disp_phcy",
            new_refill: "priority",
            transaction_type: "custevent_bio_refl_transaction_type",
            cap_eligible: "custevent_bio_copay_program_elig",
            withdrawal_code: "custevent_bio_referral_withdrawl_code",
            echeck:"custevent_bio_patient_optin_echeck"
        };

        for (var select in referralSelectFields) {
            if (referralSelectFields.hasOwnProperty(select)) {
                referralRecord.setText({ fieldId: referralSelectFields[select], text: value[select] });

                if (referralSelectFields[select] === "custevent_bio_disp_phcy") {
                    referralRecord.setText({ fieldId: "custevent_bio_refl_transaction_type", text: value[select] });
                }
            }
        }

        var referralDateFields = {
            "Order Date": "custevent_bio_referral_ship_date",
            withdrawal: "custevent_bio_referral_withdrawl_date"
        };

        for (var datefield in referralDateFields) {
            if (referralDateFields.hasOwnProperty(datefield)) {
                // check if value is empty
                if (utility.isEmpty(value[datefield]) === false) {
                    log.debug({
                        title: logTitle,
                        details: "datefield:" + datefield + " | value[datefield]:" + value[datefield]
                    });
                    var dateValue = new Date(value[datefield]);
                    log.debug({ title: logTitle, details: "dateValue:" + dateValue });
                    referralRecord.setValue({ fieldId: referralDateFields[datefield], value: dateValue });
                }
            }
        }
        referralRecord.setValue({ fieldId: "custevent_bio_referral_sp_gen_case", value: true });

        var referralId = referralRecord.save({
            enableSourcing: true,
            ignoreMandatoryFields: true
        });
        log.debug({ title: logTitle, details: "referralId:" + referralId });

        // create the Case Insurance Data
        var caseInsuranceRecord = caseInsurance.createInsurance({
            patient: patientRecord,
            prescriber: prescriberRecord,
            referral: referralId,
            data: caseInsuranceData
        });
        log.debug({ title: logTitle, details: "caseInsuranceRecord:" + caseInsuranceRecord });

        return referralId;
    }

    exports.createReferral = createReferral;

    return exports;
});
