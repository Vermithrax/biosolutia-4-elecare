define([], function () {
    /**
     * Contains the functions related to the Case Insurance record
     *
     * @exports create
     *
     * @copyright 2018 BioSolutia
     * @author Gerrom V. Infante <gerrom.infante@gmail.com>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     */
    var exports = {};

    function createCaseInsurance (params) {
        var logTitle = "createCaseInsurance";

        log.debug({
            title: logTitle,
            details: "-------------------- Creating Case Insurance Record --------------------"
        });
        log.debug({ title: logTitle, details: "params:" + JSON.stringify(params) });

        var caseInsurance;
        require(["N/record"], function (record) {
            caseInsurance = record.create({ type: "customrecord3", isDynamic: true });
        });

        caseInsurance.setValue({ fieldId: "custrecord_bio_fax_patient", value: params.patient });
        caseInsurance.setValue({ fieldId: "custrecord_bio_fax_case", value: params.referral });
        caseInsurance.setValue({ fieldId: "custrecord_bio_fax_physician_id", value: params.prescriber });
        caseInsurance.setValue({ fieldId: "custrecordndc_drug_name", value: 5 });

        var insuranceSimpleField = {
            qty_prescribed: "custrecordnumb_vial",
            days_supply: "custrecorddays_supply",
            cap_id: "custrecord_bio_biopay_copay_id"
        };

        for (var field in insuranceSimpleField) {
            if (insuranceSimpleField.hasOwnProperty(field)) {
                log.debug({ title: logTitle, details: "field:" + field });

                caseInsurance.setValue({ fieldId: insuranceSimpleField[field], value: params.data[field] });
            }
        }

        var insuranceSelectField = {
            pbm_name: "custrecord_bio_fax_insur_primary_bpm", // select
            coverage_determination: "custrecord_bio_cov_det" // Select
        };

        for (var select in insuranceSelectField) {
            if (insuranceSelectField.hasOwnProperty(select)) {
                caseInsurance.setText({ fieldId: insuranceSelectField[select], text: params.data[select] });
            }
        }

        // save the record
        var caseInsuranceId = caseInsurance.save({
            enableSourcing: true,
            ignoreMandatoryFields: true
        });
        log.debug({ title: logTitle, details: "caseInsurance:" + caseInsuranceId });

        return caseInsuranceId;
    }

    exports.createInsurance = createCaseInsurance;
    return exports;
});
