define([ "../BioSolutia_Encryption", "../UtilityBelt2.0", "../moment" ], function (encrypt, utility, moment) {
    /**
     * Contains the functions related to the Patient record
     *
     * @exports getPatientId
     *
     * @copyright 2018 BioSolutia
     * @author Gerrom V. Infante <gerrom.infante@gmail.com>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     */
    var exports = {};

    function getPatientId (data) {
        log.debug({ title: "getPatientId", details: "-------------------- Finding Patient Record --------------------" });
        log.debug({ title: "getPatientId", details: "data:" + JSON.stringify(data) });

        // first encrypt the first name and last name of the patient
        var firstNameEncrypted = encrypt.encryptField("firstname", data["First Name"]);
        var lastNameEncrypted = encrypt.encryptField("lastname", data["Last Name"]);

        // search for the patient with matching first name, last name
        var patientSearchResults = [];
        require([ "N/search" ], function (search) {
            var filters = [];
            filters.push(search.createFilter({
                name: "firstname",
                operator: search.Operator.IS,
                values: [ firstNameEncrypted ]
            }));
            filters.push(search.createFilter({
                name: "lastname",
                operator: search.Operator.IS,
                values: [ lastNameEncrypted ]
            }));

            var searchResults = [];
            searchResults.push(search.createColumn({name:"custevent_bio_physician"}));

            var patientSearch = search.create({ type: "customer", filters: filters, columns: searchResults });

            patientSearchResults = utility.executeSearch(patientSearch);
        });

        // get the internal id of the patient record and the referral
        if (utility.isEmpty(patientSearchResults) === false) {
            var patientData = {
                id: patientSearchResults[0].id,
                prescriber: patientSearchResults[0].getValue({name:"custevent_bio_physician"});
            }
        }

        return patientData;
    }

    exports.getPatientId = getPatientId;
    return exports;
});
