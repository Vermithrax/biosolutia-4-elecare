/**
 * Copyright (c) 2018
 * BioSolutia
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of BioSolutia. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with BioSolutia.
 *
 * Script Name: BioSolutia | MR Batch Create
 *
 * Script Description:
 * This is a map reduce script that will read a CSV file and create the Patient, Prescriber, and Referral record
 *
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *     | Author                      | Date          | Version       | Comments                                                                |
 *     |-----------------------------|---------------|---------------|-------------------------------------------------------------------------|
 *     | Gerrom V. Infante           | Oct 01 2018   | 1.0           | Initial Version                                                         |
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *
 * Deployed:
 *
 * Script Parameters
 *
 *     |---------------------------------------------------------------------------------------------------------------------------------|
 *     | ID                             | Type          | Description                                                                    |
 *     |--------------------------------|---------------|--------------------------------------------------------------------------------|
 *     | custscript_bs_saved_search     | FreeForm Text | Contains the search id of the saved search to be used by the script.           |
 *     | custscript_bs_processed_folder | Integer       | Contains the internal id of the folder where the processed file will be moved. |
 *     |---------------------------------------------------------------------------------------------------------------------------------|
 *
 */
define([ "./lib/UtilityBelt2.0", "./lib/papaparse.min", "./lib/Referral/BioSolutia_Referral", "N/runtime" ], function (utility, papaparse, referral, runtime) {
    /**
     * Module Description...
     *
     * @exports XXX
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType MapReduceScript
     */
    var exports = {};

    function handleErrorAndSendNotification (e, stage) {
        log.error({ title: "handleErrorAndSendNotification", details: "Stage: " + stage + " failed. Error:" + e });

        var author = -5;
        var recipients = runtime.getCurrentScript().getParameter({ name: "custscript_bs_error_email" });
        var subject = "Map/Reduce script " + runtime.getCurrentScript().id + " failed for stage: " + stage;
        var body = "An error occurred with the following information:\n" +
            "Error code: " + e.name + "\n" +
            "Error msg: " + e.message;

        require(["N/email"], function (email) {
            email.send({
                author: author,
                recipients: recipients,
                subject: subject,
                body: body
            });
        });
    }

    function handleErrorIfAny (summary) {
        log.error({ title: "handleErrorIfAny", details: "summary: " + JSON.stringify(summary) });

        var inputSummary = summary.inputSummary;
        var mapSummary = summary.mapSummary;
        var reduceSummary = summary.reduceSummary;

        if (inputSummary.error) {
            var e;
            require(["N/error"], function (error) {
                e = error.create({
                    name: "INPUT_STAGE_FAILED",
                    message: inputSummary.error
                });
            });

            handleErrorAndSendNotification(e, "getInputData");
        }

        handleErrorInStage("map", mapSummary);
        handleErrorInStage("reduce", reduceSummary);
    }

    /**
     * This function handles any error happening in the stage
     *
     * @param stage
     * @param summary
     */
    function handleErrorInStage (stage, summary) {
        log.error({ title: "handleErrorInStage", details: "Stage: " + stage + " | summary: " + JSON.stringify(summary) });

        var errorMsg = [];
        summary.errors.iterator().each(function (key, value) {
            var msg = "Failure to referral.  Row Id: " + key + ". Error was: " + JSON.parse(value).message + "\n";
            errorMsg.push(msg);
            return true;
        });
        if (errorMsg.length > 0) {
            var e;
            require(["N/error"], function (error) {
                e = error.create({
                    name: "RECORD_CREATE_FAILED",
                    message: JSON.stringify(errorMsg)
                });
            });
            handleErrorAndSendNotification(e, stage);
        }
    }

    // ------------------- Start of Entry Point Functions -------------------
    /**
     * <code>getInputData</code> event handler
     *
     * @governance 10,000
     *
     * @return {*[]|Object|Search|ObjectRef} Data that will be used as input for
     *         the subsequent <code>reduce</code> or <code>reduce</code>
     *
     * @static
     * @function getInputData
     */
    function getInputData () {
        var logTitle = "getInputData";

        log.audit({ title: logTitle, details: "==================== getInputData Start ====================" });

        /**
         * @type search.Search
         */
        var recordsToCreate;
        require([ "N/search"], function (search) {
            var useSearch = runtime.getCurrentScript().getParameter({ name: "custscript_bs_saved_search" });

            if (utility.isEmpty(useSearch) === false) {
                recordsToCreate = search.load({
                    id: useSearch
                });
            }
        }); // end of require(['N/search'], function(search)

        var filesToProces = utility.executeSearch(recordsToCreate);

        // there should be only 1 file to be processed
        if (utility.isEmpty(filesToProces) === false) {
            var csvFile = filesToProces[ 0 ];

            // open the csv file and convert to object with papaparse
            var batchFile;
            require([ "N/file" ], function (file) {
                batchFile = file.load({ id: csvFile.id });
            });
            var batchFileCSV = batchFile.getContents();

            // Parse CSV string
            var dataObject = papaparse.parse(batchFileCSV, {
                quoteChar: "\"",
                escapeChar: "\"",
                header: true,
                trimHeaders: true,
                dynamicTyping: true,
                skipEmptyLines: "greedy"
            });

            // move the file to the processed folder
            batchFile.folder = runtime.getCurrentScript().getParameter({ name: "custscript_bs_processed_folder" });
            // batchFile.save();
        }

        log.debug({ title: logTitle, details: "dataObject:" + JSON.stringify(dataObject) });

        log.audit({ title: logTitle, details: "==================== getInputData End ====================" });

        return dataObject.data;
    }

    /**
     * <code>reduce</code> event handler
     *
     * @governance 5,000
     *
     * @param context
     *        {MapContext} Data collection containing the key/value pairs to
     *            process through the reduce stage
     *
     * @return {void}
     *
     * @static
     * @function reduce
     */
    function reduce (context) {
        var logTitle = "reduce";

        log.audit({ title: logTitle, details: "==================== reduce Start ====================" });

        log.debug({ title: logTitle, details: "context.values.length:" + context.values.length });

        // since context.values is an array of stringified object, convert those to objects first
        var values = context.values.map(function (value) {
            return JSON.parse(value);
        });
        log.debug({ title: logTitle, details: "values:" + JSON.stringify(values) });

        var referralId = "";
        values.forEach(function (value) {
            referralId = referral.createReferral(value);
            return true;
        });

        log.audit({ title: logTitle, details: "==================== reduce End ====================" });

        context.write({
            key: referralId,
            value: referralId
        });
    }

    /**
     * <code>summarize</code> event handler
     *
     * @governance 10,000
     *
     * @param summary
     *        {Summary} Holds statistics regarding the execution of a reduce/reduce
     *            script
     *
     * @return {void}
     *
     * @static
     * @function summarize
     */
    function summarize (summary) {
        var logTitle = "summary";

        log.audit({ title: logTitle, details: "==================== summary Start ====================" });

        handleErrorIfAny(summary);

        log.audit({ title: logTitle, details: "==================== summary End ====================" });
    }

    exports.getInputData = getInputData;
    exports.reduce = reduce;
    exports.summarize = summarize;
    return exports;
});
