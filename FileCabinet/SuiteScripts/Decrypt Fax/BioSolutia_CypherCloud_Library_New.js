/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               | 
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |   
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             | 
 * |=======================================================================================================================|
 * | NAME:                                                                                                                 |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * |                                                                                                                       |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           | 
 * | Gerrom Infante     	Mar 24, 2014	1.0			                                                                   | 
 * |_______________________________________________________________________________________________________________________|
 */
 
  // URL for the REST interface
const REST_URL = 'https://auth.secure.biosolutia.com:8460/webservice/rest/cryptor';
	
// HTTP headers sent with the request
const HEADERS = {
	"Content-Type": "application/json; charset=utf-8",
	"Accept": "text/html,text/plain,application/json"
};

const CREDENTIALS = {
	"user": "rest_user_1",
	"pass": "a9!Eaf8orBB*jFG$Ki26"
}

function createAttachmentDecryptionRequest (contents) {

	/*var soapMessage = [];
	soapMessage.push('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dpa="http://ws.ciphercloud.com/dpaas/">');
	soapMessage.push('<soapenv:Header/>');
	soapMessage.push('<soapenv:Body>');
	soapMessage.push('<dpa:fileRequest>');
	soapMessage.push('<sessionId></sessionId>');
	soapMessage.push('<serverUrl></serverUrl>');
	soapMessage.push('<fileOptions>');
	soapMessage.push(['<base64Content>', contents, '</base64Content>'].join(''));
	soapMessage.push('<operation>DECRYPT</operation>');
	soapMessage.push('</fileOptions>');
	soapMessage.push('<applicationName>bioprod1</applicationName>');
	soapMessage.push('<appKey>netsuite</appKey>');
	soapMessage.push('</dpa:fileRequest>');
	soapMessage.push('</soapenv:Body>');
	soapMessage.push('</soapenv:Envelope>');

	return soapMessage.join('');*/
	
	var decryptionRequest = {
		"values":contents,
		"configurations": [
			{
				"type": "Base64DecodeModifier",
				"options": {
					"output-data-mode": "binary"
				}		
			},
			{
				"type": "AESDecryptor"
			},
			{
        		"type": "Base64EncodeModifier",
				"options": {
					"output-data-mode": "text"
				}
        	}
		]
	};
	
	return decryptionRequest;
}
function sendAttachmentDecryptionRequest (webServiceURL , configurations) {

	var funcName = 'sendToWS';

	/*try {
		var headers = [];
		headers['Content-Type'] = "text/plain ;charset=UTF-8";

		var response = nlapiRequestURL(webServiceURL, soapMessage);

		var body = response.getBody();

		if (isEmpty(body) === true) {
			throw nlapiCreateError('99999', 'Empty response from CypherCloud');
		}

		var responseXml = nlapiStringToXML(body);

		// get response values
		var data = nlapiSelectValue(responseXml, '//*[name()=\'base64Content\']');

		return data;
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}*/
	
	// Construct the POST of original value to decrypt, and the operations to perform. Must be JSON format.
	var decPOST = {
		"credentials": CREDENTIALS,
		"crypto": configurations
	};
	
	try {
		// Send POST for decryption
		//scriptLogger_global.debug('sendAttachmentDecryptionRequest', ['PRE RequestURL: ', JSON.stringify(decPOST)].join(''));
		var response = nlapiRequestURL(REST_URL, JSON.stringify(decPOST), HEADERS, 'POST');

		//scriptLogger_global.debug('sendAttachmentDecryptionRequest', ['PRE DecryptVal'].join(''));
		// Convert the response into a JSON object containing the decrypted values
		var decryptedValue = response.getBody();
		
		//scriptLogger_global.debug('sendAttachmentDecryptionRequest', ['POST DecryptVal: ', decryptedValue].join(''));
		
		return decryptedValue;
		
	} catch (error) {
		//scriptLogger_global.debug('sendAttachmentDecryptionRequest', ['catch start '].join(''));
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function createFieldDecryptionRequest (value, fieldName) {

	/*var soapMessage = [];
	 soapMessage.push('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dpa="http://ws.ciphercloud.com/dpaas/">');
	 soapMessage.push('<soapenv:Header/>');
	 soapMessage.push('<soapenv:Body>');
	 soapMessage.push('<dpa:detokenizeRequest>');
	 soapMessage.push('<sessionId></sessionId>');
	 soapMessage.push('<serverUrl></serverUrl>');
	 soapMessage.push('<appKey>netsuite</appKey>');
	 soapMessage.push('<applicationName>bioprod1</applicationName>');
	 soapMessage.push(["<cipherTextValue><![CDATA[", data, "]]></cipherTextValue>"].join(''));
	 soapMessage.push('</dpa:detokenizeRequest>');
	 soapMessage.push('</soapenv:Body>');
	 soapMessage.push('</soapenv:Envelope>');

	 return soapMessage.join('');*/

	// ***SK version
	var SSE = [	'searchable',
		'custpage_patient_lastname',
		'custpage_patient_firstname',
		'company'
	];
	var FPE = [	'custpage_home_addresss',
		'custpage_home_city',
		'custpage_home_zip'
	];
	var PH = ['custpage_primary_phone',
		'custpage_home_phone'];

	var DOB = [	'custpage_date_of_birth',
		'custpage_dob',
		'custevent_bio_date_of_birth'
	];
	var AES = [	'custrecord_bio_fax_insur_primary_bpm_id',
		'custrecord_bio_fax_insur_bpm_group',
		'custrecord_bio_fax_insur_id_no',
		'custrecord_bio_fax_insur_grp_no',
		'custpage_medical_id_number',
		'custpage_medical_group_number',
		'custpage_pbm_cardholder_id',
		'custpage_rx_group'
	];
	var EMAIL = [ 'custpage_email' ];

	var decryptionRequest;

	if (SSE.indexOf(fieldName) > -1) {
		decryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "EncryptionMatcher"
				},
				{
					"type": "SearchSortDecryptor",
					"options": {
						"leak-length": "0"
					}
				}
			]
		};
	} else if (DOB.indexOf(fieldName) > -1) {
		decryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "DateDecryptor",
					"options": {
						"date-format": "MM/dd/YYYY",
						"epoch-start": "01/01/1800",
						"epoch-end": "12/31/3000",
						"translation-offset": "1"
					}
				}
			]
		};
	} else if (AES.indexOf(fieldName) > -1) {
		decryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "EncryptionMatcher"
				},
				{
					"type": "AESDecryptor"
				}
			]
		};
	} else if (PH.indexOf(fieldName) > -1) {
		decryptionRequest = {
			"values":value,
			"alphabets": [
				{
					"name": "digit",
					"categories": [
						"decimaldigitnumber"
					],
					"blocks": [
						"basiclatin"
					]
				}
			],
			"configurations": [
				{
					"type": "EncryptionMatcher",
					"options": {
						"sentinel": "^"
					}
				},
				{
					"type": "FormatPreservingDecryptor",
					"alphabet": "digit",
					"options": {
						"algorithm": "ff1",
						"tweak-length": "0",
						"sentinel": "^"
					}
				}
			]
		};
	} else if (EMAIL.indexOf(fieldName) > -1) {
		decryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "EncryptionMatcher",
					"options": {
						"sentinel": "-q.xx"
					}
				},
				{
					"type": "FormatPreservingDecryptor",
					"options": {
						"algorithm": "ff1",
						"sentinel": "-q.xx"
					}
				}
			]
		};
	} else {
		decryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "EncryptionMatcher"
				},
				{
					"type": "FormatPreservingDecryptor",
					"options": {
						"algorithm": "ff1",
						"tweak-length": "2"
					}
				}
			]
		};
	}

	return decryptionRequest;
}
function sendFieldDecryptionRequest (webServiceURL , configurations) {

	var funcName = 'sendFieldDecryptionRequest';
/*
	try {
		var headers = [];
		headers['Content-Type'] = "text/plain ;charset=UTF-8";

		var response = nlapiRequestURL(webServiceURL, soapMessage, headers);

		var body = response.getBody();

		if (isEmpty(body) === true) {
			throw nlapiCreateError('99999', 'Empty response from CypherCloud');
		}

		nlapiLogExecution('DEBUG', funcName, ['DPASS Response:= ', nlapiEscapeXML(body)].join(''));

		var responseXml = nlapiStringToXML(body);

		// get response values
		var data = nlapiSelectValue(responseXml, '//*[name()=\'clearTextValue\']');

		return data;
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
*/

	// Construct the POST of original value to decrypt, and the operations to perform. Must be JSON format.
	var decPOST = {
		"credentials": CREDENTIALS,
		"crypto": configurations
	};
	
	try {
		// Send POST for decryption
		nlapiLogExecution('DEBUG', funcName, ['PRE URL post:= ', JSON.stringify(decPOST)].join(''));
		var response = nlapiRequestURL(REST_URL, JSON.stringify(decPOST), HEADERS, 'POST');

		nlapiLogExecution('DEBUG', funcName, ['POST URL post:= ', response].join(''));
		// Convert the response into a JSON object containing the decrypted values
		var decryptedValue = response.getBody();
		
		nlapiLogExecution('DEBUG', funcName, ['POST decr:= ', decryptedValue].join(''));
		
		return decryptedValue;
		
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
function createFieldEncryptionRequest (value , fieldName) {

	/*var soapMessage = [];
	soapMessage.push('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:dpa="http://ws.ciphercloud.com/dpaas/">');
	soapMessage.push('<soapenv:Header/>');
	soapMessage.push('<soapenv:Body>');
	soapMessage.push('<dpa:tokenizeRequest>');
	soapMessage.push('<field>');
	soapMessage.push('<appKey>netsuite</appKey>');
	soapMessage.push('<applicationName>bioprod1</applicationName>');
	soapMessage.push('<entityName>Acccount</entityName>');
	soapMessage.push(['<fieldName>', fieldName, '</fieldName>'].join(''));
	soapMessage.push(['<clearTextValue>', value, '</clearTextValue>'].join(''));
	soapMessage.push('</field>');
	soapMessage.push('</dpa:tokenizeRequest>');
	soapMessage.push('</soapenv:Body>');
	soapMessage.push('</soapenv:Envelope>');

	return soapMessage.join('');
	*/

	// ***SK version
	var SSE = ['lastname', 'firstname'];
	var PH = ['phone'];
	var FPE = ['addr1', 'city', 'zip'];
	var DOB = ['custentity_bio_date_of_birth'];
	 
	var encryptionRequest;
	
	if (SSE.indexOf(fieldName) > -1) {
	 	encryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "NonEncryptionMatcher"
				},
				{
					"type": "SearchSortEncryptor",
					"options": {
						"leak-length": "0"
					}
				}
			]
		};
	} else if (DOB.indexOf(fieldName) > -1) {
		encryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "DateEncryptor",
					"options": {
						"date-format": "MM/dd/YYYY",
						"epoch-start": "01/01/1800",
						"epoch-end": "12/31/3000",
						"translation-offset": "1"
					}
				}
			]
		};
	} else if (PH.indexOf(fieldName) > -1) {
		encryptionRequest = {
			"values":value,
			"alphabets": [ 
				{
					"name": "digit",
					"categories": [
						"decimaldigitnumber"
					],
					"blocks": [
						"basiclatin"
					]
				}
			],
			"configurations": [
				{
					"type": "NonEncryptionMatcher"
				},
				{
					"type": "FormatPreservingEncryptor",
					"alphabet": "digit",
					"options": {
						"algorithm": "ff1",
						"tweak-length": "0",
						"sentinel": "^"
					}
				}
			]
		};
	} else {
		encryptionRequest = {
			"values":value,
			"configurations": [
				{
					"type": "NonEncryptionMatcher"
				},
				{
					"type": "FormatPreservingEncryptor",
					"options": {
						"algorithm": "ff1",
						"tweak-length": "2"
					}
				}
			]
		};
	}

	return encryptionRequest;
}
function sendFieldEncryptionRequest (webServiceURL , configurations) {

	var funcName = 'sendFieldEncryptionRequest';

	/*try {
		var headers = [];
		headers['Content-Type'] = "text/plain ;charset=UTF-8";

		var response = nlapiRequestURL(webServiceURL, soapMessage);

		var body = response.getBody();

		if (isEmpty(body) === true) {
			throw nlapiCreateError('99999', 'Empty response from CypherCloud');
		}

		var responseXml = nlapiStringToXML(body);
		//		nlapiLogExecution('Debug', funcName, ['Data:= ', nlapiEscapeXML(body)].join(''));

		// get response values
		var data = nlapiSelectValue(responseXml, '//*[name()=\'cipherTextValue\']');
		//		nlapiLogExecution('Debug', funcName, ['Data 2:= ', nlapiEscapeXML(data)].join(''));

		var regex = /<!\[CDATA\[(.*?)\]\]>/i;

		var encrypted = regex.exec(data);
		//		nlapiLogExecution('Debug', funcName, ['Data 3:= ', JSON.stringify(encrypted)].join(''));

		if ( (encrypted.length == 2) && (isNullOrUndefined(encrypted) === false)) {
			return encrypted[1];
		}

	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
	*/
	
	// Construct the POST of original value to decrypt, and the operations to perform. Must be JSON format.
	var encPOST = {
		"credentials": CREDENTIALS,
		"crypto": configurations
	};
	
	try {
		// Send POST for encryption
		var response = nlapiRequestURL(REST_URL, JSON.stringify(encPOST), HEADERS, 'POST');

		// Convert the response into a JSON object containing the decrypted values
		var encryptedValue = response.getBody();
		
		return encryptedValue;
		
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
function isEmpty (stValue) {

	if (isNullOrUndefined(stValue) === true) {
		return true;
	}

	if ( (typeof stValue != 'string') && (getObjectName(stValue) != 'String')) {
		throw nlapiCreateError('10000', 'isEmpty should be passed a string value.  The data type passed is ' + typeof stValue + ' whose class name is ' + getObjectName(stValue));
	}

	if (stValue.length == 0) {
		return true;
	}

	return false;
}
function isNullOrUndefined (value) {

	if (value === null) {
		return true;
	}

	if (value === undefined) {
		return true;
	}

	return false;
}
function getObjectName (object) {

	if (isNullOrUndefined(object) === true) {
		return object;
	}

	return /(\w+)\(/.exec(object.constructor.toString())[1];
}
function numRows (obj) {

	var ctr = 0;
	for ( var k in obj) {
		if (obj.hasOwnProperty(k)) {
			ctr++;
		}
	}
	return ctr;
}
