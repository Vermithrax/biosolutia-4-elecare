function ScriptLogger(){var bEnableDebug=false;this.enableDebug=function Logger_enableDebug(){bEnableDebug=true;};this.disableDebug=function Logger_disableDebug(){bEnableDebug=false;};function log(stType,stTitle,stMessage){if(isEmpty(stType)){throw nlapiCreateError('ERROR','Logging Error','No Log Type Defined');}
stType=new String(stType);if(trim(stType)==='DEBUG'){if(!bEnableDebug){return;}}
if(typeof nlapiLogExecution==='undefined'){alert(stType+' : '+stTitle+' : '+stMessage);}else{nlapiLogExecution(stType,stTitle,stMessage);}}
this.debug=function Logger_debug(stTitle,stMessage){log('DEBUG',stTitle,stMessage);};this.audit=function Logger_audit(stTitle,stMessage){log('AUDIT',stTitle,stMessage);};this.error=function Logger_error(stTitle,stMessage){log('ERROR',stTitle,stMessage);};this.emergency=function Logger_emergency(stTitle,stMessage){log('EMERGENCY',stTitle,stMessage);};this.console=function Logger_console(stTitle,stMessage){var message=['Function:= ',stTitle,'\n',stMessage].join('');console.log(message);};function isEmpty(stValue){if(isNullOrUndefined(stValue)===true){return true;}
if(stValue.length==0){return true;}
return false;}
function isNullOrUndefined(value){if(value===null){return true;}
if(value===undefined){return true;}
return false;}
function leftTrim(str,chars){chars=chars||"\\s";return str.replace(new RegExp("^["+chars+"]+","g"),"");}
function rightTrim(str,chars){chars=chars||"\\s";return str.replace(new RegExp("["+chars+"]+$","g"),"");}
function trim(str,chars){return leftTrim(rightTrim(str,chars),chars);}}