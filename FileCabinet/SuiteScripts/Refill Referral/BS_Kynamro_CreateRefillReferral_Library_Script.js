/**
 * @type UtilityBelt()
 */
var utility = new UtilityBelt();
/**
 * @type ScriptLogger
 */
var logger = new ScriptLogger();
/**
 * @type nlobjContext
 */
var context = nlapiGetContext();

/* get the parameter values */
var default_status = context.getSetting('script', 'custscript_default_status');
var blank_fields = context.getSetting('script', 'custscript_blank_fields');
var checkbox_fields = context.getSetting('script', 'custscript_checkbox');

blank_fields = (utility.isEmpty(blank_fields) === false) ? blank_fields.split(',') : '';
checkbox_fields = (utility.isEmpty(checkbox_fields) === false) ? checkbox_fields.split(',') : '';

function create_refill_referral(case_id, patient_id) {

    var log_title = 'create_refill_referral';

    logger.audit(log_title, 'Original Referral Internal Id:= ' + case_id);

    /**
     * @type nlobjRecord
     * @record supportcase
     */
    var referral_copy = nlapiCopyRecord('supportcase', case_id);

    /* set the referral status to the default value */
    referral_copy.setFieldValue('custevent_bio_referral_status', default_status);
    referral_copy.setFieldValue( "custevent_bio_send_fax_to_physician", "F");
    // get the refills of the original
    var refills_remaining = referral_copy.getFieldText('custevent_bio_refills_remaining');

    if (refills_remaining > 0) {
        // reduce by 1
        refills_remaining = utility.forceParseInt(refills_remaining, 10) - 1;
    }

    // var refill = 0;
    referral_copy.setFieldText('custevent_bio_refills_remaining', refills_remaining.toString(10));

    referral_copy.setFieldValue('title', 'REFILL');
    referral_copy.setFieldText('status', 'In Progress');
    referral_copy.setFieldText('priority', 'Refill');

    /* change date to the current date */
    var current_date = new Date();
    logger.debug(log_title, 'Server Date:= ' + current_date);

    referral_copy.setDateTimeValue('custevent_bs_convert_date_time', nlapiDateToString(current_date, 'datetimetz'), '5');

    current_date = referral_copy.getFieldValue('custevent_bs_convert_date_time');
    logger.debug(log_title, 'Date in Eastern:= ' + current_date);

    current_date = current_date.split(' ');

    var day = current_date[0];
    var current_time = current_date[1];

    current_time = current_time.split(':');

    var time = current_time[0] + ':' + current_time[1] + ' ' + current_date[2];

    logger.debug(log_title, 'Day:= ' + day + ' | Time:= ' + time);

    referral_copy.setFieldValue('startdate', day);
    referral_copy.setFieldValue('starttime', time);

    /* blank the other fields */
    logger.debug(log_title, ['Blank Fields:= ', blank_fields.toString()].join(''));
    var field;
    for (var x = 0; x < blank_fields.length; x++) {
        field = blank_fields[x];

        referral_copy.setFieldValue(field, null);
    }

    logger.debug(log_title, ['Checkbox Fields:= ', checkbox_fields.toString()].join(''));
    var field;
    for (var x = 0; x < checkbox_fields.length; x++) {
        field = checkbox_fields[x];

        referral_copy.setFieldValue(field, 'F');
    }

    var copy_case = nlapiSubmitRecord(referral_copy, true, true);
    logger.audit(log_title, 'Refill Referral Internal Id:= ' + copy_case);

    /* copy Case Insurance Data */
    copy_case_insurance(case_id, patient_id, copy_case);

    return copy_case;

}

function get_refill_count(patient) {

    var log_title = 'get_refill_count';

    /**
     * @type SearchObject
     */
    var search = new SearchObject();

    search.setType('supportcase');
    search.addFilter('internalid', 'company', 'anyof', patient);
    search.addFilter('priority', null, 'anyof', '2');  // Look for Referral Type = Refill
    search.addFilter('isinactive', null, 'is', 'F');

    search.addColumn('custevent_bio_refills_remaining');
    search.addColumn('internalid');

    search.setSort(1, true);

    var results = search.execute();

    var refill = 0;
    if (results.length > 0) {
        var json_results = utility.resultsToJSON(results);
        logger.debug(log_title, ['Referral Data:= ', JSON.stringify(json_results)].join(''));

        /* get only the first result */
        var value = json_results[0];

        refill = utility.forceParseInt(value['custevent_bio_refills_remaining']['text'], 10) - 1;

        if (refill < 0) {
            refill = 0;
        }
    }
    logger.debug(log_title, ['Refill:= ', refill].join(''));

    refill = (refill == 0) ? '13' : refill;

    return refill + '';
}

function copy_case_insurance(case_id, patient_id, copy_case) {

    var log_title = 'copy_case_insurance';

    /**
     * @type SearchObject
     */
    var search = new SearchObject();

    search.setType('customrecord3');
    search.addFilter('custrecord_bio_fax_patient', null, 'anyof', patient_id);
    search.addFilter('custrecord_bio_fax_case', null, 'anyof', case_id);
    search.addFilter('isinactive', null, 'is', 'F');

    search.addColumn('internalid');

    var results = search.execute();

    var internal_id = 0;
    if (results.length > 0) {
        var json_results = utility.resultsToJSON(results);
        logger.debug(log_title, ['Case Insurance Data:= ', JSON.stringify(json_results)].join(''));

        /* get only the first result */
        var value = json_results[0];

        internal_id = value['internalid']['value'];
        logger.audit(log_title, 'Refill Case - Case Insurance Data Original Internal Id:= ' + internal_id);

        /**
         * @type nlobjRecord
         * @record customrecord3
         */
        var case_copy = nlapiCopyRecord('customrecord3', internal_id);

        case_copy.setFieldValue('custrecord_bio_fax_case', copy_case);

        var id = nlapiSubmitRecord(case_copy, true, true);
        logger.audit(log_title, 'Refill Case - Case Insurance Data Copy Internal Id:= ' + id);
    }
}