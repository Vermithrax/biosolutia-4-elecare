/**
 * Copyright (c) 2017
 * BioSolutia Inc
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of BioSolutia Inc. ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with BioSolutia Inc.
 *
 * Script Name: BioSolutia|Batch Refill Suitelet
 *
 * Script Description:
 * This script creates a shortcut in the dashboard that allows the user to run a map reduce script on demand.
 *
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *     | Author                      | Date          | Version       | Comments                                                                |
 *     |-----------------------------|---------------|---------------|-------------------------------------------------------------------------|
 *     | Gerrom V. Infante           | Oct 24 2017   | 1.0           | Initial Version                                                         |
 *     |---------------------------------------------------------------------------------------------------------------------------------------|
 *
 * Deployed to: Lists > Support > Run Batch Refill
 *
 * Script Parameters
 *
 *     |-------------------------------------------------------------------------------------------------------------|
 *     | ID                                   | Type           | Description                                         |
 *     |--------------------------------------|----------------|-----------------------------------------------------|
 *     | custscript_boss_batch_refills_script | Free-Form Text | Contains the script id of the Map Reduce Script     |
 *     | custscript_boss_batch_refills_deploy | Free-Form Text | Contains the deployment id of the Map Reduce Script |
 *     |-------------------------------------------------------------------------------------------------------------|
 *
 */
/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NScriptName BioSolutia|Batch Refill Suitelet
 * @NScriptId customscript_boss_batch_refills_sl
 * @NModuleScope SameAccount
 */
define(['N/task', 'N/runtime', 'N/redirect', './lib/UtilityBelt2.0'], function (task, runtime, redirect, utility) {
     var entryPoint = {};

     /**
      * <code>onRequest</code> event handler
      *
      * @governance XXX
      *
      * @param context
      *        {Object}
      * @param context.request
      *        {ServerRequest} The incoming request object
      * @param context.response
      *        {ServerResponse} The outgoing response object
      *
      * @return {void}
      *
      * @static
      * @function on_request
      */
     function on_request(context) {
          var logTitle = 'BioSolutia|Batch Refill Suitelet.on_request';

          log.audit({
               title: logTitle,
               details: 'Request received...'
          });

          handle_get(context);
     }

     function handle_get(context) {
          var logTitle = 'BioSolutia|Batch Refill Suitelet.handle_get';

          log.audit({
               title: logTitle,
               details: 'Processing GET request...'
          });

          var mapReduceScript = runtime.getCurrentScript()
               .getParameter({
                    name: 'custscript_boss_batch_refills_script'
               });

          var mapReduceDeploy = runtime.getCurrentScript()
               .getParameter({
                    name: 'custscript_boss_batch_refills_deploy'
               });

          log.debug({
               title: logTitle,
               details: 'mapReduceScript:= ' + mapReduceScript + ' >>> mapReduceDeploy:= ' + mapReduceDeploy
          });

          var mrTask = task.create({
               taskType: task.TaskType.MAP_REDUCE
          });

          mrTask.scriptId = mapReduceScript;
          mrTask.deploymentId = mapReduceDeploy;
          var mrTaskId = mrTask.submit();

          log.audit({
               title: logTitle,
               details: 'Showing status page'
          });

          redirect.toTaskLink({
               id: 'LIST_MAPREDUCESCRIPTSTATUS',
               parameters: {
                    'daterange': 'CUSTOM',
                    'datefrom': utility.date_to_string(new Date(), 'date')
               }
          });
     }

     entryPoint.onRequest = on_request;
     return entryPoint;
});
