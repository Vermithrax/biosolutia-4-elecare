/* |=======================================================================================================================|
 * | NAME: BioSolutia|Create Refill Button                                                                                 |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * | This script creates the custom button "Create Refill" on the Referral (Case) record.                                  |                                                                                    |
 * | ______________________________________________________________________________________________________________________|
 * | Author              Date           Version     Comments                                                               | 
 * | Gerrom V. Infante   Apr 25 2016    1.0                                                                                | 
 * |=======================================================================================================================|
 */
var refill_button = new function() {
     
     var errorHandling = {
          'script_name' : 'BioSolutia|Create Refill Button',
          'from_email' : '-5', /* Default Administrator */
          'to_email' : 'gerrom.infante@gmail.com',
          'script_file' : 'BS_Kynamro_CreateRefillButton_UserEvent.js'
     };
     
     /**
      * @type UtilityBelt()
      */
     var utility = new UtilityBelt();
     /**
      * @type ScriptLogger
      */
     var logger = new ScriptLogger();
     /**
      * @type nlobjContext
      */
     var context = nlapiGetContext();
     
     return {
          /**
           * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
           * 
           * @appliedtorecord supportcase
           * @param {String}
           *             type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF)
           *             markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
           * @param {nlobjForm}
           *             form an nlobjForm object representing the current form
           * @param {nlobjRequest}
           *             request an nlobjRequest object representing the GET request (Only available for browser requests.)
           * @returns {Void}
           */
          before_load : function(type, form, request) {
               
               var logTitle = 'refill_button.before_load';
               
               // enable debug statements if log level is not Debug
               if (context.getLogLevel() == 'DEBUG') {
                    logger.enableDebug();
               }
               
               try {
                    logger.audit(logTitle, '==================== BEFORE LOAD EVENT START ====================');
                    
                    if (type == 'view') {
                         
                         /* check if referral is a refill referral */
                         var refill = context.getSetting('script', 'custscript_refill_referral');
                         
                         var case_priority = nlapiGetFieldValue('priority');
                         
                         if (refill != case_priority) {
                              form.setScript(context.getScriptId());
                              form.addButton('custpage_create_refill', 'Create Refill', 'createRefill()');
                         }
                    }
                    
                    logger.audit(logTitle, '==================== BEFORE LOAD EVENT END ====================');
               } catch (error) {
                    utility.errorEmail(error, errorHandling);
               }
          }
     };
};
function createRefill() {
     
     /**
      * @type UtilityBelt()
      */
     var utility = new UtilityBelt();
     
     var url = nlapiResolveURL('suitelet', 'customscript_bs_create_refill', 'customdeploy_bs_create_refill');
     
     var params = [];
     params['case_id'] = nlapiGetRecordId();
     params['patient_id'] = nlapiGetFieldValue('company');
     params['mode'] = 'single';
     
     url = url + utility.addParams(params);
     
     window.location.replace(url);
}