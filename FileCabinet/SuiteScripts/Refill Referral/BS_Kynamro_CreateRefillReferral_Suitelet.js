/* |=======================================================================================================================|
 * | NAME: BioSolutia|Create Refill Referral                                                                               |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * | This Suitelet creates the Refill Referral Record.                                                                     |                                                 |
 * | ______________________________________________________________________________________________________________________|
 * | Author              Date           Version     Comments                                                               | 
 * | Gerrom V. Infante   Apr 25 2016    1.0                                                                                | 
 * |=======================================================================================================================|
 */
var create_refill = new function () {

    var errorHandling = {
        'script_name': 'BioSolutia|Create Refill Referral',
        'from_email': '-5',
        /* Default Administrator */
        'to_email': 'gerrom.infante@gmail.com',
        'script_file': 'BS_Kynamro_CreateRefillReferral_Suitelet.js'
    };

    return {
        /**
         * @param {nlobjRequest}
         *             request Request object
         * @param {nlobjResponse}
         *             response Response object
         * @returns {Void}
         */
        suitelet: function (request, response) {

            var log_title = 'create_refill.suitelet';

            // enable debug statements if log level is not Debug
            if (context.getLogLevel() == 'DEBUG') {
                logger.enableDebug();
            }

            try {
                logger.audit(log_title, '==================== SUITELET SCRIPT START ====================');

                /* check the mode of the Suitelet */
                var mode = request.getParameter('mode');

                if (mode == 'single') {

                    /* call library function to create Refill Referral */
                    var id = create_refill_referral(request.getParameter('case_id'), request.getParameter('patient_id'));

                    /* show the new referral to the user */
                    nlapiSetRedirectURL('record', 'supportcase', id);
                }

                logger.audit(log_title, '==================== SUITELET SCRIPT END ====================');
            } catch (error) {
                utility.errorEmail(error, errorHandling, true);
            }
        } /* end of suitelet */

    };
};