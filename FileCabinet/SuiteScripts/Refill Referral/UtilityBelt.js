function UtilityBelt() {
     
     /**
      * Determines if a variable is either set to null or is undefined.
      * 
      * @param {String}
      *             value - The value to test
      * @returns {Boolean} true if the variable is null or undefined, false if otherwise.
      */
     this.isNullOrUndefined = function isNullOrUndefined(value) {
          if (value === null) {
               return true;
          }
          if (value === undefined) {
               return true;
          }
          return false;
     };
     
     /**
      * Determines if a string variable is empty or not. An empty string variable is one which is null or undefined or has a length of zero.
      * 
      * @param {String}
      *             stValue - The string value to test for emptiness.
      * @throws {nlobjError}
      *              isEmpty should be passed a string value. The data type passed is {x} whose class name is {y}
      * @returns {Boolean} true if the variable is empty, false if otherwise.
      */
     this.isEmpty = function isEmpty(stValue) {
          if (this.isNullOrUndefined(stValue) === true) {
               return true;
          }
          if (stValue.length == 0) {
               return true;
          }
          return false;
     };
     
     /**
      * This function checks if one of the parameters passed is empty.
      * 
      * @param {string[]}
      *             parameters Array of parameter values.
      * @returns {boolean} true if one of the parameter values is blank, null or undefined.
      */
     this.parameterIsEmpty = function parametersEmpty(parameters) {
          
          var empty = false;
          // check if parameters is empty
          if (this.isEmpty(parameters) === true) {
               empty = true;
          } else {
               for (var x = 0; x < parameters.length; x++) {
                    var value = parameters[x];
                    
                    if (this.isEmpty(value) === true) {
                         empty = true;
                         break;
                    }
               }
          }
          
          return empty;
     };
     
     this.forceParseFloat = function forceParseFloat(variable) {
          
          var value = !this.isEmpty(variable) ? parseFloat(('' + variable).replace(/[^\d\.-]/gi, '')) : 0;
          
          return !isNaN(value) ? value : 0;
     };
     
     this.forceParseInt = function forceParseInt(variable, base) {
          
          var value = !this.isEmpty(variable) ? parseInt(('' + variable).replace(/[^\d\.-]/gi, ''), base) : 0;
          
          return !isNaN(value) ? value : 0;
     };
     
     this.padSpaces = function padSpaces(padString, intTotalDigits, right) {
          var stPaddedString = '';
          if (intTotalDigits > padString.length) {
               for (var i = 0; i < (intTotalDigits - padString.length); i++) {
                    if (right === true) {
                         stPaddedString += ' ';
                    } else {
                         stPaddedString = ' ' + stPaddedString;
                    }
                    
               }
          }
          return padString + stPaddedString;
     };
     
     this.leftTrim = function leftTrim(str, chars) {
          chars = chars || "\\s";
          return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
     };
     
     this.rightTrim = function rightTrim(str, chars) {
          chars = chars || "\\s";
          return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
     };
     
     this.trim = function trim(str, chars) {
          return this.leftTrim(this.rightTrim(str, chars), chars);
     };
     
     this.inArray = function inArray(val, arr) {
          var bIsValueFound = false;
          for (var i = 0; i < arr.length; i++) {
               if (val == arr[i]) {
                    bIsValueFound = true;
                    break;
               }
          }
          return bIsValueFound;
     };
     
     function searchAvailableDeployment(scriptId) {
          if (isEmpty(scriptId) === false) {
               var searchColumns = [];
               searchColumns.push(new nlobjSearchColumn('status'));
               searchColumns.push(new nlobjSearchColumn('scriptid'));
               var searchFilters = [];
               searchFilters.push(new nlobjSearchFilter('scriptid', 'script', 'is', scriptId));
               searchFilters.push(new nlobjSearchFilter('isdeployed', null, 'is', 'T'));
               var results = nlapiSearchRecord('scriptdeployment', null, searchFilters, searchColumns);
               if ((this.isNullOrUndefined(results) === false) && (results.length > 0)) {
                    var deployment = {};
                    deployment.existing = results[0].getId();
                    deployment.usethis = [];
                    for (var x = 0; x < results.length; x++) {
                         var status = results[x].getValue('status');
                         var scriptId = results[x].getValue('scriptid');
                         if (status == "NOTSCHEDULED") {
                              if (this.inArray(scriptId, deployment.usethis) === false) {
                                   deployment.usethis.push(scriptId);
                              }
                         }
                    }
               }
          }
          return deployment;
     }
     
     function createNewDeployment(deployId) {
          var record = nlapiCopyRecord('scriptdeployment', deployId);
          record.setFieldValue('status', 'NOTSCHEDULED');
          record.setFieldValue('startdate', nlapiDateToString(new Date(), 'date'));
          var id = nlapiSubmitRecord(record, true, true);
          return nlapiLookupField('scriptdeployment', id, 'scriptid');
     }
     
     this.callScheduledScript = function callScheduledScript(scriptId, params) {
          var logTitle = 'callScheduledScript';
          var deployment = searchAvailableDeployment(scriptId);
          if (this.isNullOrUndefined(deployment) === false) {
               if (this.isNullOrUndefined(deployment.usethis) === false) {
                    var deployIds = deployment.usethis;
                    if (deployIds.length > 0) {
                         var deployed = false;
                         for (var x = 0; x < deployIds.length; x++) {
                              var schedScriptStatus = nlapiScheduleScript(scriptId, deployIds[x], params);
                              if (schedScriptStatus == 'QUEUED') {
                                   nlapiLogExecution('audit', logTitle, [ '-------------------- Available Sched Script Deployment [', deployIds[x],
                                             '] found, using it. --------------------' ].join(''));
                                   deployed = true;
                                   break;
                              }
                         }
                    }
                    if (deployed === false) {
                         nlapiLogExecution('audit', logTitle,
                                   '-------------------- No available Sched Script Deployment found, creating new deployment. --------------------');
                         var deployId = createNewDeployment(deployment.existing);
                         var schedScriptStatus = nlapiScheduleScript(scriptId, deployId, params);
                    }
               } else {
                    nlapiLogExecution('audit', logTitle,
                              '-------------------- No available Sched Script Deployment found, creating new deployment. --------------------');
                    var deployId = createNewDeployment(deployment.existing);
                    var schedScriptStatus = nlapiScheduleScript(scriptId, deployId, params);
               }
               nlapiLogExecution('audit', logTitle, [ 'Scheduled Script Status: ' + schedScriptStatus ].join(''));
          } else {
               nlapiLogExecution('audit', logTitle, [ 'No deployments found for scheduled script.  Script Id:= ', scriptId ].join(''));
          }
     };
     
     this.addWeekDay = function addWeekDay(fromDate, days) {
          var count = 0;
          while (count < days) {
               fromDate.setDate(fromDate.getDate() + 1);
               if ((fromDate.getDay() != 0) && (fromDate.getDay() != 6)) {
                    count++;
               }
          }
          return fromDate;
     };
     
     this.addTime = function addTime(time1, time2) {
          
          var m = (time1.substring(0, time1.indexOf(':')) - 0) * 60 + (time1.substring(time1.indexOf(':') + 1, time1.length) - 0)
                    + (time2.substring(0, time2.indexOf(':')) - 0) * 60 + (time2.substring(time2.indexOf(':') + 1, time2.length) - 0);
          
          var h = Math.floor(m / 60);
          
          if ((m - (h * 60)) == 0) {
               var addTime = h + ':00';
          } else {
               var addTime = h + ':' + (m - (h * 60));
          }
          
          return addTime;
     };
     
     /**
      * Used to created URL parameters out of array.
      * 
      * @author Jasper Salcedo
      * @version 1.0
      */
     this.addParams = function addParams(pParams) {
          var additionalParams = '';
          for ( var param in pParams) {
               additionalParams += '&' + param + '=' + pParams[param];
          }
          
          return additionalParams;
     };
     /**
      * This function sends out an error email.
      * 
      * @param {nlobjError|Error}
      *             error Object containing the information.
      * @param {Object}
      *             details Custom object containing script details.
      */
     this.errorEmail = function(error, details, throw_error) {
          
          var errorMessage = 'Unexpected Error Encountered';
          
          var timezone = new DateTimeZoneUtils();
          
          var errorDetails;
          if (error instanceof nlobjError) {
               errorDetails = '\n' + 'Error:' + error.getCode() + '\n';
               errorDetails += 'Error Reference:' + error.getId() + '\n';
               errorDetails += 'Error Details:' + error.getDetails() + '\n\n';
               errorDetails += 'Stack Trace:' + error.getStackTrace() + '\n';
          } else {
               errorDetails = '\n' + 'Error:' + error.name + '\n';
               errorDetails += 'Error Details:' + error.message + '\n\n';
               errorDetails += 'Stack Trace:' + error.stack + '\n';
          }
          
          //Log the error on the execution log/show a pop-up box with the error message.
          nlapiLogExecution('ERROR', 'ERROR_NOTIFICATION', errorMessage + ".  " + errorDetails);
          

               var body = "Account: " + nlapiGetContext().getCompany() + "\n";
               body += "Environment: " + nlapiGetContext().getEnvironment() + "\n";
               body += "Execution Context: " + nlapiGetContext().getExecutionContext() + "\n";
               body += "Date and Time: " + timezone.getCurrentDateTimeText('10', 'datetimetz') + "\n";
               body += "User: " + nlapiGetContext().getUser() + ' ' + nlapiGetContext().getName() + "\n";
               body += "User Role: " + nlapiGetContext().getRole() + "\n";
               body += "NetSuite Version: " + nlapiGetContext().getVersion() + "\n";
               body += "Script: " + details.script_name + "\n";
               body += "Deployment ID: " + nlapiGetContext().getDeploymentId() + "\n";
               body += "Script File: " + details.script_file + "\n\n";
               body += "Error Details: " + errorMessage + "." + errorDetails + "\n";
               
               //Send the email
               nlapiSendEmail(details.from_email, details.to_email, "Error Notification", body, null, null, null, null);
          
		  if (throw_error === true) {
			  if (error instanceof nlobjError) {
               throw error;
			  } else {
				   throw nlapiCreateError('99999', error.toString());
			  }
		  }
     };
     
     this.padSpaces = function padSpaces(padString, intTotalDigits) {
          var stPaddedString = '';
          if (intTotalDigits > padString.length) {
               for (var i = 0; i < (intTotalDigits - padString.length); i++) {
                    stPaddedString += ' ';
               }
          }
          return padString + stPaddedString;
     };
     this.getSearchColumnNames = function (results) {
          var column_names = [];
          
          if (this.isEmpty(results) === false) {
                    var columns = results[0].getAllColumns();
                    
                    for (var j = 0; j < columns.length; j++) {
                         column_names.push(columns[j].getName());
                    }
          }
          
          return columns_names;
     };
     
     this.resultsToJSON = function(results, result_option) {
          var jsonResult = [];
          if (this.isEmpty(results) === false) {
               try {
                    switch (result_option) {
                         case 'text':
                              for (var i = 0; i < results.length; i++) {
                                   var columns = results[i].getAllColumns();
                                   var jsonObj = {
                                        internalid : results[i].getId()
                                   };
                                   var text_val, value;
                                   for (var j = 0; j < columns.length; j++) {
                                        if (columns[j].getJoin() != null) {
                                             value = results[i].getValue(columns[j].getName(), columns[j].getJoin());
                                             text_val = results[i].getText(columns[j].getName(), columns[j].getJoin());
                                        } else {
                                             value = results[i].getValue(columns[j].getName());
                                             text_val = results[i].getText(columns[j].getName());
                                        }
                                        if ((text_val != null) && (text_val != '') && (text_val != 'null')){
                                             jsonObj[columns[j].getName()] = text_val;
                                        } else {
                                             jsonObj[columns[j].getName()] = value;
                                        }
                                        
                                   }
                                   jsonResult.push(jsonObj);
                              }
                              return jsonResult;
                         case 'value':
                              for (var i = 0; i < results.length; i++) {
                                   var columns = results[i].getAllColumns();
                                   var jsonObj = {
                                        internalid : results[i].getId()
                                   };
                                   for (var j = 0; j < columns.length; j++) {
                                        if (columns[j].getJoin() != null) {
                                             jsonObj[columns[j].getName()] = results[i].getValue(columns[j].getName(), columns[j].getJoin());
                                        } else {
                                             jsonObj[columns[j].getName()] = results[i].getValue(columns[j].getName());
                                        }
                                        
                                   }
                                   jsonResult.push(jsonObj);
                              }
                              return jsonResult;
                              
                              break;
                         default:
                              for (var x = 0; x < results.length; x++) {
                                   var columns = results[x].getAllColumns();
                                   var jsonData = {
                                        internalid : results[x].getId()
                                   };
                                   
                                   for (var y = 0; y < columns.length; y++) {
                                        
                                        if (columns[y].getJoin() != null) {
                                             var data = {
                                                  'value' : results[x].getValue(columns[y].getName(), columns[j].getJoin()),
                                                  'text' : results[x].getText(columns[y].getName(), columns[j].getJoin())
                                             
                                             }
                                        } else {
                                             var data = {
                                                  'value' : results[x].getValue(columns[y].getName()),
                                                  'text' : results[x].getText(columns[y].getName())
                                             
                                             }
                                        }
                                        
                                        jsonData[columns[y].getName()] = data;
                                   }
                                   jsonResult.push(jsonData);
                              }
                              break;
                    }
               } catch (error) {
                    nlapiLogExecution('error', 'SearchObject().resultsToJSON', [ 'Unexpected Error:= ', error.toString() ].join(''));
               }
          }
          return jsonResult;
     };
     
     this.toItemRecordInternalId = function toItemRecordInternalId(stRecordType) {
          if (this.isEmpty(stRecordType)) {
               throw nlapiCreateError('10003', 'Item record type should not be empty.');
          }
          var stRecordTypeInLowerCase = stRecordType.toLowerCase().trim();
          switch (stRecordTypeInLowerCase) {
               case 'invtpart':
                    return 'inventoryitem';
               case 'description':
                    return 'descriptionitem';
               case 'assembly':
                    return 'assemblyitem';
               case 'discount':
                    return 'discountitem';
               case 'group':
                    return 'itemgroup';
               case 'markup':
                    return 'markupitem';
               case 'noninvtpart':
                    return 'noninventoryitem';
               case 'othcharge':
                    return 'otherchargeitem';
               case 'payment':
                    return 'paymentitem';
               case 'service':
                    return 'serviceitem';
               case 'subtotal':
                    return 'subtotalitem';
               default:
                    return stRecordTypeInLowerCase;
          }
     };
     
     this.closeSuitelet = function closeSuitelet() {
          var html = [];
          html.push('<html>');
          html.push('<head>');
          html.push('<script>');
          html.push("window.close()");
          html.push('</script>')
          html.push('</head>');
          html.push('<body>');
          html.push('</body>');
          html.push('</html>');
          
          html = html.join('');
          
          return html;
     };
}
function DateTimeZoneUtils() {
     
     /**
      * This is a method to get the current Date/Time based on the time zone offset specified.
      * 
      * @param timeZoneOffSet
      *             {Number} - The time zone offset.
      * @returns {Date} date - The Date/Time Object based on the time zone offset specified.
      */
     this.getCurrentDateTime = function(timeZoneOffSet) {
          if ((timeZoneOffSet == null && timeZoneOffSet == '' && timeZoneOffSet == undefined) || isNaN(timeZoneOffSet))
               return new Date();
          
          var currentDateTime = new Date();
          var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
          currentDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);
          
          return new Date(currentDateTime);
     };
     
     /**
      * This is a method to get the current Date/Time based on a passed time zone offset converted to the specified Date/Time string format.
      * 
      * @param timeZoneOffSet
      *             {Number} - The time zone offset.
      * @param nsDateFormat
      *             {String} - The Date/Time format string to convert the date to.
      * @returns {String} dateString - The Date/Time Object based on the time zone offset specified converted to the Date/Time string format
      *          specified.
      */
     this.getCurrentDateTimeText = function(timeZoneOffSet, nsDateFormat) {
          return nlapiDateToString(this.getCurrentDateTime(timeZoneOffSet), nsDateFormat);
     };
     
     /**
      * This is a method to get the current Date/Time based on the time zone selected on the Company Preferences page.
      * 
      * @returns {Date} date - The Date/Time Object based on the time zone selected under the Company Preferences page.
      */
     this.getCompanyCurrentDateTime = function() {
          var currentDateTime = new Date();
          var companyTimeZone = nlapiLoadConfiguration('companyinformation').getFieldText('timezone');
          var timeZoneOffSet = (companyTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(companyTimeZone.substr(4, 6).replace(/\+|:00/gi, '').replace(
                    /:30/gi, '.5'));
          var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
          var companyDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);
          
          return new Date(companyDateTime);
     };
     
     /**
      * This is a method to get the current Date/Time based on the time zone selected on the Company Preferences page converted to a specified
      * Date/Time string format.
      * 
      * @param nsDateFormat
      *             {String} - The Date/Time format string to convert the date to.
      * @returns {String} dateString - The Date/Time Object based on the time zone selected under the Company Preferences page converted to the
      *          Date/Time string format specified.
      */
     this.getCompanyCurrentDateTimeText = function(nsDateFormat) {
          return nlapiDateToString(this.getCompanyCurrentDateTime(), nsDateFormat);
     };
     
     /**
      * This is a method to get the current Date/Time based on the time zone selected on the Subsidiary passed as a parameter.
      * 
      * @param subsidiary
      *             {Number} - The subsidiary internal id.
      * @returns {Date} date - The Date/Time Object based on the time zone selected under the Subsidiary record specified.
      */
     this.getSubsidiaryCurrentDateTime = function(subsidiary) {
          var currentDateTime = new Date();
          var subsidiaryTimeZone = nlapiLoadRecord('subsidiary', subsidiary).getFieldText('TIMEZONE');
          var timeZoneOffSet = (subsidiaryTimeZone.indexOf('(GMT)') == 0) ? 0 : new Number(subsidiaryTimeZone.substr(4, 6).replace(/\+|:00/gi, '')
                    .replace(/:30/gi, '.5'));
          var UTC = currentDateTime.getTime() + (currentDateTime.getTimezoneOffset() * 60000);
          var subsidiaryDateTime = UTC + (timeZoneOffSet * 60 * 60 * 1000);
          
          return new Date(subsidiaryDateTime);
     };
     
     /**
      * This is a method to get the current Date/Time based on the time zone selected on
      * the Subsidiary passed as a parameter converted to the Date/Time String format specified.
      * 
      * @param subsidiary {Number} - The subsidiary internal id.
      * @param nsDateFormat {String} - The Date/Time format string to convert the date to.
      * 
      * @returns {String} dateString - The Date/Time Object based on the time zone selected under the Subsidiary record converted to the Date/Time string format specified.
      */
     this.getSubsidiaryCurrentDateTimeText = function(timeZoneOffSet, nsDateFormat) {
          return nlapiDateToString(this.getCurrentDateTime(timeZoneOffSet), nsDateFormat);
     };
}