function SearchObject() {
	var arFilters = new Array();
	var arColumns = new Array();
	var strType = '';
	var intSavedSearch = null;
	function isNullOrUndefined(value) {
		if (value === null) {
			return true;
		}
		if (value === undefined) {
			return true;
		}
		return false;
	}
	function isEmpty(stValue) {
		if (isNullOrUndefined(stValue) === true) {
			return true;
		}
		if (stValue.length == 0) {
			return true;
		}
		return false;
	}
	function getSearchResults(recordType, searchId, searchFilter, searchColumn) {
		var returnSearchResults = [];
		var count = 1000,
		min = 0,
		max = 1000;
		var savedSearch;
		if (isEmpty(searchId) === false) {
			savedSearch = nlapiLoadSearch(recordType, searchId);
			if (isNullOrUndefined(searchFilter) === false) {
				savedSearch.addFilters(searchFilter);
			}
			if (isNullOrUndefined(searchColumn) === false) {
				savedSearch.addColumns(searchColumn);
			}
		} else {
			if (recordType == '') {
				nlapiLogExecution('Audit', 'getSearchResults', 'record type not defined');
				return null;
			}
			savedSearch = nlapiCreateSearch(recordType, searchFilter, searchColumn);
		}
		var resultset = savedSearch.runSearch();
		while (count == 1000) {
			var resultslice = resultset.getResults(min, max);
			if (isNullOrUndefined(resultslice) === false) {
				returnSearchResults = returnSearchResults.concat(resultslice);
			}
			min = max;
			max += 1000;
			count = resultslice.length;
		}
		if (returnSearchResults) {
			nlapiLogExecution('DEBUG', 'getSearchResults', 'Total search results: ' + returnSearchResults.length);
		}
		return returnSearchResults;
	}
	this.addFilter = function (name, join, operator, value1, value2, summary) {
		if (summary) {
			arFilters[arFilters.length] = new nlobjSearchFilter(name, join, operator, value1, value2).setSummaryType(summary);
		} else {
			arFilters[arFilters.length] = new nlobjSearchFilter(name, join, operator, value1, value2);
		}
	};
	this.addOrFilters = function (orFilter1, orFilter2) {
		arFilters[arFilters.length] = orFilter1;
		arFilters[arFilters.length] = orFilter2;
	};
	this.addColumn = function (name, join, summary, label) {
		if (label != null) {
			arColumns[arColumns.length] = new nlobjSearchColumn(name, join, summary).setLabel(label);
		} else {
			arColumns[arColumns.length] = new nlobjSearchColumn(name, join, summary);
		}
	};
	this.clearParams = function () {
		arFilters = new Array();
		arColumns = new Array();
		strType = '';
		intSaveSearch = null;
	};
	this.setType = function (typeName) {
		strType = typeName;
	};
	this.setFilter = function (filter) {
		arFilters = filter;
	};
	this.setSort = function (columnIndex, descending) {
		arColumns[columnIndex].setSort(descending);
	};
	this.setSavedSearch = function (savedSearchId) {
		intSavedSearch = savedSearchId;
	};
	this.setFunction = function (func, columnIndex) {
		arColumns[columnIndex].setFunction(func);
	};
	this.setFormula = function (formula, columnIndex) {
		arColumns[columnIndex].setFormula(formula);
	};
	this.execute = function () {
		var result = getSearchResults(strType, intSavedSearch, arFilters, arColumns);
		return result;
	};
	this.removeLastFilter = function () {
		arFilters.pop();
	};
	this.setOr = function (filterIndex, setOr) {
		arFilters[filterIndex].setOr(setOr);
	};
	this.setLeftParens = function (filterIndex, numParens) {
		arFilters[filterIndex].setLeftParens(numParens);
	};
	this.setRightParens = function (filterIndex, numParens) {
		arFilters[filterIndex].setRightParens(numParens);
	};
}