/* =============================
 * | Global Variables          |
 * =============================
 */
{
     var generalFunctions_global;
     var scriptLogger_global;

     // global object for Execution Context
     /**
      * @type nlobjContext
      */
     var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 2017                                                                                                    |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |
 * | This software is the confidential and proprietary information of BioSolutia ('Confidential Information').             |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with BioSolutia.                                                                           |
 * |=======================================================================================================================|
 * | NAME:    NS | Decrypt Email                                                                                           |
 * |                                                                                                                       |
 * | OVERVIEW:                                                                                                             |
 * |                                                                                                                       |
 * | This automation runs when an email is sent from BioSolutia's NetSuite account.  The script will decrypt pre-determined|
 * | fields on the Patient and Case Insurance Data record using CypherCloud webservices.  The script will loop thru the    |
 * | files attached and decrypt the file using CypherCloud's webservice.  The script will save the decrypted file (folder  |
 * | is controlled by a script parameter) and replace the attached file with the decrypted file.                           |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			  Date			      Version     Comments                                                             |
 * | Gerrom Infante     	Aug 09, 2014	  1.0			                                                                         |
 * |_______________________________________________________________________________________________________________________|
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 *
 * @appliedtorecord message
 * @param {String} type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
function beforeSubmit_decryptEmail(type) {

     var funcName = 'beforeSubmit_decryptEmail';

     // global object for general functions
     /**
      * @type CommonFunctions
      */
     generalFunctions_global = new NetSuiteCustomLibrary()
          .StandardFunctions();

     // global object for script logging
     /**
      * @type Logger
      */
     scriptLogger_global = new NetSuiteCustomLibrary()
          .ScriptLogger();


     if (executionContext_global.getLogLevel() == 'DEBUG') {
          scriptLogger_global.enableDebug();
     }

     try {
          scriptLogger_global.audit(funcName, '==================== BEFORE SUBMIT EVENT START ====================');

          /**
           * @type nlobjRecord
           * @record message
           */
          var messageRecord = nlapiGetNewRecord();

          // get the template type
          var templateType = messageRecord.getFieldValue('templatetype');
          scriptLogger_global.debug(funcName, ['Template Type:= ', templateType].join(''));

          if (templateType.toUpperCase() != 'EMAIL') { // Do not continue if they are not using an email template
               return true;
          }

          // get the referral record where the email is being sent
          var referalId = messageRecord.getFieldValue('activity');
          scriptLogger_global.debug(funcName, ['Referral Record Internal Id:= ', referalId].join(''));

          if (generalFunctions_global.isEmpty(referalId) === true) {
               throw nlapiCreateError('99999', "Emails can only be sent from the Referral record.");
          }

          // get the text of the fax template
          var templateBody = messageRecord.getFieldValue('message');

          var freeMarker = false;
          // templateBody message is blank if Template type is FreeMarker
          if (generalFunctions_global.isEmpty(templateBody) === true) {
               freeMarker = true;
               var templateBody = messageRecord.getFieldValue('freemarkermessage');
          }
          scriptLogger_global.debug(funcName, ['Template Body:= ', nlapiEscapeXML(templateBody)].join(''));

          // check if body replacement logic is turned on
          var enableReplacement = executionContext_global.getSetting('script', 'custscript_email_enable_body_replacement');

          var templateId = messageRecord.getFieldValue('template');

          if (enableReplacement == 'T') {
               // get the template body replacement mapping
               var templateMapping = executionContext_global.getSetting('script', 'custscript_email_replacement_mapping');
               scriptLogger_global.debug(funcName, ['Template Replacement Mapping:= ', templateMapping].join(''));
               templateMapping = JSON.parse(templateMapping);

               // check if the template Id has a replacement mapping
               var newBodyFile = templateMapping[templateId];

               if (generalFunctions_global.isEmpty(newBodyFile) === false) {
                    // load the file mentioned in the mapping and replace the fax message body
                    /**
                     * @type nlobjFile
                     */
                    var newBodyFile = nlapiLoadFile(newBodyFile);
                    var templateBody = newBodyFile.getValue();

               }
          }

          try {
               /**
                * @type nlobjTemplateRenderer
                */
               var renderer = nlapiCreateTemplateRenderer();
               renderer.setTemplate(templateBody);

               // load the record
               /**
                * @type nlobjRecord
                * @record supportcase
                */
               var supportCase = nlapiLoadRecord('supportcase', referalId);
               renderer.addRecord('case', supportCase);
               var body = renderer.renderToString();
          } catch (error1) {
               if (error1 instanceof nlobjError) {
                    nlapiLogExecution('ERROR', funcName, [error1.getCode(), ': ', error1.getDetails()].join(''));
               } else {
                    nlapiLogExecution('ERROR', funcName, error1.toString());

               }
          }

          var newMessage = decryptEmailFields(referalId, templateBody, templateId);
          scriptLogger_global.debug(funcName, ['Template Body Decrypted:= ', nlapiEscapeXML(newMessage)].join(''));

          // replace template text
          if (freeMarker === true) {
               messageRecord.setFieldValue('freemarkermessage', newMessage);
          } else {
               messageRecord.setFieldValue('message', newMessage);
          }

          // get the value of the email receiptient
          var receipient = executionContext_global.getSetting('script', 'custscript_email_recipient_email');

          // check if Debug parameter is checked
          if ((executionContext_global.getSetting('script', 'custscript_email_debug') == 'T') || (executionContext_global.getUser() == receipient)) {
               nlapiSendEmail(receipient, receipient, 'Decrypted Email Body', newMessage);
               scriptLogger_global.debug(funcName, '-------------------- Sending Email --------------------');
          }

          var attachmentCount = messageRecord.getLineItemCount('mediaitem');
          scriptLogger_global.debug(funcName, ['Attachment:= ', attachmentCount].join(''));

          var pdf_pages = [];
          if (attachmentCount > 0) {

               // loop thru all the attachments
               for (var x = 1; x <= attachmentCount; x++) {
                    var attachment = messageRecord.getLineItemValue('mediaitem', 'mediaitem', x);
                    scriptLogger_global.debug(funcName, ['File Internal Id:= ', attachment].join(''));

                    var newAttachment = decryptEmailAttachment(attachment);
                    pdf_pages.push(newAttachment);

                    if (generalFunctions_global.isNullOrUndefined(newAttachment) === false) {
                         // replace old attachment with new attachment
                         messageRecord.setLineItemValue('mediaitem', 'mediaitem', x, newAttachment);
                    }
               }
          }

          // generate a PDF version of the email
          newMessage = decryptEmailFields(referalId, body, templateId);
          nlapiLogExecution('DEBUG', funcName, ['Email Markup:= ', newMessage].join(''));

          // add any existing attachment as pages to the email pdf
          var email_pdf_template = addAttachmentsToPDF(newMessage, pdf_pages);

          // check for the presence of \r\n in the template body and remove it
          if (email_pdf_template.indexOf('\r\n') !== -1) {
               nlapiLogExecution('AUDIT', funcName, 'Removing carriage return characters');
               email_pdf_template = replaceAll(email_pdf_template, '\r\n', '');
          }

          var email_pdf = nlapiXMLToPDF(email_pdf_template);

          // create the file name  format is <Patient FirstName>_<Patient LastName>_<today’s date>_<Patient ID>
          var patient = supportCase.getFieldValue('company');

          // get the first name, last name and patient id
          var patient_info = nlapiLookupField('customer', patient, ['entitynumber', 'firstname', 'lastname', 'custentity_bio_date_of_birth']);
          // decrypt the info
          patient_info = decryptPatientInfo(patient_info);
          nlapiLogExecution('DEBUG', funcName, ['patient_info:= ', JSON.stringify(patient_info)].join(''));

          //convert server time to user time
          //get current date/time which will return time in Pacific time
          var rightNow = new Date();

          //set date/time field using setDateTimeValue() API and pass in Olson value of 5 (US/Los_Angeles).
          //Olson value of 5 is used since we KNOW NetSuites' date is in Pacific time zone.
          supportCase.setDateTimeValue('custevent_bs_convert_date_time', nlapiDateToString(rightNow, 'datetimetz'), 5);

          //get value of date/time in users' timezone
          var userDateTime = supportCase.getFieldValue('custevent_bs_convert_date_time');

          nlapiLogExecution('DEBUG', funcName, ['serverTime:= ', rightNow.toString(), ' | bsTime:= ', userDateTime.toString()].join(''));

          var fileName = [patient_info.firstname, '_', patient_info.lastname, '_', patient_info.custentity_bio_date_of_birth, '_Elecare_', userDateTime, '_', patient_info.entitynumber, '.pdf'].join('');

          fileName = fileName.replace(/\//g, "-");
          fileName = fileName.replace(/:/g, "-");
          fileName = fileName.replace(/\s+/g, "_");

          var file = nlapiCreateFile(fileName, 'PDF', email_pdf.getValue());
          file.setFolder(executionContext_global.getSetting('SCRIPT', 'custscript_email_pdf_folder'));
          var fileId = nlapiSubmitFile(file);

          // change the subject of the email
          var current_subject = messageRecord.getFieldValue('subject');
          // append the patient id to the subject
          current_subject = [current_subject, ' - ', patient_info.entitynumber, ' ', patient_info.firstname, ' ', patient_info.lastname].join('');

          messageRecord.setFieldValue('subject', current_subject);
          nlapiLogExecution('DEBUG', funcName, ['current_subject:= ', current_subject].join(''));

          // remove any existing attachments
          var attachmentCount = messageRecord.getLineItemCount('mediaitem');
          for (var x = attachmentCount; x >= 1; x--) {
               var attachment = messageRecord.getLineItemValue('mediaitem', 'mediaitem', x);
               messageRecord.removeLineItem('mediaitem', x);
          }

          // attach file to email
          attachmentCount = messageRecord.getLineItemCount('mediaitem');
          messageRecord.setLineItemValue('mediaitem', 'mediaitem', parseInt(attachmentCount) + 1, fileId);

          scriptLogger_global.audit(funcName, ['=================== BEFORE SUBMIT EVENT END  ==================='].join(''));
     } catch (error) {
          if (error instanceof nlobjError) {
               nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
               throw error;
          } else {
               nlapiLogExecution('ERROR', funcName, error.toString());
               throw nlapiCreateError('99999', error.toString());
          }
     }
}

function decryptPatientInfo(patient_info) {

    var output = {};

    output.entitynumber = decryptField(patient_info.entitynumber, 'company');
    output.firstname = decryptField(patient_info.firstname, 'custpage_patient_firstname');
    output.lastname = decryptField(patient_info.lastname, 'custpage_patient_lastname');
    output.custentity_bio_date_of_birth = decryptField(patient_info.custentity_bio_date_of_birth, 'custevent_bio_date_of_birth');

     return output;
}

function addAttachmentsToPDF(email_body, pages) {

     var funcName = 'addAttachmentsToPDF';

     var pdf_template = [];
     pdf_template.push(['<?xml version="1.0" encoding="UTF-8"?>'].join(''));
     pdf_template.push(['<!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "http://bfo.com/products/report/report-1.1.dtd">'].join(''));
     pdf_template.push(['<pdfset>'].join('')); //Open <pdfset> tag on your XML

     // add the pdf version of the email
     pdf_template.push(['<pdf>'].join(''));
     pdf_template.push(['<head>'].join(''));
     pdf_template.push(['</head>'].join(''));
     pdf_template.push([email_body].join(''));
     pdf_template.push(['</pdf>'].join(''));

     for (var i = 0, len = pages.length; i < len; i++) {
          var pdf_url = nlapiLoadFile(pages[i])
               .getURL(); //Get the URL of the File.
          pdf_url = nlapiEscapeXML(pdf_url); //Replace the & sign on the URL with &amp; so that it will be parsed correctly by the BFO library.
          nlapiLogExecution('DEBUG', funcName, ['PDF URL:= ', pdf_url].join(''));

          pdf_template.push(['<pdf src="' + pdf_url + '"/>'].join(''));
     }

     pdf_template.push(['</pdfset>'].join('')); //Close </pdfset> tag on your XML
     pdf_template = pdf_template.join('');

     nlapiLogExecution('DEBUG', funcName, ['PDF TEMPLATE:= ', pdf_template].join(''));

     return pdf_template;
}

/**
 * This function decrypts the fields that are encrypted
 *
 * @param {string} referalId The internal id of the support case record
 * @param {string} templateBody The body of the Fax being sent
 * @param {string} templateId The internal id of the template used
 * @returns {string}
 */
function decryptEmailFields(referalId, templateBody, templateId) {

     var funcName = 'decryptEmailFields';

     // get the value of the encrypt company field from the referral record
     /**
      * @type Array
      */
     var fieldsToGet = [];

     /**
      * @type Array
      */
     var fieldsToGet = [];

    fieldsToGet.push('custevent_fax2_noaction_chkbox');
    fieldsToGet.push('custevent_fax2_actionneeded_chkbox');
    fieldsToGet.push('custevent_fax2_notcovered_chkbox');

    fieldsToGet.push('custevent_fax3_patientcontinfo_chkbox');
    fieldsToGet.push('custevent_fax3_patientbirthdate_chkbox');
    fieldsToGet.push('custevent_fax3_ptntpharminsinfo_chkbox');
    fieldsToGet.push('custevent_fax3_signedprescform_chkbox');
    fieldsToGet.push('custevent_fax3_other_chkbox');

    fieldsToGet.push('custevent_fax4_paformaccfax_chkbox');
    fieldsToGet.push('custevent_fax4_paformfromins_chkbox');
    fieldsToGet.push('custevent_fax4_paperformedverbal_chkbox');
    fieldsToGet.push('custevent_fax4_submittedbyhub');

    fieldsToGet.push('custevent_fax5_ship_copay_auth');
    fieldsToGet.push('custevent_fax5_pt_unresp_msg_info');

    fieldsToGet.push('custevent_fax6_rx_canc_pt_unresp');
    fieldsToGet.push('custevent_fax6_rx_canc_pt_choice');
    fieldsToGet.push('custevent_fax6_rx_canc_msg_info');
    fieldsToGet.push('custevent_fax6_rx_canc_msg_signed_rx');
    fieldsToGet.push('custevent_fax6_rx_canc_other_chk_box');

    fieldsToGet.push('custevent_fax7_phcy_triage_instructions');
    fieldsToGet.push('custevent_bio_date_of_birth');
    fieldsToGet.push('custevent_bio_refl_transaction_type');

     /*
      * This if for the Issue 293213 workaround
      */
     fieldsToGet.push('CUSTEVENT_BIO_PRESCRIBER_ID_ONLY');
     fieldsToGet.push('CUSTEVENT_BIO_PHYSICIAN_FAX_NUMBER');
     fieldsToGet.push('CUSTEVENT_BIO_PHYSICIAN_PHONE_NUMBER');
     fieldsToGet.push('CUSTEVENT_BIO_PATIENT_ID_ONLY');
     fieldsToGet.push('custevent_bio_sp_triage_nbd');
     fieldsToGet.push('CUSTEVENT_BIO_COPAY_PROGRAM_ELIG');
     /*
      * End of Workaround
      */

     fieldsToGet.push('custevent_bio_physician');

     var encryptedCompany = nlapiLookupField('supportcase', referalId, 'company', true);
     scriptLogger_global.debug(funcName, ['Patient:= ', encryptedCompany].join(''));

     var checkboxes = nlapiLookupField('supportcase', referalId, fieldsToGet);
     scriptLogger_global.debug(funcName, ['Fax Template Checkbox Values:= ', JSON.stringify(checkboxes)].join(''));

     // check if '<NLCOMPANY>' is in the fax template
     if (templateBody.indexOf('<NLCOMPANY>') != -1) {
          // pass the company for decryption
          var decryptedCompany = decryptField(encryptedCompany, 'company');

          templateBody = templateBody.replace('<NLCOMPANY>', decryptedCompany);
     }

     // check if '<NLCUSTENTITY_BIO_DATE_OF_BIRTH>' is in the fax template
     if (templateBody.indexOf('<NLCUSTENTITY_BIO_DATE_OF_BIRTH>') != -1) {
          // pass the company for decryption
          var decryptedDOB = decryptField(checkboxes['custevent_bio_date_of_birth'], 'custevent_bio_date_of_birth');

          templateBody = templateBody.replace('<NLCUSTENTITY_BIO_DATE_OF_BIRTH>', decryptedDOB);
     }

     /*
      * This if for the Issue 293213 workaround
      */
     // check if '{NLCOMPANY}' is in the fax template
     if (templateBody.indexOf('{NLCOMPANY}') != -1) {
          // pass the company for decryption
          var decryptedCompany = decryptField(encryptedCompany, 'company');

          templateBody = templateBody.replace('{NLCOMPANY}', decryptedCompany);
     }

     // check if '{NLCUSTENTITY_BIO_DATE_OF_BIRTH}' is in the fax template
     if (templateBody.indexOf('{NLCUSTENTITY_BIO_DATE_OF_BIRTH}') != -1) {
          // pass the dob for decryption
          var decryptedDOB = decryptField(checkboxes['custevent_bio_date_of_birth'], 'custevent_bio_date_of_birth');

          templateBody = templateBody.replace('{NLCUSTENTITY_BIO_DATE_OF_BIRTH}', decryptedDOB);
     }

     var templateFields = ['CUSTEVENT_BIO_PRESCRIBER_ID_ONLY', 'CUSTEVENT_BIO_PHYSICIAN_FAX_NUMBER', 'CUSTEVENT_BIO_PHYSICIAN_PHONE_NUMBER',
          'CUSTEVENT_BIO_PATIENT_ID_ONLY', 'CURRENTDATE', 'custevent_bio_sp_triage_nbd'
     ];

     for (var field in templateFields) {
          var fieldName = templateFields[field];

          var replaceTag = ['{NL', fieldName, '}'].join('');
          replaceTag = replaceTag.toUpperCase();

          if (fieldName != 'CURRENTDATE') {
               var replaceValue = (generalFunctions_global.isEmpty(checkboxes[fieldName]) === true) ? '' : checkboxes[fieldName];
          } else {
               var replaceValue = nlapiDateToString(new Date(), 'date');
          }

          templateBody = templateBody.replace(replaceTag, replaceValue);
     }

     // get the template id for Fax 2
     var fax2Template = executionContext_global.getSetting('script', 'custscript_email_template2');

     if (templateId == fax2Template) {
          var referralTextFields = ['custevent_bio_sp_triage_mop'];
     } else {
          var referralTextFields = ['CUSTEVENT_BIO_COPAY_PROGRAM_ELIG', 'custevent_bio_sp_triage_mop'];
     }

     var referralTextData = nlapiLookupField('supportcase', referalId, referralTextFields, true);

     for (var field in referralTextFields) {
          var fieldName = referralTextFields[field];

          var replaceTag = ['{NL', fieldName, '}'].join('');
          replaceTag = replaceTag.toUpperCase();

          var replaceValue = (generalFunctions_global.isEmpty(referralTextData[fieldName]) === true) ? '' : referralTextData[fieldName];

          templateBody = templateBody.replace(replaceTag, replaceValue);
     }

     //     templateBody = templateBody.replace('<nlcustevent_fax7_phcy_triage_instructions>', checkboxes.custevent_fax7_phcy_triage_instructions);
     templateBody = templateBody.replace('{NLCUSTEVENT_FAX7_PHCY_TRIAGE_INSTRUCTIONS}', checkboxes.custevent_fax7_phcy_triage_instructions);
     /*
      * End of Workaround
      */

     // check if '<NLDELIVERY_ADDRESS>' is in the fax template
     if ((templateBody.indexOf('<NLDELIVERY_ADDRESS>') != -1) || (templateBody.indexOf('{NLDELIVERY_ADDRESS}') != -1)) {
          // get the address of the Prescriber
          var prescriberId = checkboxes['custevent_bio_physician'];
          scriptLogger_global.debug(funcName, ['Prescriber [Internal Id]:= ', prescriberId].join(''));

          var prescriberFields = [];
          prescriberFields.push('billaddress1');
          prescriberFields.push('billaddress2');
          prescriberFields.push('billcity');
          prescriberFields.push('billstate');
          prescriberFields.push('billzipcode');
          prescriberFields.push('billcountrycode');

          var addressInfo = nlapiLookupField('partner', prescriberId, prescriberFields);
          scriptLogger_global.debug(funcName, ['Address Info:= ', JSON.stringify(addressInfo)].join(''));

          if (generalFunctions_global.isEmpty(addressInfo['billaddress1']) === false) {
               // check if address is not 'APC USER'
               if (addressInfo['billaddress1'].indexOf('APC USER ONLY') == -1) {
                    var deliveryAddress = [];

                    // construct the address line
                    deliveryAddress.push(addressInfo['billaddress1']);
                    deliveryAddress.push(addressInfo['billaddress2']);
                    deliveryAddress.push(addressInfo['billcity']);
                    deliveryAddress.push(addressInfo['billstate']);
                    deliveryAddress.push(addressInfo['billzipcode']);
                    deliveryAddress.push(addressInfo['billcountrycode']);

                    deliveryAddress = deliveryAddress.join(' ');
               }
          } else {
               var deliveryAddress = '';
          }

          templateBody = templateBody.replace('<NLDELIVERY_ADDRESS>', deliveryAddress);
          templateBody = templateBody.replace('{NLDELIVERY_ADDRESS}', deliveryAddress);
     }

     // get the value of "NO - NOT ELIGIBLE" for the Copay Program Eligibility field in the script parameter
     var capNotEligible = executionContext_global.getSetting('script', 'custscript_cap_not_eligible');

     // update the checkboxes
     for (var checkbox in checkboxes) {
          var replaceTag = ['{nl', checkbox, '}'].join('');
          replaceTag = replaceTag.toUpperCase();
          //          scriptLogger_global.debug(funcName, [ 'Tag To Replace:= ', replaceTag ].join(''));

          var replaceValue;
          if (replaceTag != '{NLCUSTEVENT_BIO_COPAY_PROGRAM_ELIG}') {
               if (checkboxes[checkbox] === 'T') {
                    replaceValue = ['<input type="checkbox" name="', checkbox, '" value="', checkbox, '" checked>'].join('');
               } else {
                    replaceValue = ['<input type="checkbox" name="', checkbox, '" value="', checkbox, '">'].join('');
               }
          } else {
               // check if template is fax 2
               if (templateId == fax2Template) {
                    // check if the value is equal to the parameter value
                    var fieldValue = checkboxes['CUSTEVENT_BIO_COPAY_PROGRAM_ELIG'];

                    if (fieldValue == capNotEligible) {
                         replaceValue = ['<input type="checkbox" name="', checkbox, '" value="', checkbox, '" checked>'].join('');
                    } else {
                         replaceValue = ['<input type="checkbox" name="', checkbox, '" value="', checkbox, '">'].join('');
                    }
               }
          }

          //          scriptLogger_global.debug(funcName, [ 'Replace Value:= ', nlapiEscapeXML(replaceValue) ].join(''));

          templateBody = templateBody.replace(replaceTag, replaceValue);
     }

     // check if the record has associated Case Insurance Data
     var caseInsuranceData = getCaseInsuranceData(referalId);

     if (generalFunctions_global.isNullOrUndefined(caseInsuranceData) === true) {

          var caseInsuranceData = {
               'custrecord_bio_fax_insur_bpm_copay': '',
               'custrecordnumb_vial': '',
               'custrecord_bio_fax_insur_primary_bpm': '',
               'custrecord_bio_fax_insur_primary_bpm_id': '',
               'custrecord_bio_fax_insur_bpm_group': '',
               'custrecordpharmacy_bin_number': '',
               'custrecord_bio_fax_insur_pbm_npi_cntrl': '',
               'custrecord_bio_fax_insur_pbm_copay_excep': '',
               'custrecord_bio_fax_insur_relatshp': '',
               'custrecordbio_person_code_ins': '',
               'custrecord_bio_pa_info': '',
               'custrecord_bio_fax_insur_mfg_copay_sub': '',
               'custrecord_bio_net_copay_after_sub': '',
               'custrecord_bio_benfts_autho_vsts_allw': '',
               'custrecord_bio_benfts_autho_no': '',
               'custrecord_bio_biopay_copay_bin': '',
               'custrecord_bio_biopay_copay_id': '',
               'custrecord_bio_biopay_copay_group': ''
          };
     }

     scriptLogger_global.debug(funcName, ['Case Insurance Data:= ', JSON.stringify(caseInsuranceData)].join(''));

     for (var caseInsuranceField in caseInsuranceData) {

          if (capNotEligible == checkboxes['CUSTEVENT_BIO_COPAY_PROGRAM_ELIG']) {
               caseInsuranceData.custrecord_bio_biopay_copay_bin = '&nbsp;';
               caseInsuranceData.custrecord_bio_biopay_copay_id = '&nbsp;';
               caseInsuranceData.custrecord_bio_biopay_copay_group = '&nbsp;';
          }

          var replaceTag = ['<nl', caseInsuranceField, '>'].join('');
          replaceTag = replaceTag.toUpperCase();
          //               scriptLogger_global.debug(funcName, [ 'Tag To Replace:= ', replaceTag ].join(''));

          var replaceValue = caseInsuranceData[caseInsuranceField];

         var forDecryption = [];
         forDecryption.push('custrecord_bio_fax_insur_id_no');
         forDecryption.push('custrecord_bio_fax_insur_primary_bpm_id');
         forDecryption.push('custrecord_bio_fax_insur_bpm_group');
         forDecryption.push('custrecord_bio_fax_insur_grp_no');

         if (forDecryption.indexOf(caseInsuranceField) !== -1) {
             replaceValue = decryptField(replaceValue, caseInsuranceField);
         }
          //               scriptLogger_global.debug(funcName, [ 'Replace Value:= ', replaceValue ].join(''));

          templateBody = templateBody.replace(replaceTag, replaceValue);

          /*
           * This if for the Issue 293213 workaround
           */
          var replaceTag = ['{nl', caseInsuranceField, '}'].join('');
          replaceTag = replaceTag.toUpperCase();
          templateBody = templateBody.replace(replaceTag, replaceValue);
          /*
           * End of Workaround
           */
     }

     return templateBody;
}

function getCaseInsuranceData(referalId) {

    scriptLogger_global.debug('getCaseInsuranceData', 'Getting insurance data');

    var filters = [];
    filters.push(["custrecord_bio_fax_case", null, 'anyof', referalId]);
    filters.push(['isinactive', null, 'is', 'F']);

    var columns = [];
    columns.push(["custrecord_bio_fax_insur_bpm_copay"]);
    columns.push(["custrecordnumb_vial"]);
    columns.push(["custrecord_bio_fax_insur_primary_bpm"]);
    columns.push(["custrecord_bio_fax_insur_primary_bpm_id"]);
    columns.push(["custrecord_bio_fax_insur_bpm_group"]);
    columns.push(["custrecordpharmacy_bin_number"]);
    columns.push(["custrecord_bio_fax_insur_pbm_npi_cntrl"]);
    columns.push(["custrecord_bio_fax_insur_pbm_copay_excep"]);
    columns.push(["custrecord_bio_fax_insur_relatshp"]);
    columns.push(["custrecordbio_person_code_ins"]);
    columns.push(["custrecord_bio_fax_insur_mfg_copay_sub"]);
    columns.push(["custrecord_bio_net_copay_after_sub"]);
    columns.push(["custrecord_bio_benfts_autho_vsts_allw"]);
    columns.push(["custrecord_bio_fax_insur_primary"]);
    columns.push(["custrecord_bio_fax_insur_id_no"]);
    columns.push(["custrecord_bio_fax_insur_grp_no"]);
    columns.push(["custrecord_bio_cardholder_name"]);
    columns.push(["custrecordndc_drug_name"]);
    columns.push(["custrecord_bio_pa_info"]);
    columns.push(["custrecord_bio_benfts_detl_typ"]);
    columns.push(["custrecord_bio_benfts_autho_no"]);

    var result = generalFunctions_global.buildSearch('customrecord3', filters, columns);

    if (generalFunctions_global.numRows(result) > 0) {
        // get the data of the first result
        var caseInsuranceData = {};
        var selectFields = [];
        selectFields.push('custrecord_bio_fax_insur_primary_bpm');
        selectFields.push('custrecord_bio_fax_insur_relatshp');
        selectFields.push('custrecordbio_person_code_ins');
        selectFields.push('custrecord_bio_fax_insur_primary');
        selectFields.push('custrecordndc_drug_name');
        selectFields.push('custrecord_bio_benfts_detl_typ');

        columns.forEach(function (field) {

            var fieldId = field[0];
            if (selectFields.indexOf(fieldId) === -1) {
                caseInsuranceData[fieldId] = result[0].getValue(fieldId);
            } else {
                caseInsuranceData[fieldId] = result[0].getText(fieldId);
            }

            return true;
        });
    }

     return caseInsuranceData;
}

function decryptField (value, fieldName) {

    var funcName = 'decryptField';

    // get the webservice url from the script
    var webServiceURL = executionContext_global.getSetting('script', 'custscript_webservice_url');
    scriptLogger_global.audit(funcName, ['[Script Parameter]Web Service URL:= ', webServiceURL].join(''));

    var decryptionRequest = createFieldDecryptionRequest(value, fieldName);

    var decryptedValue = sendFieldDecryptionRequest(webServiceURL, decryptionRequest);

    if (generalFunctions_global.isEmpty(decryptedValue) === false) {
        if (decryptedValue.indexOf("'") == 0) {
            decryptedValue = decryptedValue.substr(1);
        }
    }

    return decryptedValue;
}

function decryptEmailAttachment(file) {

     var funcName = 'decryptEmailAttachment';

     try {
          // get the webservice url from the script
          var webServiceURL = executionContext_global.getSetting('script', 'custscript_webservice_url');
          scriptLogger_global.audit(funcName, ['[Script Parameter]Web Service URL:= ', webServiceURL].join(''));

          var outputFolder = executionContext_global.getSetting('script', 'custscript_output_folder');
          scriptLogger_global.audit(funcName, ['[Script Parameter]Output Folder:= ', outputFolder].join(''));

          // if folder parameter is empty, use the Suitescript folder
          if (generalFunctions_global.isEmpty(outputFolder) === true) {
               outputFolder = '-15';
          }
          scriptLogger_global.audit(funcName, ['Output Folder To Use:= ', outputFolder].join(''));

          scriptLogger_global.debug(funcName, ['Decrypting File[Internal Id]:= ', file].join(''));

          /**
           * @type nlobjFile
           */
          var encryptedFile = nlapiLoadFile(file);

          // check if the file is in the output folder
          var currentFileFolder = encryptedFile.getFolder();
          scriptLogger_global.debug(funcName, ['Current File Folder:= ', currentFileFolder].join(''));

          var invalidFolders = ['-9', '-15', '257'];
          invalidFolders.push(outputFolder);

          if (generalFunctions_global.inArray(currentFileFolder, invalidFolders) === true) {
               throw nlapiCreateError('99999', 'Location for attached files is invalid.');
          }

          var fileName = encryptedFile.getName();
          scriptLogger_global.debug(funcName, ['File Name:= ', fileName].join(''));

          var fileExtension = getFileExtension(fileName);
          scriptLogger_global.debug(funcName, ['File Extension:= ', fileExtension].join(''));

          var fileType = (encryptedFile.getType() == 'PDF') ? 'PDF' : 'WORD';
          scriptLogger_global.debug(funcName, ['File Type:= ', fileType].join(''));

          // if there is no extension, provide one
          if (generalFunctions_global.isEmpty(fileExtension) === true) {
               fileExtension = (fileType == 'PDF') ? '.pdf' : '.doc';
               scriptLogger_global.debug(funcName, ['New File Extension:= ', fileExtension].join(''));

               // create new filename with extension
               fileName = [fileName, fileExtension].join('');
               scriptLogger_global.debug(funcName, ['New File Name:= ', fileName].join(''));
          }

          var contents = encryptedFile.getValue();

          // build SOAP request
          var soapRequest = createAttachmentDecryptionRequest(contents);
          var wsContents = sendAttachmentDecryptionRequest(webServiceURL, soapRequest);

          if (generalFunctions_global.isEmpty(wsContents) === false) {

               /**
                * @type nlobjFile
                */
               var outputFile = nlapiCreateFile(fileName, fileType, wsContents);
               outputFile.setFolder(outputFolder);
               outputFile.setIsOnline(true);

               var id = nlapiSubmitFile(outputFile);
          }

          return id;
     } catch (error) {
          if (error instanceof nlobjError) {
               nlapiLogExecution('ERROR', funcName, ['Error Code:= ', error.getCode(), '<br/>Error Details:= ', error.getDetails()].join(''));
               throw error;
          } else {
               nlapiLogExecution('ERROR', funcName, ['Error:= ', error.toString()].join(''));
               throw nlapiCreateError('99999', error.toString());
          }
     }
}

function getFileExtension(filename) {

     if (generalFunctions_global.isEmpty(filename) === false) {
          if (filename.indexOf('.') != -1) {
               var a = filename.split(".");

               return a.pop();
          } else {
               return null;
          }
     }
}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 *
 * @appliedtorecord message
 * @param {String} type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
function afterSubmit_inactivateFilesEmail(type) {

     var funcName = 'afterSubmit_inactivateFilesEmail';

     // global object for general functions
     /**
      * @type CommonFunctions
      */
     generalFunctions_global = new NetSuiteCustomLibrary()
          .StandardFunctions();

     // global object for script logging
     /**
      * @type Logger
      */
     scriptLogger_global = new NetSuiteCustomLibrary()
          .ScriptLogger();

     // disable DEBUG logs if Script Log Level is not Debug
     if (executionContext_global.getLogLevel() == 'DEBUG') {
          scriptLogger_global.enableDebug();
     }

     try {
          scriptLogger_global.audit(funcName, '==================== AFTER SUBMIT EVENT START ====================');

          // get the files that was attached to this email
          var attachedFiles = getAttachedFiles();
          var fileCount = generalFunctions_global.numRows(attachedFiles);
          scriptLogger_global.debug(funcName, ['Number of files for email:= ', fileCount].join(''));

          if (fileCount > 0) {
               setFilesToInactive(attachedFiles);
          } else {
               scriptLogger_global.audit(funcName, 'No attachments for the email.');
          }

          scriptLogger_global.audit(funcName, ['=================== AFTER SUBMIT EVENT END  ==================='].join(''));
     } catch (error) {
          if (error instanceof nlobjError) {
               nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
               throw error;
          } else {
               nlapiLogExecution('ERROR', funcName, error.toString());
               throw nlapiCreateError('99999', error.toString());
          }
     }
}

function getAttachedFiles() {

     // get the record id
     var recordId = nlapiGetRecordId();

     // create the search filter
     var filter1 = ['internalid', null, 'anyof', recordId];
     var filter2 = ['messagetype', null, 'anyof', 'EMAIL'];
     var filters = [filter1, filter2];

     // create the search column
     var col0 = ['internalid', 'attachments'];
     var columns = [col0];

     // get all the attachments
     var results = generalFunctions_global.buildSearch('message', filters, columns);

     var attachmentId = [];
     if (generalFunctions_global.isNullOrUndefined(results) === false) {
          if (generalFunctions_global.numRows(results) > 0) {

               for (var x = 0; x < results.length; x++) {
                    var col = results[x].getAllColumns();

                    attachmentId.push(results[x].getValue(col[0]));
               }
          }
     }

     return attachmentId;
}

function setFilesToInactive(attachedFiles) {

     var funcName = 'setFilesToInactive';

     for (var x = 0; x < attachedFiles.length; x++) {

          var attachId = attachedFiles[x];

          if (generalFunctions_global.isEmpty(attachId) === true) {
               continue;
          }

          // set the file to inactive
          /**
           * @type nlobjFile
           */
          var attachmentFile = nlapiLoadFile(attachId);
          attachmentFile.setIsInactive(true);
          nlapiSubmitFile(attachmentFile);
     }

}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 *
 * @appliedtorecord message
 * @param {String} type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @param {nlobjForm} form NetSuite Form Object
 * @returns {Void}
 */
function beforeLoad_hideFieldsEmail(type, form) {

     var funcName = 'beforeLoad_hideFieldsEmail';

     // global object for general functions
     /**
      * @type CommonFunctions
      */
     generalFunctions_global = new NetSuiteCustomLibrary()
          .StandardFunctions();

     // global object for script logging
     /**
      * @type Logger
      */
     scriptLogger_global = new NetSuiteCustomLibrary()
          .ScriptLogger();

     // disable DEBUG logs if Script Log Level is not Debug
     if (executionContext_global.getLogLevel() == 'DEBUG') {
          scriptLogger_global.enableDebug();
     }

     if (type == 'view') {
          return true;
     }
     try {
          scriptLogger_global.audit(funcName, '==================== BEFORE LOAD EVENT START ====================');

          form.setScript('customscript_fax_client_scripts');

          // get the template type
          var templateType = nlapiGetFieldValue('templatetype');
          scriptLogger_global.debug(funcName, ['Template Type:= ', templateType].join(''));

          if (templateType.toUpperCase() != 'EMAIL') { // Do not continue if they are not using an email template
               return true;
          }

          // set the recipient email field blank
          nlapiSetFieldValue('recipientemail','');
          nlapiSetFieldDisplay("crmsdk_mergetypesmessages", false);
          nlapiSetFieldDisplay("crmsdk_mergefieldsmessages", false);
          nlapiSetFieldDisplay("templatelabel", false);
          nlapiSetFieldDisplay("updatetemplate", false);
          nlapiSetFieldDisplay("freemarker_mergetypesmessages", false);
          nlapiSetFieldDisplay("freemarker_mergefieldsmessages", false);
          nlapiSetFieldDisplay("senderaddresslabel", false);

          /**
           * @type nlobjField
           */
          var hideFields = form.addField('custpage_hide_fields_email', 'inlinehtml', 'Hide Fields', null, 'messages');
          var content = '<script>' + 'var porlets = document.getElementsByClassName("uir-field-wrapper uir-inline-tag uir-onoff");' + 'for (var x = 0; x < porlets.length; x++) {' +
               'porlets[x].style.display="none";' + '}' + '</script>';
          hideFields.setDefaultValue(content);

          var content2 = '<div style="color:#0000FF; position: absolute; left: 10px; top: 106px; width: 1045px; height: 250px; background-color:#FFF">""</div>';
          content2 = '<td><span>' + content2 + '</span></td>';

          /**
           * @type nlobjField
           */

          var hideFields2 = form.addField('custpage_hide_fields2_email', 'inlinehtml', 'Hide Fields 2', null, 'messages');
          hideFields2.setDefaultValue(content2);

          var content3 = '<div style="color:#FFF; position: absolute; left: 10px; top: 10px; width: 1045px; height: 85px; background-color:#FFF">""</div>';
          // content4 = '<td><span>' + content4 + '</span></td>';

          var hideAttach = form.addField('custpage_hide_fields_attach', 'inlinehtml', 'Hide Fields', null, 'attachments');
          hideAttach.setDefaultValue(content3);

         // setTimeout(function(){ nlapiSetFieldValue('recipientemail',''); }, 1000);

         var content4 = '<script>' + 'setTimeout(function(){ nlapiSetFieldValue(\'recipientemail\',\'\'); }, 1000);' + '</script>';
         var emptyEmail = form.addField('custpage_empty_recipients', 'inlinehtml', 'Hide Fields', null, 'recipients');
         emptyEmail.setDefaultValue(content4);

          scriptLogger_global.audit(funcName, ['=================== BEFORE LOAD EVENT END ==================='].join(''));
     } catch (error) {
          if (error instanceof nlobjError) {
               nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
               throw error;
          } else {
               nlapiLogExecution('ERROR', funcName, error.toString());
               throw nlapiCreateError('99999', error.toString());
          }
     }
}
/**
 * This function is used to remove any special/reserved RegEx characters.
 *
 * @param  {String} str The string where the reserved characters are to be removed.
 * @return {String}     String without the reserved characters.
 */
function escapeRegExp(str) {
     return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$&");
}
/**
 * This function replaces all the occurence of a specified string.
 *
 * @param  {String} str     The string where the changes will be made.
 * @param  {String|Array} find    The string or array of characters that needs to be found.
 * @param  {String} replace The string that will be put in the place of the find string.
 * @return {String}         The string with the replaced values.
 */
function replaceAll(str, find, replace) {

     // convert the find parameter to array if it is string
     if (find.constructor !== Array) {
          find = find.split('');
     }

     for (var i = 0; i < find.length; i++) {

          escapeRegExp(find[i]);

          str = str.replace(new RegExp(find[i], 'g'), replace);
     }

     return str;
}
