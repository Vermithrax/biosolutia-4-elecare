/* ============================= 
 * | Global Variables          | 
 * =============================
 */
{
	// global object for general functions
	/**
	 * @type CommonFunctions
	 */
	var generalFunctions_global = new NetSuiteCustomLibrary().StandardFunctions();

	// global object for script logging
	/**
	 * @type Logger
	 */
	var scriptLogger_global = new NetSuiteCustomLibrary().ScriptLogger();

	// global stop watch object, for calcuating script runtime
	/**
	 * @type StopWatchObject
	 */
	var stopWatch_global = new NetSuiteCustomLibrary().StopWatch();

	// global object for Execution Context
	/**
	 * @type nlobjContext
	 */
	var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               | 
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |   
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             | 
 * |=======================================================================================================================|
 * | NAME: NS | Update Information                                                                                         |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * | The purpose of this script is to loop through Open Referral records associated with a Patient or Prescriber and update|
 * | the Referral(s). Further, the script also updates the custom Case Insurance Data records associated with each Referral|
 * | as well. It will also update associated Call Tasks. The script will not update the Clinical Data and Assistance       |
 * | records. The script is also designed to reschedule itself when needed should it consume all its allocated usage units |
 * | when invoked where processing is not complete when updating Referral/Case Insurance Data records.                     |                                                                                                 |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           | 
 * | Gerrom Infante     	Mar 28, 2014	1.0			                                                                   | 
 * |_______________________________________________________________________________________________________________________|
 */
/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled_updateInfo (type) {

	var funcName = 'scheduled_updateInfo';

	// enable debug statements if log level is not Debug
	if (executionContext_global.getLogLevel() == 'DEBUG') {
		scriptLogger_global.enableDebug();
	}

	try {
		scriptLogger_global.audit(funcName, '==================== SCHEDULED SCRIPT START ====================');
		stopWatch_global.start();

		var recordId = executionContext_global.getSetting('script', 'custscript_recordid');
		scriptLogger_global.audit(funcName, 'Record ID = ' + recordId);

		var recordType = executionContext_global.getSetting('script', 'custscript_recordtype');
		scriptLogger_global.audit(funcName, 'Record Type = ' + recordType);

		var userEmail = executionContext_global.getSetting('script', 'custscript_user_email');
		scriptLogger_global.audit(funcName, 'User Email = ' + userEmail);

		var userId = executionContext_global.getSetting('script', 'custscript_user_id');
		scriptLogger_global.audit(funcName, 'User Id = ' + userId);

		if (recordType == 'partner') {
			// Update all Patient record associated with the Prescriber
			updatePatientRecords(recordId);
		}

		// search all open Referral and Case Insurance Data records associated with the Patient or Prescriber
		updateReferralRecords(recordId, recordType);

		// send email that update is done
		sendEmail(userId, userEmail, recordId, recordType);

		stopWatch_global.stop();
		scriptLogger_global.audit(funcName, ['=================== SCHEDULED SCRIPT END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
function sendEmail (userId , userEmail , recordId , recordType) {

	var funcName = 'sendEmail';

	// check if sending of email is disabled
	var sendEmails = executionContext_global.getSetting('script', 'custscript_send_emails');
	scriptLogger_global.audit(funcName, 'Send Emails = ' + sendEmails);

	if (sendEmails == 'F') {
		return true;
	}

	generalFunctions_global.rescheduleScript(50);

	// get the record name
	var record = nlapiLoadRecord(recordType, recordId, {
		recordmode: 'dynamic'
	});

	var recordName = record.getFieldValue('entitytitle');

	//compose the email
	var emailText = ['Update for record ', recordName, ' is done.'].join('');

	nlapiSendEmail(userId, userEmail, 'Update Complete', emailText);
}
/**
 * This function will update all the open Referral and associated Case Insurance Data for the passed recordId.
 * 
 * @param {String}
 *            recordId Record Internal Id
 * @param {String}
 *            recordType Record Type Internal Id
 */
function updateReferralRecords (recordId , recordType) {

	var funcName = 'updateReferralRecords';

	scriptLogger_global.audit(funcName, '-------------------- Updating open Referral records. --------------------');

	var caseFilters = [];
	caseFilters.push(new nlobjSearchFilter('status', null, 'anyof', '2'));

	if (recordType == 'partner') {
		caseFilters.push(new nlobjSearchFilter('custevent_bio_physician', null, 'anyof', recordId));
	} else {
		caseFilters.push(new nlobjSearchFilter('internalid', 'customer', 'anyof', recordId));
	}

	// execute the search
	var results = generalFunctions_global.getSearchResults('', 'supportcase', caseFilters);

	// check if there are any search results
	var referralCount = generalFunctions_global.numRows(results);
	if (referralCount > 0) {
		for (var x = 0; x < referralCount; x++) {
			generalFunctions_global.rescheduleScript('100');

			var referralId = results[x].getId();

			// load the record in dynamic mode
			var referral = nlapiLoadRecord('supportcase', referralId, {
				recordmode: 'dynamic'
			});

			var fieldToUpdate = (recordType == 'customer') ? 'company' : 'custevent_bio_physician';

			referral.setFieldValue(fieldToUpdate, recordId);

			// get the patient and prescriber on the referral record
			var patient = referral.getFieldValue('company');
			var prescriber = referral.getFieldValue('custevent_bio_physician');

			var id = nlapiSubmitRecord(referral, true, true);

			scriptLogger_global.debug(funcName, ['Referral Record has been updated.  Internal Id:= ', id].join(''));

			var caseInsuranceId = getCaseInsuranceRecord(referralId);
			// check if there is a Case Insurance Data record associated with the Referral record
			if (generalFunctions_global.isEmpty(caseInsuranceId) === false) {
				updateCaseInsurance(caseInsuranceId, patient, prescriber);
			} else {
				scriptLogger_global.audit(funcName, '-------------------- No Case Insurance Data record assocaiated with the Referral record. --------------------');
			}

			// search for all Phone Calls associated with the Referral record
			updatePhoneCalls(referralId);
		}
	} else {
		scriptLogger_global.audit(funcName, '-------------------- No Open Referral records found. --------------------');
	}
}
function updatePhoneCalls (referralId) {

	var funcName = 'updatePhoneCalls';

	scriptLogger_global.audit(funcName, '-------------------- Updating Phone Calls associated with Referral record. --------------------');

	// search for all phone calls associated with the referral record
	var filter = [new nlobjSearchFilter("internalid", 'case', 'anyof', referralId)];

	// execute the search
	var results = generalFunctions_global.getSearchResults('', 'phonecall', filter);
	var phoneCalls = generalFunctions_global.numRows(results);
	scriptLogger_global.debug(funcName, ['No of Phone Calls for Referral Record:= ', phoneCalls].join(''));

	if (phoneCalls > 0) {
		for (var x = 0; x < phoneCalls; x++) {
			generalFunctions_global.rescheduleScript(100);

			var phoneId = results[x].getId();

			/**
			 * @type nlobjRecord
			 * @record phonecall
			 */
			var phoneRec = nlapiLoadRecord('phonecall', phoneId, {
				recordmode: 'dynamic'
			});
			phoneRec.setFieldValue('supportcase', referralId);
			var id = nlapiSubmitRecord(phoneRec, true, true);
			scriptLogger_global.debug(funcName, ['Phone Call has been updated. Internal Id:= ', id].join(''));
		}
	} else {
		scriptLogger_global.audit(funcName, '-------------------- No Phone Call records found. --------------------');
	}
}
function updateCaseInsurance (caseInsuranceId , patient , prescriber) {

	scriptLogger_global.audit(funcName, '-------------------- Updating Case Insurance Data associated with Referral record. --------------------');

	var funcName = 'updateCaseInsurance';

	// load the record in dynamic mode
	var caseInsurance = nlapiLoadRecord('customrecord3', caseInsuranceId, {
		recordmode: 'dynamic'
	});

	caseInsurance.setFieldValue('custrecord_bio_fax_patient', patient);
	caseInsurance.setFieldValue('custrecord_bio_fax_physician_id', prescriber);

	var id = nlapiSubmitRecord(caseInsurance, true, true);
	scriptLogger_global.debug(funcName, ['Case Insurance Data Record has been updated.  Internal Id:= ', id].join(''));
}
function updatePatientRecords (recordId) {

	var funcName = 'updatePatientRecords';

	scriptLogger_global.audit(funcName, '-------------------- Updating Patient records. --------------------');

	// search for all patient records associated with the partner
	var filter1 = ['partner', null, 'anyof', recordId];
	var searchFilter = [filter1];

	var results = generalFunctions_global.buildSearch('customer', searchFilter);
	// check if there are any search results
	var count = generalFunctions_global.numRows(results);
	if (count > 0) {
		for (var x = 0; x < count; x++) {
			generalFunctions_global.rescheduleScript('100');

			var patientId = results[x].getId();

			var patientRecord = nlapiLoadRecord('customer', patientId, {
				recordmode: 'dynamic'
			});
			patientRecord.setFieldValue('partner', recordId);

			var id = nlapiSubmitRecord(patientRecord, true, true);
			scriptLogger_global.debug(funcName, ['Patient Record has been updated.  Internal Id:= ', id].join(''));
		}
	} else {
		scriptLogger_global.audit(funcName, '-------------------- No Patient records founds. --------------------');
	}
}
function getCaseInsuranceRecord (referralId) {

	var funcName = 'getCaseInsuranceRecord';

	var searchFilter = [];
	searchFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	searchFilter.push(new nlobjSearchFilter('custrecord_bio_fax_case', null, 'anyof', referralId));

	// execute the search
	var results = generalFunctions_global.getSearchResults('', 'customrecord3', searchFilter);

	if (generalFunctions_global.numRows(results) > 0) {
		// there is only one Case Insurance Data record per Referral record
		return results[0].getId();
	} else {
		return '';
	}
}