/* ============================= 
 * | Global Variables          | 
 * =============================
 */
{
	// global object for general functions
	/**
	 * @type CommonFunctions
	 */
	var generalFunctions_global = new NetSuiteCustomLibrary().StandardFunctions();

	// global object for script logging
	/**
	 * @type Logger
	 */
	var scriptLogger_global = new NetSuiteCustomLibrary().ScriptLogger();

	// global stop watch object, for calcuating script runtime
	/**
	 * @type StopWatchObject
	 */
	var stopWatch_global = new NetSuiteCustomLibrary().StopWatch();

	// global object for Execution Context
	/**
	 * @type nlobjContext
	 */
	var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               | 
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |   
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             | 
 * |=======================================================================================================================|
 * | NAME:    NS | Save and Continue                                                                                       |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * |                                                                                                                       |					
 * | This is a client side script that asks the user if they want to continue working with the Referral record or release  | 
 * | it so that it goes back to the Grab Queue.                                                                            |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           | 
 * | Gerrom Infante     	May 13, 2014	1.0			                                                                   | 
 * |_______________________________________________________________________________________________________________________|
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord supportcase
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */
function pageInit_clearContinue (type) {

	nlapiSetFieldValue('custevent_bio_continue', 'F');

}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord supportcase
 * @returns {Boolean} True to continue save, false to abort save
 */
function saveRecord_askContinue () {

	var continueWork = confirm('Continue working with this record?');

	if (continueWork === true) {
		nlapiSetFieldValue('custevent_bio_continue', 'T');
	} else {
		nlapiSetFieldValue('custevent_bio_continue', 'F');
	}

	return true;
}