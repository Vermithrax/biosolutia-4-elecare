/**
 * Copyright (c) 1998-2012 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

var CONST_EVENT_TYPE;

/**
 * Get the event type
 * 
 * @author Aurel Shenne Sinsin 
 * @version 1.0
 */
function pageInit_checkType(stType) 
{
	CONST_EVENT_TYPE = stType;  
}

/** 
 * The script is invoked on the change of the Referral Status (custom) field on the Case record when the Case record is created 
 * or edited.
 * 
 * @author Aurel Shenne Sinsin 
 * @version 1.0
 */
function fieldChanged_updateReferralStatus(stType, stName)
{
	try
	{	
		var stLoggerTitle = 'fieldChanged_updateReferralStatus';		
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Entered Field Changed');
				
		if (CONST_EVENT_TYPE != 'create' && CONST_EVENT_TYPE != 'edit')
		{
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Type of Operation Unsupported. Exit After Field Changed Successfully. Type = ' + CONST_EVENT_TYPE);
			return true;
		}
		
		if (stName != 'custevent_bio_referral_status')
		{			
			return true;
		}
    	
    	// Determine the Referral Status
    	var stReferralStatus = nlapiGetFieldValue('custevent_bio_referral_status');
    	nlapiLogExecution('DEBUG', stLoggerTitle, 'Referral Status = ' + stReferralStatus);
    	
    	var stReferralStatusName = nlapiGetFieldText('custevent_bio_referral_status');
    	nlapiLogExecution('DEBUG', stLoggerTitle, 'Referral Status Name = ' + stReferralStatusName);
    	
    	// Search for the Referral Status from the custom record customrecord_bio_referrel_status
    	var arrFilters = [new nlobjSearchFilter('internalid', null, 'anyof', stReferralStatus)];
    	var arrColumns = [new nlobjSearchColumn('custrecord_bio_ref_status_desc'),
    	                  new nlobjSearchColumn('custrecord_bio_ref_status_color')];
		var arrResults = nlapiSearchRecord('customrecord_bio_referral_status', null, arrFilters, arrColumns);				                  	    		    		
		if (arrResults != null)
		{
			var stDescription = arrResults[0].getValue('custrecord_bio_ref_status_desc');
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Status Description = ' + stDescription);
			var stColor = arrResults[0].getValue('custrecord_bio_ref_status_color');
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Color = ' + stColor);
			
			var stColorCode = getColorCode(stColor);
			
			var stReferralBox = '<table width="40" height="30"><tr><td bgcolor="' + stColorCode + '"></td></tr></table>';
			
			// Set the Referral Box to the Color
			nlapiSetFieldValue('custevent_bio_referral_box', stReferralBox);
		}
		else
		{
			alert('Cannot determine color code and description for Referral Status: ' + stReferralStatusName);
		}
		
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Exit Field Changed');
		
		return true;
	}
   	catch (error)
	{
   		if (error.getDetails != undefined)
        {
            nlapiLogExecution('ERROR','Process Error',error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else
        {
            nlapiLogExecution('ERROR','Unexpected Error',error.toString()); 
            throw nlapiCreateError('99999', error.toString());
        }
	}	
}

/**
 * Get the color code
 * @param stColor
 * @returns {String}
 */
function getColorCode(stColor)
{
	var stColorCode = '#FFFFFF';
	if (stColor == 'Red')
	{
		stColorCode = '#FF0000';
	}
	else if (stColor == 'Green')
	{
		stColorCode = '#008000';
	}
	else if (stColor == 'Yellow')
	{
		stColorCode = '#FFFF00';
	}
	
	return stColorCode;
}