/**
 * Copyright (c) 1998-2012 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

var CONST_EVENT_TYPE;

/**
 * Get the event type
 * 
 * @author Aurel Shenne Sinsin 
 * @version 1.0
 */
function pageInit_checkType(stType) 
{
	CONST_EVENT_TYPE = stType;  
}

/** 
 * The purpose of the script is to display a confirmation message to send fax to physician.
 * 
 * @author Aurel Shenne Sinsin 
 * @version 1.0
 */
function saveRecord_sendFaxToPhysician()
{
	try
	{	
		var stLoggerTitle = 'saveRecord_caseClosedAlert';		
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Entered Save Record');
		
		if (CONST_EVENT_TYPE != 'create')
		{
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Type of Operation Unsupported. Exit After Field Changed Successfully. Type = ' + CONST_EVENT_TYPE);
			return true;
		}
		
		// Display a confirm message
		var stResponse = confirm('Do you wish to send a Referral Confirmation Fax to the Physician?');
		
		// If user click Yes
		if (stResponse == true)
		{
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax will be sent to physician.');
			nlapiSetFieldValue('custevent_bio_send_fax_to_physician', 'T');
		}
		else
		{
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax will not be sent to physician.');
			nlapiSetFieldValue('custevent_bio_send_fax_to_physician', 'F');
		}
		
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Exit Save Record');
		
		return true;
	}
   	catch (error)
	{
   		if (error.getDetails != undefined)
        {
            nlapiLogExecution('ERROR','Process Error',error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else
        {
            nlapiLogExecution('ERROR','Unexpected Error',error.toString()); 
            throw nlapiCreateError('99999', error.toString());
        }
	}	
}