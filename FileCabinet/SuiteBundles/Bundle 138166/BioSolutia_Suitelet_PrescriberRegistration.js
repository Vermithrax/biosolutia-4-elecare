/* =============================
 * | Global Variables          |
 * =============================
 */
{
	// global object for general functions
	/**
	 * @type CommonFunctions
	 */
	var generalFunctions_global = new NetSuiteCustomLibrary()
		.StandardFunctions();

	// global object for script logging
	/**
	 * @type Logger
	 */
	var scriptLogger_global = new NetSuiteCustomLibrary()
		.ScriptLogger();

	// global stop watch object, for calcuating script runtime
	/**
	 * @type StopWatchObject
	 */
	var stopWatch_global = new NetSuiteCustomLibrary()
		.StopWatch();

	// global object for Execution Context
	/**
	 * @type nlobjContext
	 */
	var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               |
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             |
 * |=======================================================================================================================|
 * | NAME:                                                                                                                 |
 * |                                                                                                                       |
 * | OVERVIEW:                                                                                                             |
 * |                                                                                                                       |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                          |
 * | Gerrom Infante     	     Apr 21, 2014	1.0			                                                               |
 * |_______________________________________________________________________________________________________________________|
 */
/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void}
 */
function suitelet_prescriberReg(request, response) {

	var funcName = 'suitelet_prescriberReg';

	// disable DEBUG logs if Script Log Level is not Debug
	if (executionContext_global.getLogLevel() == 'DEBUG') {
		scriptLogger_global.enableDebug();
	}

	try {
		scriptLogger_global.audit(funcName, '==================== SUITELET START ====================');
		stopWatch_global.start();

		var stage = request.getParameter('custpage_stage');

		var regForm;
		if (generalFunctions_global.isEmpty(stage) === true) {
			scriptLogger_global.debug(funcName, '-------------------- Generating Registration Page. --------------------');
			regForm = createRegistrationForm();
		} else if (stage == 'submit') {
			scriptLogger_global.debug(funcName, '-------------------- Creating Presciber Record. --------------------');

			var error = createPrescriberRecord(request);
			if (generalFunctions_global.isEmpty(error) === true) {
				regForm = createRegistrationForm(
					'Your request has been submitted. If you have more than one Prescriber please repeat the process as instructed below. Otherwise you should receive your User Account information via e-mail within one business day or less. Thank You!'
				);
			} else {
				regForm = createRegistrationForm(error);
			}

		} else {
			scriptLogger_global.debug(funcName, '-------------------- Generating Registration Page. --------------------');
			regForm = createRegistrationForm();
		}

		response.writePage(regForm);

		stopWatch_global.stop();
		scriptLogger_global.audit(funcName, ['=================== SUITELET END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
/**
 * This function creates the 'Registration' form
 *
 * @returns {nlobjForm}
 */
function createRegistrationForm(error) {

	var funcName = 'createRegistrationForm';

	// get the image file for the form
	var imageFile = executionContext_global.getSetting('script', 'custscript_reg_image');

	// Get Netsuite URL
	// var netsuiteUrl = generalFunctions_global.getNetsuiteURL(false);

	// get the url of the image
	var image = nlapiLoadFile(imageFile);
	var image_url = nlapiEscapeXML(image.getURL());

	//  var imageUrl = [netsuiteUrl, image_url].join('');

	var imageFieldValue = ['<img src="', image_url, '" height="95">'].join('');

	/**
	 * @type nlobjForm
	 */
	var regForm = nlapiCreateForm('');

	regForm.setScript('customscript_registration_client');

	/**
	 * @type nlobjField
	 */
	var stage = regForm.addField('custpage_stage', 'text', 'Stage', null);
	stage.setDefaultValue('submit');
	stage.setDisplayType('hidden');

	/**
	 * @type nlobjFieldGroup
	 */
	var practiceGroup = regForm.addFieldGroup('custpage_practice_grp', 'Practice');
	practiceGroup.setShowBorder(false);
	practiceGroup.setSingleColumn(true);

	regForm.addField('custpage_line4', 'inlinehtml', '', null, 'custpage_practice_grp')
		.setDefaultValue('<hr color="#0f629b" align="Center" size="0">');

	// show banner image
	/**
	 * @type nlobjField
	 */
	var imageField = regForm.addField('custpage_image', 'inlinehtml', '', null, 'custpage_practice_grp');
	imageField.setDefaultValue(imageFieldValue);
	imageField.setLayoutType('startrow');

	regForm.addField('custpage_line5', 'inlinehtml', '', null, 'custpage_practice_grp')
		.setDefaultValue('<hr color="#0f629b" align="Center" size="0">');

	// registration label
	/**
	 * @type nlobjField
	 */
	var requestField = regForm.addField('custpage_request', 'inlinehtml', '', null, 'custpage_practice_grp');
	requestField.setDefaultValue('<br/><font size="5" color="#0f629b">Request Account</font><br/>');
	requestField.setLayoutType('startrow');

	regForm.addField('custpage_line1', 'inlinehtml', '', null, 'custpage_practice_grp')
		.setDefaultValue('<hr color="#dcdfea" align="Center" size="0">');

	/**
	 * @type nlobjField
	 */
	var practiceName = regForm.addField('custpage_practice_name', 'text', 'Practice Name', null, 'custpage_practice_grp');
	practiceName.setDefaultValue('');
	practiceName.setMandatory(true);
	practiceName.setLayoutType('startrow');

	regForm.addField('custpage_help1', 'label', 'Enter the practice name.', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	regForm.addField('custpage_line2', 'inlinehtml', '', null, 'custpage_practice_grp')
		.setDefaultValue('<hr color="#dcdfea" align="Center" size="0">');

	/**
	 * @type nlobjField
	 */
	var prescriberFirstName = regForm.addField('custpage_prescriber_firstname', 'text', 'Prescriber First Name', null, 'custpage_practice_grp');
	prescriberFirstName.setDefaultValue('');
	prescriberFirstName.setMandatory(true);
	prescriberFirstName.setLayoutType('startrow');
	regForm.addField('custpage_help2', 'label', 'Enter the prescriber\'s first name.', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	/**
	 * @type nlobjField
	 */
	var prescriberLastName = regForm.addField('custpage_prescriber_lastname', 'text', 'Prescriber Last Name', null, 'custpage_practice_grp');
	prescriberLastName.setDefaultValue('');
	prescriberLastName.setMandatory(true);
	prescriberLastName.setLayoutType('startrow');
	regForm.addField('custpage_help3', 'label', 'Enter the prescriber\'s last name.', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	/**
	 * @type nlobjField
	 */
	var userFirstName = regForm.addField('custpage_user_firstname', 'text', 'User First Name', null, 'custpage_practice_grp');
	userFirstName.setDefaultValue('');
	userFirstName.setMandatory(true);
	userFirstName.setLayoutType('startrow');
	regForm.addField('custpage_help4', 'label', 'Enter the user\'s first name.', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	/**
	 * @type nlobjField
	 */
	var userLastName = regForm.addField('custpage_user_lastname', 'text', 'User Last Name', null, 'custpage_practice_grp');
	userLastName.setDefaultValue('');
	userLastName.setMandatory(true);
	userLastName.setLayoutType('startrow');
	regForm.addField('custpage_help5', 'label', 'Enter the user\'s last name.', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	/**
	 * @type nlobjField
	 */
	var userEmail = regForm.addField('custpage_user_email', 'email', 'User Email', null, 'custpage_practice_grp');
	userEmail.setDefaultValue('');
	userEmail.setMandatory(true);
	userEmail.setLayoutType('startrow');
	regForm.addField('custpage_help6', 'label', 'Enter the user\'s E-mail address.', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	/**
	 * @type nlobjField
	 */
	var userPhoneNo = regForm.addField('custpage_user_phone', 'phone', 'User Phone No.', null, 'custpage_practice_grp');
	userPhoneNo.setDefaultValue('');
	userPhoneNo.setMandatory(true);
	userPhoneNo.setLayoutType('startrow');
	regForm.addField('custpage_help7', 'label', 'Enter the user\'s phone number.', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	// checkbox for the indicating if request was sent
	/**
	 * @type nlobjField
	 */
	var requestSent = regForm.addField('custpage_request_sent', 'select', 'Number of Prescribers', null, 'custpage_practice_grp');
	requestSent.addSelectOption('', '', true);
	requestSent.addSelectOption('One', 'One');
	requestSent.addSelectOption('Many', 'Many');
	requestSent.setMandatory(true);
	requestSent.setLayoutType('startrow');
	regForm.addField('custpage_help8', 'label', 'If there is more than one Prescriber to associate with this User Account, choose "Many".', null, 'custpage_practice_grp')
		.setLayoutType('midrow');

	regForm.addField('custpage_line3', 'inlinehtml', '', null, 'custpage_practice_grp')
		.setDefaultValue('<hr color="#dcdfea" align="Center" size="0">');

	regForm.addField('custpage_text1', 'inlinehtml', '', null, 'custpage_practice_grp')
		.setDefaultValue(
			'<p align = Center><font size = "1" face = "Arial">If there is more than one Prescriber to associate with this User Account, 1) For the "Number of Prescribers" field, choose "Many", otherwise choose "One" 2) Click "Submit" 3) Enter information for each additional Prescriber. User information can remain the same but must be re-entered each time. All Prescribers entered will then be linked as individual choices under one User Account</font></p>'
		);

	if (generalFunctions_global.isEmpty(error) === false) {

		var suiteletUrl = nlapiResolveURL('suitelet', executionContext_global.getScriptId(), executionContext_global.getDeploymentId(), true);
		scriptLogger_global.debug(funcName, ['Suitelet External Link:= ', suiteletUrl].join(''));

		var errorhtml = ['<script type="text/javascript"> alert("', error, '");window.location.assign("', suiteletUrl, '");</script>'].join('');
		scriptLogger_global.debug(funcName, ['Error HTML:= ', nlapiEscapeXML(errorhtml)].join(''));

		// display error message
		/**
		 * @type nlobjField
		 */
		var fldError = regForm.addField('custpage_error_message', 'inlinehtml', '', 'custpage_practice_grp');
		fldError.setDefaultValue(errorhtml);
		fldError.setLayoutType('startrow');
		fldError.setPadding(1);
	}

	regForm.addSubmitButton(' Submit ');

	var mainPageUrl = nlapiGetContext()
		.getSetting('script', 'custscript_landing_page');
	nlapiLogExecution('debug', funcName, ['Landing Page URL:= ', mainPageUrl].join(''));

	var callScript = ["backToLanding('", mainPageUrl, "');"].join('');
	nlapiLogExecution('debug', funcName, ['Script Call:= ', callScript].join(''));

	regForm.addButton('custpage_all_done', ' Cancel/Return to Main Menu ', callScript);

	return regForm;
}
/**
 * This function creates the Partner record and the Registration Prescribers custom record
 *
 * @param {nlobjResponse}
 *            response Response object
 * @returns {String}
 */
function createPrescriberRecord(response) {

	var funcName = 'createPrescriberRecord';

	// get the values that you have submitted
	var practiceName = response.getParameter('custpage_practice_name');
	var prescriberFirstName = response.getParameter('custpage_prescriber_firstname');
	var prescriberLastName = response.getParameter('custpage_prescriber_lastname');
	var userFirstName = response.getParameter('custpage_user_firstname');
	var userLastName = response.getParameter('custpage_user_lastname');
	var userEmail = response.getParameter('custpage_user_email');
	var userPhone = response.getParameter('custpage_user_phone');
	var formSubmitted = response.getParameter('custpage_request_sent');

	var apcUser = executionContext_global.getSetting('script', 'custscript_apc_user');
	var regEmail = executionContext_global.getSetting('script', 'custscript_reg_email');
	var senderId = executionContext_global.getSetting('script', 'custscript_sender_id');
	scriptLogger_global.debug(funcName, ['Practice Name:= ', practiceName, '<br/>Prescriber First Name:= ', prescriberFirstName, '<br/>User First Name:= ', userFirstName, '<br/>User Last Name:= ',
		userLastName, '<br/>User Email:= ', userEmail, '<br/>User Phone:= ', userPhone, '<br/>Form Already Submitted:= ', formSubmitted, '<br/>[Script Parameter]APC User Type(Internal Id):= ', apcUser,
		'<br/>[Script Parameter]New Registration Email Address:= ', regEmail, '<br/>[Script Parameter]New Registration Email Sender(Internal Id):= ', senderId].join(''));

	if (formSubmitted.toUpperCase() != 'MANY') { // form was submitted the first time
		scriptLogger_global.debug(funcName, '-------------------- Creating Prescriber record. --------------------');

		// create the Prescriber record
		/**
		 * @type nlobjRecord
		 * @record partner
		 */
		var partnerRec = nlapiCreateRecord('partner', {
			recordmode: 'dynamic'
		});

		partnerRec.setFieldValue('firstname', userFirstName);
		partnerRec.setFieldValue('lastname', userLastName);
		partnerRec.setFieldValue('email', userEmail);
		partnerRec.setFieldValue('homephone', userPhone);
		partnerRec.setFieldValue('companyname', practiceName);
		partnerRec.setFieldValue('fax', '(111) 111-1111'); // fax defaulted to (111) 111-1111
		partnerRec.setFieldValue('custentity_apc_user', apcUser);

		var apcText = partnerRec.getFieldText('custentity_apc_user');

		partnerRec.setFieldValue('custentity_bio_phys_npi', apcText);

		// add APC Text to the address line 1
		partnerRec.selectNewLineItem('addressbook');
		partnerRec.setCurrentLineItemValue('addressbook', 'addr1', apcText);
		partnerRec.commitLineItem('addressbook');

		// save record
		var partnerId = nlapiSubmitRecord(partnerRec, true, true);
		scriptLogger_global.audit(funcName, ['Prescriber record has been created. Internal Id:= ', partnerId].join(''));

		var error = '';
		var newUser = true;
	} else { // form was already submitted
		scriptLogger_global.debug(funcName, '-------------------- Finding Prescriber record using entered email. --------------------');

		// search for a partner record with the same email
		var filter1 = ['email', null, 'is', userEmail];
		var filters = [filter1];

		var results = generalFunctions_global.buildSearch('partner', filters, null);

		if (generalFunctions_global.numRows(results) > 0) {
			// always get the first result
			var partnerId = results[0].getId();
			scriptLogger_global.audit(funcName, ['Prescriber record found. Internal Id:= ', partnerId].join(''));
			var error = '';
		} else {
			scriptLogger_global.debug(funcName, '-------------------- Creating Prescriber record. --------------------');

			// create the Prescriber record
			/**
			 * @type nlobjRecord
			 * @record partner
			 */
			var partnerRec = nlapiCreateRecord('partner', {
				recordmode: 'dynamic'
			});

			partnerRec.setFieldValue('firstname', userFirstName);
			partnerRec.setFieldValue('lastname', userLastName);
			partnerRec.setFieldValue('email', userEmail);
			partnerRec.setFieldValue('companyname', practiceName);
			partnerRec.setFieldValue('homephone', userPhone);
			partnerRec.setFieldValue('fax', '(111) 111-1111'); // fax defaulted to (111) 111-1111
			partnerRec.setFieldValue('custentity_apc_user', apcUser);

			var apcText = partnerRec.getFieldText('custentity_apc_user');

			partnerRec.setFieldValue('custentity_bio_phys_npi', apcText);

			// add APC Text to the address line 1
			partnerRec.selectNewLineItem('addressbook');
			partnerRec.setCurrentLineItemValue('addressbook', 'addr1', apcText);
			partnerRec.commitLineItem('addressbook');

			// save record
			var partnerId = nlapiSubmitRecord(partnerRec, true, true);
			scriptLogger_global.audit(funcName, ['Prescriber record has been created. Internal Id:= ', partnerId].join(''));

			var newUser = true;
		}
	}

	if (generalFunctions_global.isEmpty(error) === true) {
		// create the Registration Prescriber record
		/**
		 * @type nlobjRecord
		 * @record customrecord_arestin_reg_prescriber
		 */
		var regPrescriber = nlapiCreateRecord('customrecord_arestin_reg_prescriber');
		regPrescriber.setFieldValue('custrecord_reg_prescriber', partnerId);
		regPrescriber.setFieldValue('custrecord_reg_practice_name', practiceName);
		regPrescriber.setFieldValue('custrecord_reg_pres_first_name', prescriberFirstName);
		regPrescriber.setFieldValue('custrecord_reg_pres_last_name', prescriberLastName);

		var id = nlapiSubmitRecord(regPrescriber, true, true);

		// send email to indicate that a new Prescriber was entered
		var prescriberData = nlapiLookupField('partner', partnerId, ['entityid', 'firstname', 'lastname']);

		var emailData = [prescriberData.entityid, ' ', prescriberData.firstname, ' ', prescriberData.lastname].join('');

		if (newUser === true) {
			var emailBody = ['A new Arestin Access Request has been submitted for ', emailData].join('');

			nlapiSendEmail(senderId, regEmail, 'New Arestin Request', emailBody);
			scriptLogger_global.debug(funcName, '-------------------- Email sent out. --------------------');
		} else {
			var prescriberData = [practiceName, '-', prescriberFirstName, ' ', prescriberLastName].join('');

			var emailBody = ['A new Prescriber, ', prescriberData, ' has been submitted for ', emailData].join('');

			nlapiSendEmail(senderId, regEmail, 'New Arestin Request', emailBody);
			scriptLogger_global.debug(funcName, '-------------------- Email sent out. --------------------');
		}
	}

	return error;
}

function backToLanding(mainPageUrl) {

	var funcName = 'backToLanding';

	window.open(mainPageUrl, "_parent");
}
