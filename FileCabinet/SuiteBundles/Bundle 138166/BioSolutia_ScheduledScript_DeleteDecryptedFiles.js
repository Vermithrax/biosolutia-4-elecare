/* ============================= 
 * | Global Variables          | 
 * =============================
 */
{
	// global object for general functions
	/**
	 * @type CommonFunctions
	 */
	var generalFunctions_global = new NetSuiteCustomLibrary().StandardFunctions();

	// global object for script logging
	/**
	 * @type Logger
	 */
	var scriptLogger_global = new NetSuiteCustomLibrary().ScriptLogger();

	// global stop watch object, for calcuating script runtime
	/**
	 * @type StopWatchObject
	 */
	var stopWatch_global = new NetSuiteCustomLibrary().StopWatch();

	// global object for Execution Context
	/**
	 * @type nlobjContext
	 */
	var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               | 
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |   
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             | 
 * |=======================================================================================================================|
 * | NAME: NS | Delete Decrypted Fax Attachments                                                                             |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * | This is a scheduled script that will run when the BioSolutia office is closed to delete all the decrypted files that  |
 * | were created by the NS | Decrypt Fax Attachments script.  Files to be deleted will be identified by a scheduled       |
 * | script.                                                                                                               |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           | 
 * | Gerrom Infante     	Mar 20, 2014	1.0			                                                                   | 
 * |_______________________________________________________________________________________________________________________|
 */
/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled_deleteDecrypted (type) {

	var funcName = 'scheduled_deleteDecrypted';

	// enable debug statements if log level is not Debug
	if (executionContext_global.getLogLevel() == 'DEBUG') {
		scriptLogger_global.enableDebug();
	}

	try {
		scriptLogger_global.audit(funcName, '==================== SCHEDULED SCRIPT START ====================');
		stopWatch_global.start();

		// get the search to be used from the Company Preference
		var savedSearch = executionContext_global.getSetting('script', 'custscript_delete_docs_search');
		scriptLogger_global.audit(funcName, ['[Script Parameter]Saved Search To Use[Internal Id]:= ', savedSearch].join(''));

		if (generalFunctions_global.isEmpty(savedSearch) === true) {
			scriptLogger_global.audit(funcName, '-------------------- No Saved Search provided, terminating script. --------------------');
			stopWatch_global.stop();
			scriptLogger_global.audit(funcName, ['=================== SCHEDULED SCRIPT END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
			return true;
		}

		var recordsToProcess = generalFunctions_global.getSearchResults(savedSearch, null, null, null);
		var count = generalFunctions_global.numRows(recordsToProcess);
		scriptLogger_global.audit(funcName, ['Number of records found:= ', savedSearch].join(''));

		// check if records were found
		if (count < 1) {
			scriptLogger_global.audit(funcName, '-------------------- No records found, terminating script. --------------------');
			stopWatch_global.stop();
			scriptLogger_global.audit(funcName, ['=================== SCHEDULED SCRIPT END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
			return true;
		}

		// release the records found
		deleteDecryptedFiles(recordsToProcess);

		stopWatch_global.stop();
		scriptLogger_global.audit(funcName, ['=================== SCHEDULED SCRIPT END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
function deleteDecryptedFiles (records) {

	var funcName = 'deleteDecryptedFiles';

	for (var x = 0; x < records.length; x++) {
		var recordId = records[x].getId();

		generalFunctions_global.rescheduleScript(100);

		// delete the file
		nlapiDeleteFile(recordId);

		scriptLogger_global.debug(funcName, ['File has been deleted.  Internal Id:= ', recordId].join(''));
	}
}