/* ============================= 
 * | Global Variables          | 
 * =============================
 */
{
	// global object for general functions
	/**
	 * @type CommonFunctions
	 */
	var generalFunctions_global = new NetSuiteCustomLibrary().StandardFunctions();

	// global object for script logging
	/**
	 * @type Logger
	 */
	var scriptLogger_global = new NetSuiteCustomLibrary().ScriptLogger();

	// global object for Execution Context
	/**
	 * @type nlobjContext
	 */
	var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               | 
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |   
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             | 
 * |=======================================================================================================================|
 * | NAME:                                                                                                                 |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * |                                                                                                                       |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           | 
 * | Gerrom Infante     	May 01, 2014	1.0			                                                                   | 
 * |_______________________________________________________________________________________________________________________|
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */
function pageInit_copyPhone (type) {

	var funcName = 'pageInit_copyPhone';

	try {
		scriptLogger_global.enableDebug();

		scriptLogger_global.audit(funcName, '==================== PAGE INIT EVENT START ====================');
		var stopWatch = new NetSuiteCustomLibrary().StopWatch();
		stopWatch.start();

		// check if default phone field is blank
		var defaultPhone = nlapiGetFieldValue('phone');
		scriptLogger_global.debug(funcName, ['Default Phone:= ', defaultPhone].join(''));

		if (generalFunctions_global.isEmpty(defaultPhone) === true) {
			// copy the encrypted phone to the phone field
			var encryptedPhone = nlapiGetFieldValue('custentity_bio_phone');
			scriptLogger_global.debug(funcName, ['Encrypted:= ', encryptedPhone].join(''));

			if (generalFunctions_global.isEmpty(encryptedPhone) === false) {
				nlapiSetFieldValue('phone', encryptedPhone, false, true);
				scriptLogger_global.debug(funcName, ['Set Default Phone:= ', encryptedPhone].join(''));
			}
		}

		/*		// copy DOB to hidden custom DOB field
				var defaultDOB = nlapiGetFieldValue('custentity_bio_date_of_birth');
				scriptLogger_global.debug(funcName, ['Default DOB:= ', defaultDOB].join(''));

				if (generalFunctions_global.isEmpty(defaultDOB) === false) {
					var hiddenDob = nlapiGetFieldValue('custentity_bio_hidden_dob');
					scriptLogger_global.debug(funcName, ['Hidden DOB:= ', hiddenDob].join(''));

					if (generalFunctions_global.isEmpty(hiddenDob) === true) {
						nlapiSetFieldValue('custentity_bio_hidden_dob', defaultDOB, false, true);
						scriptLogger_global.debug(funcName, ['Set Hidden DOB:= ', defaultDOB].join(''));
					}
				}*/

		stopWatch.stop();
		scriptLogger_global.audit(funcName, ['=================== PAGE INIT EVENT END ', '[Running Time:= ', stopWatch.duration(), '] ==================='].join(''));
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function fieldChanged_copyData (type , name , linenum) {

	if (name != 'custentity_bio_date_of_birth') {
		return true;
	}

	var funcName = 'fieldChanged_copyData';

	try {
		scriptLogger_global.enableDebug();

		scriptLogger_global.audit(funcName, '==================== FIELD CHANGED EVENT START ====================');
		var stopWatch = new NetSuiteCustomLibrary().StopWatch();
		stopWatch.start();

		// copy DOB to hidden custom DOB field
		var defaultDOB = nlapiGetFieldValue('custentity_bio_date_of_birth');
		scriptLogger_global.debug(funcName, ['Default DOB:= ', defaultDOB].join(''));

		if (generalFunctions_global.isEmpty(defaultDOB) === false) {
			nlapiSetFieldValue('custentity_bio_hidden_dob', defaultDOB, false, true);
			scriptLogger_global.debug(funcName, ['Set Hidden DOB:= ', defaultDOB].join(''));
		}

		stopWatch.stop();
		scriptLogger_global.audit(funcName, ['=================== FIELD CHANGED EVENT END ', '[Running Time:= ', stopWatch.duration(), '] ==================='].join(''));
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}