/**
 * Copyright (c) 1998-2012 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

/** 
 * The purpose of the script is to set appropriate value on Fax Template related fields on the initialization of Referral record
 * 
 * @author Aurel Shenne Sinsin 
 * @version 1.0
 */
function pageInit_faxTemplateFieldsProcessing(stType)
{
	try
	{
		if (stType != 'edit')
		{			
			return;
		}
		
		var stLoggerTitle = 'pageInit_faxTemplateFieldsProcessing';		
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Entered Page Init');
		
		var stDiagnosisGroupForEmailFax = nlapiGetFieldText('custevent_bio_diagnosis_for_email_fax');
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Diagnosis Group for Email/Fax = ' + stDiagnosisGroupForEmailFax);
		
		if (isEmpty(stDiagnosisGroupForEmailFax))
		{
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Diagnosis Group for Email/Fax is empty. Exit Page Init');	
			return;
		}
		
		var stPrimaryDignosisGroup = '';
		var arrFilters = [new nlobjSearchFilter('name', null, 'is', stDiagnosisGroupForEmailFax)];
		var arrResults = nlapiSearchRecord('customrecord_bio_diagnosis', null, arrFilters);				                  	    		    		
		if (arrResults == null)
		{
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Diagnosis Group for Email/Fax is empty. Exit Page Init');	
			return;
		}
		else
		{
			stPrimaryDignosisGroup = arrResults[0].getId();
			nlapiLogExecution('DEBUG', stLoggerTitle, 'Primary Diagnosis = ' + stPrimaryDignosisGroup);
		}		
		
		var stFaxTemplateName = '';
		var arrFilters = [new nlobjSearchFilter('custrecord_bio_diagnosis_group', null, 'anyof', stPrimaryDignosisGroup)];
		var arrColumns = [new nlobjSearchColumn('custrecord_bio_fax_template_name')];
		var arrResults = nlapiSearchRecord('customrecord_bio_fax_template_content', null, arrFilters, arrColumns);				                  	    		    		
		if (arrResults != null)
		{
			//stFaxTemplateName = arrResults[0].getValue('custrecord_bio_fax_template_name');
			stFaxTemplateName = arrResults[0].getId();
		}
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax Template Name = ' + stFaxTemplateName);
		
		nlapiSetFieldValue('custevent_bio_fax_template_internal_id', stFaxTemplateName);
		
		if (nlapiGetFieldValue('custevent_bio_incl_para_one_in_fax') == 'T')
		{
			nlapiSetFieldValue('custevent_fax_temp_para_one_html_chkbox', 'checked');
		}
		else
		{
			nlapiSetFieldValue('custevent_fax_temp_para_one_html_chkbox', '');
		}
		
		if (nlapiGetFieldValue('custevent_bio_incl_para_two_in_fax') == 'T')
		{
			nlapiSetFieldValue('custevent_fax_temp_para_two_html_chkbox', 'checked');
		}
		else
		{
			nlapiSetFieldValue('custevent_fax_temp_para_two_html_chkbox', '');
		}
		
		if (nlapiGetFieldValue('custevent_bio_incl_para_three_in_fax') == 'T')
		{
			nlapiSetFieldValue('custevent_fax_temp_para_three_html_box', 'checked');
		}
		else
		{
			nlapiSetFieldValue('custevent_fax_temp_para_three_html_box', '');
		}
		
		if (nlapiGetFieldValue('custevent_bio_incl_para_four_in_fax') == 'T')
		{
			nlapiSetFieldValue('custevent_fax_temp_para_four_html_box', 'checked');
		}
		else
		{
			nlapiSetFieldValue('custevent_fax_temp_para_four_html_box', '');
		}
		
		if (nlapiGetFieldValue('custevent_bio_incl_para_five_in_fax') == 'T')
		{
			nlapiSetFieldValue('custevent_fax_temp_para_five_html_box', 'checked');
		}
		else
		{
			nlapiSetFieldValue('custevent_fax_temp_para_five_html_box', '');
		}
		 
		nlapiLogExecution('DEBUG', stLoggerTitle, 'Exit Page Init');		
		return true;
	}
   	catch (error)
	{
   		if (error.getDetails != undefined)
        {
            nlapiLogExecution('ERROR','Process Error',error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else
        {
            nlapiLogExecution('ERROR','Unexpected Error',error.toString()); 
            throw nlapiCreateError('99999', error.toString());
        }
	}	
}

/** 
 * The purpose of the script is to set appropriate value on Fax Template related fields on field changed of key fields
 * 
 * @author Aurel Shenne Sinsin 
 * @version 1.0
 */
function fieldChanged_faxTemplateFieldsProcessing(stType, stName)
{
	try
	{	
		if (stName != 'custevent_bio_incl_para_one_in_fax' && stName != 'custevent_bio_incl_para_two_in_fax' 
			&& stName != 'custevent_bio_incl_para_three_in_fax' && stName != 'custevent_bio_incl_para_four_in_fax'
			&& stName != 'custevent_bio_incl_para_five_in_fax' && stName != 'custevent_bio_diagnosis_for_email_fax')
		{			
			return true;
		}    	
		else if (stName == 'custevent_bio_incl_para_one_in_fax')
		{
			if (nlapiGetFieldValue('custevent_bio_incl_para_one_in_fax') == 'T')
			{
				nlapiSetFieldValue('custevent_fax_temp_para_one_html_chkbox', 'checked');
			}
			else
			{
				nlapiSetFieldValue('custevent_fax_temp_para_one_html_chkbox', '');
			}
		}
		else if (stName == 'custevent_bio_incl_para_two_in_fax')
		{
			if (nlapiGetFieldValue('custevent_bio_incl_para_two_in_fax') == 'T')
			{
				nlapiSetFieldValue('custevent_fax_temp_para_two_html_chkbox', 'checked');
			}
			else
			{
				nlapiSetFieldValue('custevent_fax_temp_para_two_html_chkbox', '');
			}
		}
		else if (stName == 'custevent_bio_incl_para_three_in_fax')
		{
			if (nlapiGetFieldValue('custevent_bio_incl_para_three_in_fax') == 'T')
			{
				nlapiSetFieldValue('custevent_fax_temp_para_three_html_box', 'checked');
			}
			else
			{
				nlapiSetFieldValue('custevent_fax_temp_para_three_html_box', '');
			}
		}
		else if (stName == 'custevent_bio_incl_para_four_in_fax')
		{
			if (nlapiGetFieldValue('custevent_bio_incl_para_four_in_fax') == 'T')
			{
				nlapiSetFieldValue('custevent_fax_temp_para_four_html_box', 'checked');
			}
			else
			{
				nlapiSetFieldValue('custevent_fax_temp_para_four_html_box', '');
			}
		}
		else if (stName == 'custevent_bio_incl_para_five_in_fax')
		{
			if (nlapiGetFieldValue('custevent_bio_incl_para_five_in_fax') == 'T')
			{
				nlapiSetFieldValue('custevent_fax_temp_para_five_html_box', 'checked');
			}
			else
			{
				nlapiSetFieldValue('custevent_fax_temp_para_five_html_box', '');
			}
		}
		else if (stName == 'custevent_bio_diagnosis_for_email_fax')
		{
			var stDiagnosisGroupForEmailFax = nlapiGetFieldText('custevent_bio_diagnosis_for_email_fax');
			
			if (isEmpty(stDiagnosisGroupForEmailFax))
			{
				return true;
			}
			
			var stPrimaryDignosisGroup = '';
			var arrFilters = [new nlobjSearchFilter('name', null, 'is', stDiagnosisGroupForEmailFax)];
			var arrResults = nlapiSearchRecord('customrecord_bio_diagnosis', null, arrFilters);				                  	    		    		
			if (arrResults == null)
			{					
				return;
			}
			else
			{
				stPrimaryDignosisGroup = arrResults[0].getId();				
			}		
			
			var stFaxTemplateName = '';
			var arrFilters = [new nlobjSearchFilter('custrecord_bio_diagnosis_group', null, 'anyof', stPrimaryDignosisGroup)];
			var arrColumns = [new nlobjSearchColumn('custrecord_bio_fax_template_name')];
			var arrResults = nlapiSearchRecord('customrecord_bio_fax_template_content', null, arrFilters, arrColumns);				                  	    		    		
			if (arrResults != null)
			{
				//stFaxTemplateName = arrResults[0].getValue('custrecord_bio_fax_template_name');
			        stFaxTemplateName = arrResults[0].getId();
			}
			
			nlapiSetFieldValue('custevent_bio_fax_template_internal_id', stFaxTemplateName);
		}
		
		return true;
	}
   	catch (error)
	{
   		if (error.getDetails != undefined)
        {
            nlapiLogExecution('ERROR','Process Error',error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else
        {
            nlapiLogExecution('ERROR','Unexpected Error',error.toString()); 
            throw nlapiCreateError('99999', error.toString());
        }
	}	
}

/**
 * Check if value is empty
 * @param stValue
 * @returns {Boolean}
 */
function isEmpty(stValue)
{
	if ((stValue == '') || (stValue == null) ||(stValue == undefined))
    {
        return true;
    }
    
    return false;
}