/**
 * Copyright (c) 1998-2012 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

/**
 * The script is invoked after a record is created. When the Send Fax to Physician is checked, the script will send fax.
 *
 * @param (string)
 *             stType Type of operation submitted
 * @author Aurel Shenne Sinsin
 * @version 1.0
 */
function afterSubmit_sendFaxToPhysician(stType) {

     try {
          var stLoggerTitle = 'afterSubmit_sendFaxToPhysician';
          nlapiLogExecution('DEBUG', stLoggerTitle, 'Entered After Submit');

          if(stType != 'create') {
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Type of Operation Unsupported. Exit After Submit Successfully. Type = ' + stType);
               return true;
          }

          var stRecordId = nlapiGetRecordId();
          nlapiLogExecution('DEBUG', stLoggerTitle, 'Case ID = ' + stRecordId);

          var bSendFax = nlapiGetFieldValue('custevent_bio_send_fax_to_physician');
          nlapiLogExecution('DEBUG', stLoggerTitle, 'Send Fax to Physician = ' + bSendFax);

          if(bSendFax == 'F') {
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Script will not send fax to physician. Exit After Submit Successfully.');
               return true;
          }

          // Determine the NS Login user
          var stUser = nlapiGetUser();
          nlapiLogExecution('DEBUG', stLoggerTitle, 'User = ' + stUser);

          // Load the employee record for the User
          var recEmployee = nlapiLoadRecord('employee', stUser);

          // Determine the Patient ID, Physician (custom), Physician Fax Number (custom) from the newly created Case record.
          var stPatient = nlapiGetFieldValue('company');
          nlapiLogExecution('DEBUG', stLoggerTitle, 'Patient = ' + stPatient);

          var stPhysician = nlapiGetFieldValue('custevent_bio_physician');
          nlapiLogExecution('DEBUG', stLoggerTitle, 'Physician = ' + stPhysician);
          if(isEmpty(stPhysician)) {
               nlapiLogExecution('ERROR', stLoggerTitle, 'Physician for Patient ID: ' + stPatient + ' Not Found.');
               return true;
          }

          var stPhysicianFaxNumber = nlapiGetFieldValue('custevent_bio_physician_fax_number');
          nlapiLogExecution('DEBUG', stLoggerTitle, 'Physician Fax Number = ' + stPhysicianFaxNumber);
          if(isEmpty(stPhysicianFaxNumber)) {
               nlapiLogExecution('ERROR', stLoggerTitle, 'Physician Fax Number for Patient ID: ' + stPatient + ' Not Found.');
               return true;
          }

          var stSubject = 'Patient ID: ' + stPatient + ' has been recorded.';
          nlapiLogExecution('DEBUG', stLoggerTitle, 'Subject = ' + stSubject);

          /*
           * Added by Gerrom Infante
           * Date: 03/25/2014
           * Details:  Need to add code to decrypt the patient name since BioSolutia has encryption in place
           */
          var patientName = nlapiGetFieldText('company');
          var decryptedPatientName = decryptField(patientName, 'company');

          // Check if the 'Referral Form Type'
          var referralTypeNew = nlapiGetContext().getSetting('SCRIPT', 'custscript_bio_referral_form_type');
          nlapiLogExecution('DEBUG', stLoggerTitle, '[Script Parameter]Referral Type New = ' + referralTypeNew);

          var fax1aOn = nlapiGetContext().getSetting('SCRIPT', 'custscript_fax1a_on');
          nlapiLogExecution('DEBUG', stLoggerTitle, '[Script Parameter]Fax 1A On = ' + fax1aOn);

          var blnOld = false,
               referralType, stFax1Template, stFax1aTemplate, stFaxTemplate;
          if(isEmpty(referralTypeNew) === false) {
               blnOld = true;

               referralType = nlapiGetFieldValue('custevent_bio_referral_form_type');

               if(isEmpty(referralType) === true) {
                    referralType = referralTypeNew;
               }

               nlapiLogExecution('DEBUG', stLoggerTitle, 'Referral Type = ' + referralType);

               // Get the Fax 1 Template from the script parameter
               stFax1Template = nlapiGetContext().getSetting('SCRIPT', 'custscript_bio_fax_template');
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax 1 Template = ' + stFax1Template);

               // Get the Fax 1A Template from the script parameter
               stFax1aTemplate = nlapiGetContext().getSetting('SCRIPT', 'custscript_bio_fax_template_1a');
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax 1A Template = ' + stFax1aTemplate);

               if(fax1aOn === 'T') {
                    // if value of Referral Type is 'New' use Fax 1 Template, else use Fax 1A Tempalte
                    stFaxTemplate = (referralType != referralTypeNew) ? stFax1aTemplate : stFax1Template;
               } else {
                    stFaxTemplate = stFax1Template;
               }
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax Template Used = ' + stFaxTemplate);
          } else {
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Using nlobjTemplateRenderer');

               referralTypeNew = nlapiGetContext().getSetting('SCRIPT', 'custscript_bio_referral_form_consent');
               nlapiLogExecution('DEBUG', stLoggerTitle, '[Script Parameter]Referral Form - Consent Field - No = ' + referralTypeNew);

               referralType = nlapiGetFieldValue('custevent_bio_consent_sign_v2');
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Consent Check & Signature (Box 5) = ' + referralType);

               if(isEmpty(referralType) === true) {
                    referralType = referralTypeNew;
               }

               nlapiLogExecution('DEBUG', stLoggerTitle, 'Referral Type = ' + referralType);

               // Get the Fax 1 Template from the script parameter
               stFax1Template = nlapiGetContext().getSetting('SCRIPT', 'custscript_bio_fax_template');
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax 1 Template = ' + stFax1Template);

               // Get the Fax 1A Template from the script parameter
               stFax1aTemplate = nlapiGetContext().getSetting('SCRIPT', 'custscript_bio_fax_template_1a');
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax 1A Template = ' + stFax1aTemplate);

               if(fax1aOn === 'T') {
                    // if value of Referral Type is 'New' use Fax 1 Template, else use Fax 1A Tempalte
                    stFaxTemplate = (referralType != referralTypeNew) ? stFax1Template : stFax1aTemplate;
               } else {
                    stFaxTemplate = stFax1Template;
               }
               nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax Template Used = ' + stFaxTemplate);
          }

          var faxData;
          try {
               // load the Fax Template
               /**
                * @type nlobjFile
                */
               var faxFile = nlapiLoadFile(stFaxTemplate);

               // get the text of the fax template
               var templateBody = faxFile.getValue();

               /**
                * @type nlobjTemplateRenderer
                */
               var renderer = nlapiCreateTemplateRenderer();
               renderer.setTemplate(templateBody);

               // load the record
               /**
                * @type nlobjRecord
                * @record supportcase
                */
               var supportCase = nlapiLoadRecord('supportcase', nlapiGetRecordId());
               renderer.addRecord('case', supportCase);
               faxData = renderer.renderToString();
          } catch(error1) {
               if(error1 instanceof nlobjError) {
                    nlapiLogExecution('ERROR', stLoggerTitle, [error1.getCode(), ': ', error1.getDetails()].join(''));
               } else {
                    nlapiLogExecution('ERROR', stLoggerTitle, error1.toString());

               }
          }

          nlapiLogExecution('DEBUG', stLoggerTitle, ['Original Fax:= ', nlapiEscapeXML(faxData)].join(''));

          faxData = replacePatientName(decryptedPatientName, faxData);
          nlapiLogExecution('DEBUG', stLoggerTitle, ['Replaced Fax:= ', nlapiEscapeXML(faxData)].join(''));

          // get the information of the Prescriber
          var prescriberFields = [];
          prescriberFields.push('firstname');
          prescriberFields.push('lastname');

          var prescriberInfo = nlapiLookupField('partner', stPhysician, prescriberFields);
          nlapiLogExecution('DEBUG', stLoggerTitle, ['Prescriber Info:= ', JSON.stringify(prescriberInfo)].join(''));

          // check if <NLPRESCRIBER_NAME> is in the template
          if(faxData.indexOf('<NLPRESCRIBER_NAME>') != -1) {
               var prescriberName = [];

               prescriberName.push(prescriberInfo['firstname']);
               prescriberName.push(prescriberInfo['lastname']);

               faxData = faxData.replace('<NLPRESCRIBER_NAME>', prescriberName.join(' '));
          }

          var records = new Object();
          records['activity'] = stRecordId;
          var stFax1aAttach, attachment;
          if(blnOld === true) {
               if(referralType != referralTypeNew) {
                    if(fax1aOn === 'T') { // Get the Fax 1A Attachment
                         stFax1aAttach = nlapiGetContext().getSetting('script', 'custscript_bio_fax_1a_attach');
                         nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax 1A Attachment = ' + stFax1aAttach);

                         // load the value of the attachment
                         /**
                          * @type nlobjFile
                          */
                         attachment = nlapiLoadFile(stFax1aAttach);

                         nlapiSendFax(stUser, stPhysician, stSubject, faxData, records, attachment);
                    } else {
                         nlapiSendFax(stUser, stPhysician, stSubject, faxData, records);
                    }
               } else {
                    nlapiSendFax(stUser, stPhysician, stSubject, faxData, records);
               }
          } else {
               if(referralType == referralTypeNew) {
                    if(fax1aOn === 'T') {
                         // Get the Fax 1A Attachment
                         stFax1aAttach = nlapiGetContext().getSetting('script', 'custscript_bio_fax_1a_attach');
                         nlapiLogExecution('DEBUG', stLoggerTitle, 'Fax 1A Attachment = ' + stFax1aAttach);

                         // load the value of the attachment
                         /**
                          * @type nlobjFile
                          */
                         attachment = nlapiLoadFile(stFax1aAttach);

                         nlapiSendFax(stUser, stPhysician, stSubject, faxData, records, attachment);
                    } else {
                         nlapiSendFax(stUser, stPhysician, stSubject, faxData, records);
                    }
               } else {
                    nlapiSendFax(stUser, stPhysician, stSubject, faxData, records);
               }
          }

          nlapiLogExecution('DEBUG', stLoggerTitle, 'Successfully sent fax to physician.');

          nlapiLogExecution('DEBUG', stLoggerTitle, 'Exit After Submit');
     } catch(error) {
          if(error.getDetails != undefined) {
               nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
               throw error;
          } else {
               nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
               throw nlapiCreateError('99999', error.toString());
          }
     }
}

function decryptField(value, fieldName) {

    var funcName = 'decryptField';

    // get the webservice url from the script
    var webServiceURL = nlapiGetContext().getSetting('script', 'custscript_webservice_url');
    nlapiLogExecution("AUDIT", funcName, ['[Script Parameter]Web Service URL:= ', webServiceURL].join(''));

    var decryptionRequest = createFieldDecryptionRequest(value, fieldName);

    var decryptedValue = sendFieldDecryptionRequest(webServiceURL, decryptionRequest);

    if (isEmpty(decryptedValue) === false) {
        if (decryptedValue.indexOf("'") == 0) {
            decryptedValue = decryptedValue.substr(1);
        }
    }

    return decryptedValue;
}

function replacePatientName(patientName, faxData) {

     var stLoggerTitle = 'replacePatientName';

     if(faxData.indexOf('<NLDECRYPTEDCOMPANY>') != -1) {
          faxData = faxData.replace('<NLDECRYPTEDCOMPANY>', patientName);
     }

     return faxData;

     //     var referralConfirmation = faxData.match(/(td.*?\>)(.*?)(Referral Confirmation for.*?)(\<\/td)/mi);
     //
     //     var confirmationText = referralConfirmation[3];
     //     nlapiLogExecution('DEBUG', stLoggerTitle, [ 'Confirmation Text:= ', confirmationText ].join(''));
     //
     //     var replacedLine = confirmationText.replace(/(Referral Confirmation for )(.*$)/mi, '$1 ' + patientName);
     //     nlapiLogExecution('DEBUG', stLoggerTitle, [ 'Replaced Line:= ', replacedLine ].join(''));
     //
     //     return faxData.replace(confirmationText, replacedLine);
}
/**
 * Check if value is empty
 *
 * @param stValue
 * @returns {Boolean}
 */
function isEmpty(stValue) {

     if((((((stValue == ''))))) || (((((stValue == null))))) || (((((stValue == undefined)))))) {
          return true;
     }

     return false;
}
