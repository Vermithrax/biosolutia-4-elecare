/**
 * Copyright (c) 1998-2012 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 * 
 * @author Milagros Garicoits
 * @version 1.0
 */

function pageInitHideMenus(type)
{
	nlapiLogExecution('DEBUG', "pageInitHideMenus");
	var quickMenu = parent.document.getElementById("ns-header-quick-menu"); if (quickMenu) { quickMenu.style.display="none"; };
	var help = parent.document.getElementsByClassName("ns-help")[0]; if (help) { help.style.display="none"; };
	var home = parent.document.getElementsByClassName("ns-menu uir-menu-home")[0]; if (home) { home.style.display="none"; };
	var shortcuts = parent.document.getElementsByClassName("ns-menu uir-menu-shortcuts")[0]; if (shortcuts) { shortcuts.style.display="none"; };
	var recent = parent.document.getElementsByClassName("ns-menu uir-menu-recent")[0]; if (recent) { recent.style.display="none"; };
	var menuHeaders = parent.document.getElementsByClassName("ns-menuitem ns-header");
	var dataTittle = "";
	for (var i = 0; i < menuHeaders.length; i++) {
		dataTittle = menuHeaders[i].getAttribute("data-title");
		if( dataTittle == "Setup" || dataTittle == "Support") {
			menuHeaders[i].style.display="none";
		}
	}
	var head = parent.document.head || parent.document.getElementsByTagName("head")[0];
	var styleNode = parent.document.createElement("style");
	styleNode.type = "text/css";
	if(!!(window.attachEvent && !window.opera)) {
		styleNode.styleSheet.cssText = "span { color: rgb(255, 0, 0); }";
	} else {
		var styleText = parent.document.createTextNode(".uir-menuitem-viewall{ display: none!important }");
		styleNode.appendChild(styleText);
	}
	parent.document.getElementsByTagName("head")[0].appendChild(styleNode);
}