/* ============================= 
 * | Global Variables          | 
 * =============================
 */
{
	// global object for general functions
	/**
	 * @type CommonFunctions
	 */
	var generalFunctions_global = new NetSuiteCustomLibrary().StandardFunctions();

	// global object for script logging
	/**
	 * @type Logger
	 */
	var scriptLogger_global = new NetSuiteCustomLibrary().ScriptLogger();

	// global stop watch object, for calcuating script runtime
	/**
	 * @type StopWatchObject
	 */
	var stopWatch_global = new NetSuiteCustomLibrary().StopWatch();

	// global object for Execution Context
	/**
	 * @type nlobjContext
	 */
	var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               | 
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |   
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             | 
 * |=======================================================================================================================|
 * | NAME: NS | Set Assigned To	                                                                                           |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * | This script will execute when a Referral record is editted via the UI.  It will call supportcasegrabber.nl so that    |
 * | the edited record will be removed from the Grab queue                                                                 |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           |
 * | Gerrom Infante			Apr	24, 2014	2.0			Changed the way on how to call the core support grabber function   | 
 * | Gerrom Infante     	Mar 25, 2014	1.0			                                                                   | 
 * |_______________________________________________________________________________________________________________________|
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */
function pageInit_setAssignedTo (type) {

	var funcName = 'pageInit_setAssignedTo';

	try {
		scriptLogger_global.enableDebug();

		scriptLogger_global.audit(funcName, '==================== PAGE INIT EVENT START ====================');
		var stopWatch = new NetSuiteCustomLibrary().StopWatch();
		stopWatch.start();

		var context = executionContext_global.getExecutionContext();
		scriptLogger_global.audit(funcName, ['Execution context:= ', context, ' Operation Type:= ', type].join(''));

		var referralIntId = nlapiGetRecordId();
		scriptLogger_global.debug(funcName, ['Referral Record[Internal Id]:= ', referralIntId].join(''));

		// get the value of the assigned field
		var assignedTo = nlapiGetFieldValue('assigned');
		scriptLogger_global.debug(funcName, ['Assigned[Internal Id]:= ', assignedTo].join(''));

		if (generalFunctions_global.isEmpty(assignedTo) === false) {
			// check if assigned to is an employee
			if (isEmployee(assignedTo) === false) { // If the Assigned To field is not an employee, call grabber function
				var finalUrl = ['/app/crm/support/supportcasegrabber.nl?id=', referralIntId].join('');

				scriptLogger_global.audit(funcName, ['Assigned To is not an Employee.  Calling Support Case Grabber:= ', finalUrl].join(''));

				// call supportcasegrabber.nl
				document.location = finalUrl;
			}
		} else { // If Assigned field is blank, call grabber function
			var finalUrl = ['/app/crm/support/supportcasegrabber.nl?id=', referralIntId].join('');

			scriptLogger_global.audit(funcName, ['Assigned To is blank.  Calling Support Case Grabber:= ', finalUrl].join(''));

			// call supportcasegrabber.nl
			document.location = finalUrl;
		}

		stopWatch.stop();
		scriptLogger_global.audit(funcName, ['=================== PAGE INIT EVENT END ', '[Running Time:= ', stopWatch.duration(), '] ==================='].join(''));

	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
function isEmployee (id) {

	// get the value of the script parameter that contains the internal id of the Employee Groups
	var employeeGroups = executionContext_global.getSetting('script', 'custscript_employee_groups');

	var response = true;

	if (generalFunctions_global.isEmpty(employeeGroups) === false) {
		var arrayEmployeeGroup = employeeGroups.split(',');

		// if id is in the array, return false
		if (generalFunctions_global.inArray(id, arrayEmployeeGroup) === true) {
			response = false;
		}
	}

	return response;
}
