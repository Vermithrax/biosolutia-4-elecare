/* ============================= 
 * | Global Variables          | 
 * =============================
 */
{
     // global object for general functions
     /**
      * @type CommonFunctions
      */
     var generalFunctions_global = new NetSuiteCustomLibrary().StandardFunctions();
     
     // global object for script logging
     /**
      * @type Logger
      */
     var scriptLogger_global = new NetSuiteCustomLibrary().ScriptLogger();
     
     // global stop watch object, for calcuating script runtime
     /**
      * @type StopWatchObject
      */
     var stopWatch_global = new NetSuiteCustomLibrary().StopWatch();
     
     // global object for Execution Context
     /**
      * @type nlobjContext
      */
     var executionContext_global = nlapiGetContext();
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               | 
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |   
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             | 
 * |=======================================================================================================================|
 * | NAME: NS | Trigger Update                                                                                             |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * | This automation executes when a Patient (customer) or Prescriber/Physician (partner) record is edited.  The script    | 
 * | will check if predefined fields were changed, if at lease one of the fields was change.  It will need to update the   |
 * | associated Open Referral (supportcase) and Case Insurance Data custom record.                                         |
 * | The updating of the Referral and Case Insurance Data will be done by a scheduled script which will be called by this  |
 * | User Event script.                                                                                                    |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           | 
 * | Gerrom Infante     	Mar 28, 2014	1.0			                                                                   | 
 * |_______________________________________________________________________________________________________________________|
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord customer, partner
 * @param {String} type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
function beforeSubmit_triggerUpdate(type) {
     
     var funcName = 'beforeSubmit_triggerUpdate';
     
     if ((type != 'edit') && (type != 'xedit')) {
          scriptLogger_global.audit(funcName, [ 'Operation Type:= ', type ].join(''));
          return true;
     }
     
     // disable DEBUG logs if Script Log Level is not Debug
     if (executionContext_global.getLogLevel() == 'DEBUG') {
          scriptLogger_global.enableDebug();
     }
     
     try {
          scriptLogger_global.audit(funcName, '==================== BEFORE SUBMIT EVENT START ====================');
          stopWatch_global.start();
          
          // get the old record
          /**
           * @type nlobjRecord
           * @record customer
           * @record partner
           */
          var oldRecord = nlapiGetOldRecord();
          /**
           * @type nlobjRecord
           * @record customer
           * @record partner
           */
          var newRecord = nlapiGetNewRecord();
          
          // get the fields to check from the script paramater
          var fieldsToCheck = nlapiGetContext().getSetting('script', 'custscript_fields_to_check');
          if (generalFunctions_global.isEmpty(fieldsToCheck) === false) {
               var fieldsToCheckArray = fieldsToCheck.split(',');
               
               var fieldsToCheck = {};
               for ( var x in fieldsToCheckArray) {
                    var field = fieldsToCheckArray[x];
                    
                    fieldsToCheck[field] = '';
               }
               
               scriptLogger_global.debug(funcName, [ 'Fields To Check:= ', JSON.stringify(fieldsToCheck) ].join(''));
               
               var oldRecordData = getRecordData(oldRecord, fieldsToCheck);
               scriptLogger_global.audit(funcName, [ 'Old Record Data:= ', JSON.stringify(oldRecordData) ].join(''));
               
               var newRecordData = getRecordData(newRecord, fieldsToCheck);
               scriptLogger_global.audit(funcName, [ 'New Record Data:= ', JSON.stringify(newRecordData) ].join(''));
               
               // check if data changed
               if (dataChanged(oldRecordData, newRecordData) === true) {
                    scriptLogger_global.audit(funcName, '-------------------- Data has changed scheduling Scheduled Script --------------------');
                    
                    // call scheduled script
                    var params = new Array();
                    params['custscript_recordid'] = nlapiGetRecordId();
                    params['custscript_recordtype'] = nlapiGetRecordType();
                    params['custscript_user_email'] = executionContext_global.getEmail();
                    params['custscript_user_id'] = nlapiGetUser();
                    
                    var scriptId = 'customscript_update_patient_information';
                    
                    // search if any deployment are available
                    var deployment = searchAvailableDeployment(scriptId);
                    
                    // do not call script if no deployments are found
                    if (generalFunctions_global.isNullOrUndefined(deployment) === false) {
                         // check if an available deployment was found
                         if (generalFunctions_global.isNullOrUndefined(deployment.usethis) === false) {
                              var deployIds = deployment.usethis;
                              
                              if (generalFunctions_global.numRows(deployIds) > 0) {
                                   var deployed = false;
                                   // loop thru the available deployments and schedule the script
                                   for (var x = 0; x < deployIds.length; x++) {
                                        var schedScriptStatus = nlapiScheduleScript(scriptId, deployIds[x], params);
                                        
                                        if (schedScriptStatus == 'QUEUED') {
                                             scriptLogger_global.audit(funcName, [ '-------------------- Available Sched Script Deployment [', deployIds[x], '] found, using it. --------------------' ].join(''));
                                             deployed = true;
                                             break;
                                        }
                                        
                                   }
                              }
                              
                              // check if it went through all the available deployments and the script was not scheduled
                              if (deployed === false) {
                                   // create a new deployment
                                   scriptLogger_global.audit(funcName, '-------------------- No available Sched Script Deployment found, creating new deployment. --------------------');
                                   
                                   // no deployment was available, script to create one
                                   var deployId = createNewDeployment(deployment.existing);
                                   
                                   // schedule the new deployment
                                   var schedScriptStatus = nlapiScheduleScript(scriptId, deployId, params);
                              }
                              
                         } else { // create a new deployment
                              scriptLogger_global.audit(funcName, '-------------------- No available Sched Script Deployment found, creating new deployment. --------------------');
                              
                              // no deployment was available, script to create one
                              var deployId = createNewDeployment(deployment.existing);
                              
                              // schedule the new deployment
                              var schedScriptStatus = nlapiScheduleScript(scriptId, deployId, params);
                              
                         }
                         
                         scriptLogger_global.audit(funcName, [ 'Scheduled Script Status: ' + schedScriptStatus ].join(''));
                    } else {
                         scriptLogger_global.audit(funcName, [ 'No deployments found for scheduled script.  Script Id:= ', scriptId ].join(''));
                    }
               }
          }
          
          stopWatch_global.stop();
          scriptLogger_global.audit(funcName, [ '=================== BEFORE SUBMIT EVENT END ', '[Running Time:= ', stopWatch_global.duration(), '] ===================' ].join(''));
     } catch (error) {
          if (error instanceof nlobjError) {
               nlapiLogExecution('ERROR', funcName, [ error.getCode(), ': ', error.getDetails() ].join(''));
               throw error;
          } else {
               nlapiLogExecution('ERROR', funcName, error.toString());
               throw nlapiCreateError('99999', error.toString());
          }
     }
}
function searchAvailableDeployment(scriptId) {
     
     var filter1 = [ "scriptid", 'script', 'is', scriptId ];
     var filter2 = [ "isdeployed", null, 'is', 'T' ];
     var filters = [ filter1, filter2 ];
     
     var col0 = [ "status" ];
     var col1 = [ "scriptid" ];
     var columns = [ col0, col1 ];
     
     var result = generalFunctions_global.buildSearch('scriptdeployment', filters, columns);
     
     if (generalFunctions_global.numRows(result) > 0) {
          var deployment = {};
          deployment.existing = result[0].getId();
          deployment.usethis = [];
          
          // loop thru the results and get the first unscheduled deployment
          for (var x = 0; x < result.length; x++) {
               var status = result[x].getValue('status');
               var scriptId = result[x].getValue('scriptid');
               
               if (status == "NOTSCHEDULED") { // use this deployment
                    if (generalFunctions_global.inArray(scriptId, deployment.usethis) === false) {
                         deployment.usethis.push(scriptId);
                    }
               }
          }
     }
     
     return deployment;
}
function createNewDeployment(deployId) {
     
     // copy the existing deployment
     /**
      * @type nlobjRecord
      * @record scriptdeployment
      */
     var record = nlapiCopyRecord('scriptdeployment', deployId);
     
     // set the status to Not Scheduled
     record.setFieldValue('status', 'NOTSCHEDULED');
     // set start date to today
     record.setFieldValue('startdate', nlapiDateToString(new Date(), 'date'));
     var id = nlapiSubmitRecord(record, true, true);
     
     return nlapiLookupField('scriptdeployment', id, 'scriptid');
}
/**
 * This function gets the data from the old record
 * 
 * @param {nlobjRecord} record NetSuite record object
 * @param {Object} fields Custom object containing the field ids
 * @returns {Object}
 */
function getRecordData(record, fields) {
     
     var funcName = 'getRecordData';
     
     var recordData = {};
     for ( var field in fields) {
          if (field !== 'address') {
               var fieldData = record.getFieldValue(field);
          } else {
               var fieldData = getAddressData(record);
          }
          scriptLogger_global.debug(funcName, [ 'Field:= ', field, ' - Field Data:= ', fieldData ].join(''));
          
          recordData[field] = fieldData;
     }
     
     return recordData;
}
/**
 * This function checks if any of the data in the specified fields changed
 * 
 * @param {Object} oldDataSet Custom object containing the field data
 * @param {Object} newDataSet Custom object containing the field data
 * @returns {Boolean}
 */
function dataChanged(oldDataSet, newDataSet) {
     
     var funcName = 'dataChanged';
     
     var changed = false;
     
     // oldDataSet and newDataSet have the same fields
     for ( var field in oldDataSet) {
          var oldData = oldDataSet[field];
          var newData = newDataSet[field];
          scriptLogger_global.debug(funcName, [ 'Field:= ', field, ' - Old Field Data:= ', oldData, ' - New Field Data:= ', newData ].join(''));
          
          if (oldData !== newData) {
               changed = true;
               break;
          }
     }
     
     return changed;
}
/**
 * This function gets the first addressline data of the passed record
 * 
 * @param {nlobjRecord} record NetSuite record object
 * @returns {String}
 */
function getAddressData(record) {
     
     var address = [];
     
     var addressFields = {};
     addressFields.addr1 = '';
     addressFields.addr2 = '';
     addressFields.addr3 = '';
     addressFields.city = '';
     addressFields.state = '';
     addressFields.zip = '';
     addressFields.country = '';
     
     var addressCount = record.getLineItemCount('addressbook');
     if (addressCount == '1') {
          for ( var field in addressFields) {
               address.push(record.getLineItemValue('addressbook', field, '1'));
          }
     } else {
          /*
           * if there are more than one addres,
           * for Patient (customer) get the one that is marked as 'Default Billing' and 'Residential'
           * for Prescriber (partner) get the one that is marked as 'Default Billing' and 'Default Shipping'
           */
          var recordType = nlapiGetRecordType();
          
          for (var x = 1; x <= addressCount; x++) {
               // get the value of the default billing
               var defaultBill = record.getLineItemValue('addressbook', 'defaultbilling', x);
               var defaultShip = (recordType == 'customer') ? record.getLineItemValue('addressbook', 'isresidential', x) : record.getLineItemValue('addressbook', 'defaultshipping', x);
               
               if ((defaultBill == 'T') && (defaultShip == 'T')) {
                    for ( var field in addressFields) {
                         address.push(record.getLineItemValue('addressbook', field, x));
                    }
                    break;
               }
          }
     }
     
     return address.join(' ');
}
