/* |=======================================================================================================================|
 * | NAME: BS|Clear Pharmacy Fax                                                                                           |                      |
 * |                                                                                                                       |   
 * | OVERVIEW:                                                                                                             |
 * | On save of the Case Insurance custom record, the script will get the fax select on the record.  It will then update   |
 * | the referral record to clear the fax number of the unselected Pharamacies.                                            |                                                                          |
 * | ______________________________________________________________________________________________________________________|
 * | Author              Date           Version     Comments                                                               | 
 * | Gerrom V. Infante   Apr 29 2016    1.0                                                                                | 
 * |=======================================================================================================================|
 */
var clear_fax = new function() {
     
     var errorHandling = {
          'script_name' : 'BS|Clear Pharmacy Fax',
          'from_email' : '-5', /* Default Administrator */
          'to_email' : 'gerrom.infante@gmail.com',
          'script_file' : 'BS_ClearPharmacyFax_UserEvent.js'
     };
     
     /**
      * @type UtilityBelt()
      */
     var utility = new UtilityBelt();
     /**
      * @type ScriptLogger
      */
     var logger = new ScriptLogger();
     /**
      * @type nlobjContext
      */
     var context = nlapiGetContext();
     
     return {
          /**
           * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
           * 
           * @appliedtorecord recordType
           * @param {String}
           *             type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF)
           *             markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
           * @returns {Void}
           */
          after_submit : function(type) {
               
               var logTitle = 'clear_fax.after_submit';
               
               // enable debug statements if log level is not Debug
               if (context.getLogLevel() == 'DEBUG') {
                    logger.enableDebug();
               }
               
               try {
                    logger.audit(logTitle, '==================== AFTER SUBMIT EVENT START ====================');
                    
                    if (type != 'delete') {
                         /* get the value of the script parameter */
                         var pharmacy_map = context.getSetting('script', 'custscript_pharmacy_map');
                         var pharmacy_number = context.getSetting('script', 'custscript_pharmacy_number');
                         
                         if (utility.isEmpty(pharmacy_map) === false) {
                              logger.audit(logTitle, 'Pharmacy Map:= ' + pharmacy_map + ' | Pharmacy Phone:= ' + pharmacy_number);
                              pharmacy_map = JSON.parse(pharmacy_map);
                              pharmacy_number = JSON.parse(pharmacy_number);
                              
                              /* get the value of the Specialty Pharmacy (List) */
                              var selected_pharmacy = nlapiGetFieldValue('custrecord_bio_case_ins_data_phcy_list');
                              
                              /* get the referral record */
                              var referral_id = nlapiGetFieldValue('custrecord_bio_fax_case');
                              
                              logger.audit(logTitle, 'Selected Pharmacy:= ' + selected_pharmacy);
                              
                              /**
                               * @type nlobjRecord
                               * @record supportcase
                               */
                              var referral = nlapiLoadRecord('supportcase', referral_id);
                              
                              var pharmac, pharmacy_fax, referral_updated = false;
                              for ( var x in pharmacy_map) {
                                   pharmacy_fax = referral.getFieldValue(pharmacy_map[x]);
                                   
                                   if (x != selected_pharmacy) {
                                        /* get the value of the pharmacy field */

                                        if (utility.isEmpty(pharmacy_fax) === false) {
                                             referral.setFieldValue(pharmacy_map[x], null);
                                             referral_updated = true;
                                        }
                                   } else {
                                        if (utility.isEmpty(pharmacy_fax) === true) {
                                             referral.setFieldValue(pharmacy_map[x], pharmacy_number[x]);
                                             referral_updated = true;
                                        }
                                   }
                              }
                              
                              if (referral_updated === true) {
                                   nlapiSubmitRecord(referral, true, true);
                              }
                         }
                         
                    }
                    logger.audit(logTitle, '==================== AFTER SUBMIT EVENT END ====================');
               } catch (error) {
                    utility.errorEmail(error, errorHandling, true);
               }
          }
     };
};
