/* =============================
 * | Global Variables          |
 * =============================
 */
{
	// global object for general functions
	/**
	 * @type CommonFunctions
	 */

	var generalFunctions_global = new NetSuiteCustomLibrary().StandardFunctions();

	// global object for script logging
	/**
	 * @type Logger
	 */

	var scriptLogger_global = new NetSuiteCustomLibrary().ScriptLogger();

	// global stop watch object, for calcuating script runtime
	/**
	 * @type StopWatchObject
	 */

	var stopWatch_global = new NetSuiteCustomLibrary().StopWatch();

	// global object for Execution Context
	/**
	 * @type nlobjContext
	 */

	var executionContext_global = nlapiGetContext();

	// create object for parameters
	var scriptParameters_global = {};
}
/* |=======================================================================================================================|
 * | Copyright (c) 1998-2014                                                                                               |
 * | NetSuite, Inc. 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511                                            |
 * | All Rights Reserved.                                                                                                  |
 * |                                                                                                                       |
 * | This software is the confidential and proprietary information of NetSuite, Inc. ('Confidential Information').         |
 * | You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license|
 * | agreement you entered into with NetSuite.                                                                             |
 * |=======================================================================================================================|
 * | NAME: ARESTIN Referral Form                                                                                           |
 * |                                                                                                                       |
 * | OVERVIEW:                                                                                                             |
 * | This is a suitelet that will display a referral form.  Prescriber Information will be sourced from the logged in      |
 * | Partner.The Partner will fill up the fields then click 'Save & Print' button. Upon clicking the button, the script    |
 * | will create a Patient (Customer) record in NetSuite.  It will also create a Case Record and a custom                  |
 * | 'Medical Insurance' custom record.  Script will also generate a PDF and allow the user to print or save the PDF.      |
 * | ______________________________________________________________________________________________________________________|
 * | Author        			Date			Version     Comments                                                           |
 * | Gerrom Infante     	Jan 21, 2014	1.0			                                                                   |
 * |_______________________________________________________________________________________________________________________|
 */
/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void}
 */
function suitelet_referralForm (request , response) {

	var funcName = 'suitelet_referralForm';

	// disable DEBUG logs if Script Log Level is not Debug
	if (executionContext_global.getLogLevel() == 'DEBUG') {
		scriptLogger_global.enableDebug();
	}

	try {
		scriptLogger_global.audit(funcName, '==================== SUITELET START ====================');
		stopWatch_global.start();

		// get the script parameter values
		var primaryDiagnosis = nlapiGetContext().getSetting('script', 'custscript_patient_primary_diagnosis');
		var diagnosisGroup = nlapiGetContext().getSetting('script', 'custscript_patient_diagnosis_group');
		var statusCategory = nlapiGetContext().getSetting('script', 'custscript_default_referral_status_cat');
		var referralSubject = nlapiGetContext().getSetting('script', 'custscript_default_referral_subject');
		var referralOrigin = nlapiGetContext().getSetting('script', 'custscript_default_referral_origin');
		var referralInfo = nlapiGetContext().getSetting('script', 'custscript_default_referral_info');
		var referralStatus = nlapiGetContext().getSetting('script', 'custscript_default_referral_status');
		var referralPriority = nlapiGetContext().getSetting('script', 'custscript_default_referral_priority');
		var timeZone = nlapiGetContext().getSetting('script', 'custscript_default_patient_timezone');
		var referralFeeOption = nlapiGetContext().getSetting('script', 'custscript_referral_fee_for_service_opt');
		var creditCardAuthForm = nlapiGetContext().getSetting('script', 'custscript_credit_card_auth_form');

		scriptParameters_global.primaryDiagnosis = primaryDiagnosis;
		scriptParameters_global.diagnosisGroup = diagnosisGroup;
		scriptParameters_global.statusCategory = statusCategory;
		scriptParameters_global.referralSubject = referralSubject;
		scriptParameters_global.referralOrigin = referralOrigin;
		scriptParameters_global.referralInfo = referralInfo;
		scriptParameters_global.referralStatus = referralStatus;
		scriptParameters_global.referralPriority = referralPriority;
		scriptParameters_global.timeZone = timeZone;
		scriptParameters_global.referralFeeOption = referralFeeOption;
		scriptParameters_global.creditCardAuthForm = creditCardAuthForm;

		scriptLogger_global.debug(funcName, ['Script Parameters:= ', JSON.stringify(scriptParameters_global)].join(''));

		// get the value of the hidden field custpage_stage to determine if form should be displayed or PDF is to be generated
		var stage = request.getParameter('custpage_stage');
		scriptLogger_global.audit(funcName, ['Script Stage:= ', stage].join(''));

		var alert = request.getParameter('alert');
		scriptLogger_global.audit(funcName, ['Script Alert:= ', alert].join(''));

		if (stage == 'generate') {
			// create the Patient record
			var patientData = createPatientRecord(request);

			if (generalFunctions_global.isEmpty(patientData.message) === true) {
				scriptLogger_global.audit(funcName, '-------------------- Patient Record successfully created. --------------------');
				scriptLogger_global.debug(funcName, ['Patient Record Internal Id:= ', patientData.patientId].join(''));

				// create the Referral (Case) record
				var referralData = createReferralCase(patientData.patientId, request);

			} else {
				/**
				 * @type nlobjForm
				 */
				var suiteletForm = createSuiteletForm(patientData.message);
				response.writePage(suiteletForm);

				stopWatch_global.stop();
				scriptLogger_global.audit(funcName, ['=================== SUITELET END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
				return true;
			}

			if (generalFunctions_global.isEmpty(referralData.message) === true) {
				scriptLogger_global.audit(funcName, '-------------------- Referral Record successfully created. --------------------');
				scriptLogger_global.debug(funcName, ['Referral Record Internal Id:= ', referralData.internalId].join(''));

				//  send out the notification email
				sendNotificationEmail(patientData.patientId, referralData.internalId);

				// call another suitelet for the pdf creation output PDF in a new window/tab
				var form = callGeneratePDFSuitelet(patientData.patientId, referralData.internalId, request);
				response.write(form);

			} else {
				//delete the created Patient record
				nlapiDeleteRecord('customer', patientData.patientId);

				var suiteletForm = createSuiteletForm(referralData.message);
				response.writePage(suiteletForm);

				stopWatch_global.stop();
				scriptLogger_global.audit(funcName, ['=================== SUITELET END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
				return true;
			}
		} else { // create the Suitelet form
			/**
			 * @type nlobjForm
			 */
			var suiteletForm = createSuiteletForm();

			response.writePage(suiteletForm);
		}

		stopWatch_global.stop();
		scriptLogger_global.audit(funcName, ['=================== SUITELET END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
function sendNotificationEmail (patientId , referralId) {

	var funcName = 'sendNotificationEmail';

	// get the email address
	var referalEmail = executionContext_global.getSetting('SCRIPT', 'custscript_arestin_referral_email');
	scriptLogger_global.audit(funcName, ['[Script Parameter]ARESTIN FORM Email:= ', referalEmail].join(''));

	var referalAuthor = executionContext_global.getSetting('SCRIPT', 'custscript_email_author');
	scriptLogger_global.audit(funcName, ['[Script Parameter]ARESTIN FORM Email Author:= ', referalAuthor].join(''));

	// get the patient id and the case number
	var patientNumber = nlapiLookupField('customer', patientId, 'entityid');
	scriptLogger_global.audit(funcName, ['Patient Number:= ', patientNumber].join(''));

	var caseNumber = nlapiLookupField('supportcase', referralId, 'casenumber');
	scriptLogger_global.audit(funcName, ['Referral Number:= ', caseNumber].join(''));

	// compose the email
	var emailBody = [];
	emailBody.push('An ARESTIN Referral Form has been submitted');
	emailBody.push(['Patient Number:= ', patientNumber].join(''));
	emailBody.push(['Referral Number:= ', caseNumber].join(''));
	emailBody.join('\n');

	// send the email out
	nlapiSendEmail(referalAuthor, referalEmail, 'An ARESTIN Referral Form has been submitted', emailBody);
	scriptLogger_global.audit(funcName, '------------------- Email has been sent. ------------------- ');
}
function callGeneratePDFSuitelet (patientId , referralId , request) {

	var funcName = 'callGeneratePDFSuitelet';

	scriptLogger_global.audit(funcName, '-------------------- Calling Generate PDF Suitelet. --------------------');

	// this if for testing purposes only
	var strSuiteletUrl = nlapiResolveURL('SUITELET', 'customscript_bs_gen_pdf', 'customdeploy_bs_gen_pdf');
	var strParam = ['&patientid=', patientId, '&referralid=', referralId].join('');

	var strFormUrl = [strSuiteletUrl, strParam].join('');
	scriptLogger_global.debug(funcName, ['Generate PDF Suitelet URL:= ', strFormUrl].join(''));

	var referralUrl = nlapiResolveURL('suitelet', executionContext_global.getScriptId(), executionContext_global.getDeploymentId());
	scriptLogger_global.debug(funcName, ['ARESTIN Referral Form Suitelet URL:= ', referralUrl].join(''));

	var stHtml = '<html>';
	stHtml += '<head>';
	stHtml += '<script language="JavaScript">';
	//	stHtml += 'var host = document.location.hostname;';
	//	stHtml += 'var genPdfUrl = [host, "' + strFormUrl + '"].join("");';
	stHtml += 'alert("Please Print or Save the PDF that will be shown shortly. This may take a few seconds, please do not click on Submit and Print again.");';
	stHtml += 'var winname = window.open("' + strFormUrl + '", "generate_pdf", "toolbar=no,directories=no,status=no,menubar=no,modal=yes");';
	stHtml += 'if (winname == null) {';
	stHtml += 'alert("Unable to open popup window. Please allow popups for this site. ");';
	stHtml += 'window.location.replace("' + referralUrl + '");';
	stHtml += '}';
	stHtml += 'else {';
	stHtml += 'window.location.replace("' + referralUrl + '");';
	stHtml += '};';
	stHtml += '</script>';
	stHtml += '</head>';
	stHtml += '<body>';
	stHtml += '</body>';
	stHtml += '</html>';

	scriptLogger_global.debug(funcName, ['Generate PDF Suitelet Call:= ', nlapiEscapeXML(stHtml)].join(''));

	return stHtml;
}
function getPartnerData (partnerInternalId) {

	var funcName = 'getPartnerData';

	try {
		/**
		 * @type nlobjRecord
		 * @record partner
		 */
		var partner = nlapiLoadRecord('partner', partnerInternalId);

		var practiceName = partner.getFieldValue('companyname');
		var prescriberName = partner.getFieldValue('entityid');

		/**
		 * @type Array
		 */
		var prescriberNameArray = prescriberName.split(' ');
		prescriberNameArray.shift();
		prescriberName = prescriberNameArray.join(' ');

		var presciberPhone = partner.getFieldValue('phone');
		var officeContactName = partner.getFieldValue('custentity_bio_phys_off_cntct');
		var officeContactPhone = partner.getFieldValue('homephone');
		var officeContactExtension = partner.getFieldValue('custentityoff_con_phon_ext');
		var officeContactEmail = partner.getFieldValue('email');
		var preferredMethod = partner.getFieldValue('custentity_bio_phys_md_pref');
		var faxNumber = partner.getFieldValue('fax');
		var prescriberNPI = partner.getFieldValue('custentity_bio_phys_npi');
		var prescriberAddress = partner.getFieldValue('billaddr1');
		var prescriberAddressCity = partner.getFieldValue('billcity');
		var prescriberAddressState = partner.getFieldValue('billstate');
		var prescriberAddressZip = partner.getFieldValue('billzip');

		return {
			'custpage_practice_name': practiceName,
			'custpage_prescriber_name': prescriberName,
			'custpage_com_phone': presciberPhone,
			'custpage_office_contact_name': officeContactName,
			'officeContactPhone': officeContactPhone,
			'officeContactExtension': officeContactExtension,
			'custpage_office_contact_email': officeContactEmail,
			'preferredMethod': preferredMethod,
			'custpage_com_fax': faxNumber,
			'custpage_prescriber_npi': prescriberNPI,
			'custpage_deliver_to_address': prescriberAddress,
			'custpage_deliver_to_city': prescriberAddressCity,
			'custpage_deliver_to_state': prescriberAddressState,
			'custpage_deliver_to_zip': prescriberAddressZip
		};
	} catch (error) {
		return {
			'custpage_practice_name': '',
			'custpage_prescriber_name': '',
			'custpage_com_phone': '',
			'custpage_office_contact_name': '',
			'officeContactPhone': '',
			'officeContactExtension': '',
			'custpage_office_contact_email': '',
			'preferredMethod': '',
			'custpage_com_fax': '',
			'custpage_prescriber_npi': '',
			'custpage_deliver_to_address': '',
			'custpage_deliver_to_city': '',
			'custpage_deliver_to_state': '',
			'custpage_deliver_to_zip': ''
		};
	}

}
function createSuiteletForm (error) {

	var funcName = 'createSuiteletForm';

	try {

		/**
		 * @type nlobjForm
		 */
		var suiteletForm = nlapiCreateForm('ARESTIN&reg; Referral Form', false);

		var popUpText = [];
		popUpText.push('<b><font color="red"><font size="3">IMPORTANT - PLEASE READ</font><br/>');
		popUpText.push('Please WAIT after choosing "Submit & Print" while your pdf form is generated.');
		popUpText.push('This can take 5 to 7 seconds. The generated PDF is displayed in a separate window/tab.</br>');
		popUpText.push('Please disable your popup blocker or allow popups for this site before clicking	the "Submit and Print" button.</font></b>');
		popUpText.join('');

		if (generalFunctions_global.isEmpty(error) === false) {
			var errorhtml = ['<h2><font color="red">', error, '</font></h2>'].join('');

			// display error message
			/**
			 * @type nlobjField
			 */
			var fldError = suiteletForm.addField('custpage_error_message', 'inlinehtml', '');
			fldError.setDefaultValue(errorhtml);
			fldError.setLayoutType('outsideabove', 'startrow');
			fldError.setPadding(1);
		}

		suiteletForm.setScript('customscript_arestin_validation');

		var parameters = JSON.stringify(scriptParameters_global);
		suiteletForm.addField('custpage_script_parameters', 'textarea', 'Script Parameters').setDisplayType('hidden').setDefaultValue(parameters);

		// Stage field - Hidden
		suiteletForm.addField('custpage_stage', 'text', 'Stage').setDisplayType('hidden').setDefaultValue('generate');

		/**
		 * @type nlobjField
		 */
		var popUp2 = suiteletForm.addField('custpage_popup2', 'inlinehtml', '', null, null);
		popUp2.setDefaultValue(popUpText);
		popUp2.setLayoutType('outsideabove', 'startrow');

		// create field group 'Patient Information'
		suiteletForm.addFieldGroup('custpage_group_patient', 'General Information <font size="3" color="red"><b>Scroll Down for More</b></font>');

		// create label 'Patient Information'
		suiteletForm.addField('custpage_patient_label', 'inlinehtml', '', null, 'custpage_group_patient').setDefaultValue('<h3>Patient Information</h3>');

		//		/**
		//		 * @type nlobjField
		//		 */
		//		var scrollDown1 = suiteletForm.addField('custpage_scroll_down1', 'inlinehtml', null, null, 'custpage_group_patient');
		//		scrollDown1.setDefaultValue('<font size="3" color="red"><b>Scroll Down for More</b></font>');
		//		scrollDown1.setLayoutType('startrow');

		// Patient Last Name
		/**
		 * @type nlobjField
		 */
		var patientLName = suiteletForm.addField('custpage_patient_lastname', 'text', 'Last Name', null, 'custpage_group_patient');
		patientLName.setDisplaySize(30);
		patientLName.setMandatory(true);
		patientLName.setLayoutType('startrow');

		// Patient First Name
		/**
		 * @type nlobjField
		 */
		var patientFName = suiteletForm.addField('custpage_patient_firstname', 'text', 'First Name', null, 'custpage_group_patient');
		patientFName.setDisplaySize(30);
		patientFName.setMandatory(true);
		patientFName.setLayoutType('endrow');

		// Primary Phone
		/**
		 * @type nlobjField
		 */
		var primaryPhone = suiteletForm.addField('custpage_primary_phone', 'phone', 'Primary Phone Number', null, 'custpage_group_patient');
		primaryPhone.setDisplaySize(20);
		primaryPhone.setMandatory(true);
		primaryPhone.setLayoutType('startrow');

		// Home Address
		/**
		 * @type nlobjField
		 */
		var homeAdd = suiteletForm.addField('custpage_home_addresss', 'text', 'Home Address', null, 'custpage_group_patient').setMandatory(true).setLayoutType('startrow');
		homeAdd.setDisplaySize(50);
		homeAdd.setMandatory(true);
		homeAdd.setLayoutType('endrow');

		// Home City
		suiteletForm.addField('custpage_home_city', 'text', 'City', null, 'custpage_group_patient').setMandatory(true).setDisplaySize(30).setLayoutType('startrow');

		// Home State
		/**
		 * @type nlobjField
		 */
		var fieldState = suiteletForm.addField('custpage_home_state', 'select', 'State', null, 'custpage_group_patient').setMandatory(true).setLayoutType('midrow');
		fieldState = getUsStates(fieldState);

		// Home Zip
		suiteletForm.addField('custpage_home_zip', 'text', 'Zip', null, 'custpage_group_patient').setMandatory(true).setLayoutType('endrow');
		// Gender
		suiteletForm.addField('custpage_gender', 'select', 'Gender', 'customlist_bio_gender', 'custpage_group_patient').setLayoutType('startrow');

		// Date of Birth
		suiteletForm.addField('custpage_date_of_birth', 'date', 'Patient Date Of Birth', null, 'custpage_group_patient').setMandatory(true).setLayoutType('endrow');

		// add fields for Practice Information

		// create label 'Practice Information'
		/**
		 * @type nlobjField
		 */
		var pracLabel = suiteletForm.addField('custpage_practice_label', 'inlinehtml', '', null, 'custpage_group_patient');
		pracLabel.setBreakType('startcol');
		pracLabel.setDefaultValue('<h3>Practice Information</h3>');

		/**
		 * @type nlobjField
		 */
		var prescriberLabel = suiteletForm.addField('custpage_prescriber_label', 'inlinehtml', null, null, 'custpage_group_patient');
		prescriberLabel.setDefaultValue('<font size="3" color="red"><b>Please select the prescriber for this referral using the \'Prescriber Name\' field.</b></font>');
		prescriberLabel.setLayoutType('startrow');

		// Prescriber Name
		/**
		 * @type nlobjField
		 */
		var prescriberName = suiteletForm.addField('custpage_prescriber_name', 'select', 'Prescriber Name', null, 'custpage_group_patient');

		if (nlapiGetUser() == '90480') {
			var prescriberId = '90515';
		} else {
			var prescriberId = nlapiGetUser();
		}
		prescriberName.addSelectOption('', '');
		prescriberName = getPracticeNames(prescriberName, prescriberId);
		prescriberName.setMandatory(true);
		prescriberName.setLayoutType('startrow');

		/**
		 * @type nlobjField
		 */
		var prescriberNameText = suiteletForm.addField('custpage_prescriber_name_text', 'text', 'Prescriber Name Text', null, 'custpage_group_patient').setDisplayType('hidden');
		prescriberNameText.setLayoutType('startrow');

		// Practice Name
		/**
		 * @type nlobjField
		 */
		var practiceName = suiteletForm.addField('custpage_practice_name', 'text', 'Practice Name', null, 'custpage_group_patient').setDisplayType('disabled');
		practiceName.setMandatory(true);
		practiceName.setLayoutType('endrow');
		practiceName.setDisplaySize(30);

		// Office Contact Name
		/**
		 * @type nlobjField
		 */
		var officeContactName = suiteletForm.addField('custpage_office_contact_name', 'text', 'Office Contact Name', null, 'custpage_group_patient');
		officeContactName.setMandatory(true);
		officeContactName.setLayoutType('startrow');
		officeContactName.setDisplaySize(30);

		// Office Contact Email
		/**
		 * @type nlobjField
		 */
		var officeContactEmail = suiteletForm.addField('custpage_office_contact_email', 'email', 'Office Contact Email', null, 'custpage_group_patient');
		officeContactEmail.setMandatory(true);
		officeContactEmail.setLayoutType('endrow');
		officeContactEmail.setDisplaySize(30);

		// Preferred Form of Communication
		/**
		 * @type nlobjField
		 */
		var preferredMethod = suiteletForm.addField('custpage_pref_form_com', 'select', 'Preferred Form of Communication', '', 'custpage_group_patient');
		preferredMethod.addSelectOption('', '', true);
		preferredMethod.addSelectOption('1', 'Call');
		preferredMethod.addSelectOption('2', 'FAX');
		preferredMethod.setMandatory(true);
		preferredMethod.setLayoutType('startrow');
		preferredMethod.setDisplaySize(50);

		// Phone Number
		/**
		 * @type nlobjField
		 */
		var presciberPhone = suiteletForm.addField('custpage_com_phone', 'text', 'Phone Number', null, 'custpage_group_patient').setDisplayType('disabled');
		presciberPhone.setMandatory(true);
		presciberPhone.setLayoutType('startrow');
		presciberPhone.setDisplaySize(15);

		// Fax Number
		/**
		 * @type nlobjField
		 */
		var faxNumber = suiteletForm.addField('custpage_com_fax', 'text', 'Fax Number', null, 'custpage_group_patient').setDisplayType('disabled');
		faxNumber.setMandatory(true);
		faxNumber.setLayoutType('endrow');
		faxNumber.setDisplaySize(15);

		// Deliver To Address
		/**
		 * @type nlobjField
		 */
		var prescriberAddress = suiteletForm.addField('custpage_deliver_to_address', 'text', 'Address', null, 'custpage_group_patient').setDisplayType('disabled');
		prescriberAddress.setMandatory(true);
		prescriberAddress.setLayoutType('startrow');
		prescriberAddress.setDisplaySize(30);

		// Deliver To City
		/**
		 * @type nlobjField
		 */
		var prescriberAddressCity = suiteletForm.addField('custpage_deliver_to_city', 'text', 'City', null, 'custpage_group_patient').setDisplayType('disabled');
		prescriberAddressCity.setMandatory(true);
		prescriberAddressCity.setLayoutType('endrow');
		prescriberAddressCity.setDisplaySize(30);

		// Deliver To State
		/**
		 * @type nlobjField
		 */
		var fieldDeliverState = suiteletForm.addField('custpage_deliver_to_state', 'select', 'State', null, 'custpage_group_patient').setMandatory(true).setLayoutType('startrow').setDisplayType('disabled');
		fieldDeliverState = getUsStates(fieldDeliverState);

		// Deliver To Zip
		/**
		 * @type nlobjField
		 */
		var prescriberAddressZip = suiteletForm.addField('custpage_deliver_to_zip', 'text', 'Zip', null, 'custpage_group_patient').setMandatory(true).setLayoutType('endrow').setDisplaySize(28).setDisplayType('disabled');

		// Presciber NPI#
		/**
		 * @type nlobjField
		 */
		var prescriberNPI = suiteletForm.addField('custpage_prescriber_npi', 'text', 'Prescriber NPI#', null, 'custpage_group_patient').setMandatory(true).setLayoutType('startrow').setDisplaySize(28);

		// Date Today
		/**
		 * @type nlobjField
		 */
		var dateToday = suiteletForm.addField('custpage_date_today', 'date', 'Today\'s Date', null, 'custpage_group_patient').setMandatory(true).setLayoutType('endrow').setDisplaySize(28).setDisplayType('disabled');
		dateToday.setDefaultValue(nlapiDateToString(new Date(), 'date'));

		// add field group for Insurance Information
		suiteletForm.addFieldGroup('custpage_insurance_info', 'Insurance Information     <font size="3" color="red"><b>Scroll Down for More</b></font>').setShowBorder(true);

		// create label 'Primary Medical Insurance'
		/**
		 * @type nlobjField
		 */
		var medInsuranceLabel = suiteletForm.addField('custpage_med_insurance_label', 'inlinehtml', '', null, 'custpage_insurance_info');
		medInsuranceLabel.setBreakType('startcol');
		medInsuranceLabel.setDefaultValue('<h3>Primary Medical Insurance</h3>');

		// Primary (Medical) Insurance
		suiteletForm.addField('custpage_primary_med_insurance', 'text', 'Primary (Medical) Insurance', null, 'custpage_insurance_info');

		// Medical ID Number
		suiteletForm.addField('custpage_medical_id_number', 'text', 'Medical ID Number', null, 'custpage_insurance_info');

		// Medical Group Number
		suiteletForm.addField('custpage_medical_group_number', 'text', 'Medical Group Number', null, 'custpage_insurance_info');

		// Policy Holder Name
		suiteletForm.addField('custpage_policy_holder_name', 'text', 'Policy Holder Name', null, 'custpage_insurance_info');

		// Medical Services Phone
		suiteletForm.addField('custpage_primary_member_phone', 'phone', 'Medical Services Phone', null, 'custpage_insurance_info');

		// create label 'Prescription Benefit Insurance'
		/**
		 * @type nlobjField
		 */
		var benInsuranceLabel = suiteletForm.addField('custpage_pres_ben_insurance_label', 'inlinehtml', '', null, 'custpage_insurance_info');
		benInsuranceLabel.setBreakType('startcol');
		benInsuranceLabel.setDefaultValue('<h3>Prescription Benefit Insurance</h3>');

		// Primary PBM Name
		suiteletForm.addField('custpage_primary_pbm_name', 'text', 'Prescription Insurance', null, 'custpage_insurance_info');

		// PBM Cardholder ID#
		suiteletForm.addField('custpage_pbm_cardholder_id', 'text', 'PBM Cardholder ID#', null, 'custpage_insurance_info');

		// Rx Group #
		suiteletForm.addField('custpage_rx_group', 'text', 'Rx Group #', null, 'custpage_insurance_info');

		// Rx Bin #
		suiteletForm.addField('custpage_rx_bin', 'text', 'Rx Bin #', null, 'custpage_insurance_info');

		// Rx PCN #
		suiteletForm.addField('custpage_rx_pcn', 'text', 'Rx PCN #', null, 'custpage_insurance_info');

		// PBM Phone Number
		suiteletForm.addField('custpage_pbm_phone_no', 'phone', 'PBM Phone Number', null, 'custpage_insurance_info');

		// Create Prescription Field Group
		suiteletForm.addFieldGroup('custpage_group_prescription', 'Prescription <font size="3" color="red"><b>Scroll Down for More</b></font>', null);

		/**
		 * @type nlobjField
		 */
		var disclaimer4 = suiteletForm.addField('custpage_pres_disclaimer4', 'inlinehtml', '', null, 'custpage_group_prescription');
		disclaimer4.setDefaultValue('');
		disclaimer4.setLayoutType('startrow', 'startcol');

		var prescriberText = [];
		prescriberText.push('The dental practitioner prescribing ARESTIN&reg will determine the appropriate course of therapy for the patient.Each prescription is a 30-day supply with no refills; a new prescription is required for each	 order.The prescription is for the patient listed on the prescription form and cannot be resold or used for any other patient');
		prescriberText.push('<br/>');
		prescriberText.push('<b><u>COMPLETE THE FOLLOWING PRESCRIPTION PRIOR TO FAXING.</u></b> The quantity dispensed represents no greater than a 30-day supply.');
		prescriberText.push('<br/>');
		prescriberText.push('<b>PRESCRIPTION: ARESTIN&reg; (minocycline HCI) Microspheres, 1 mg Cartridges</b>');
		prescriberText.push('<br/>');
		prescriberText.push('<b>SIG: For administration in the periodontal pocket only</b>');
		prescriberText.push('<br/>');
		prescriberText.push('<b>1 cartridge per site diagnosed</b>');
		prescriberText.push('<br/>');

		var instructionText = ['<center>', prescriberText.join('<br/>'), '</center>'].join('');
		/**
		 * @type nlobjField
		 */
		var instructions = suiteletForm.addField('custpage_sig_text', 'inlinehtml', '', null, 'custpage_group_prescription').setDisplaySize(500);
		instructions.setLayoutType('startrow', 'startcol');
		instructions.setDefaultValue(instructionText);

		suiteletForm.addField('custpage_pres_quantity', 'integer', 'Quantity', null, 'custpage_group_prescription').setLayoutType('startrow').setMandatory(true);

		// Create Prescription Field Group

		// section for legal text
		suiteletForm.addFieldGroup('custpage_consent', 'Prescriber Consent', null);

		/**
		 * @type nlobjField
		 */
		var disclaimer5 = suiteletForm.addField('custpage_pres_disclaimer5', 'inlinehtml', '', null, 'custpage_consent');
		disclaimer5.setDefaultValue('');
		disclaimer5.setLayoutType('startrow', 'startcol');

		var consentText = executionContext_global.getSetting('SCRIPT', 'custscript_arestin_suitelet_consent');
		consentText = ['<center>', consentText, '</center>'].join('');
		/**
		 * @type nlobjField
		 */
		var consent = suiteletForm.addField('custpage_consent_text', 'inlinehtml', '', null, 'custpage_consent').setDisplaySize(1000);
		consent.setLayoutType('startrow', 'startcol');
		consent.setDefaultValue(consentText);

		/**
		 * @type nlobjField
		 */
		var popUp = suiteletForm.addField('custpage_popup', 'inlinehtml', '', null, null);
		popUp.setDefaultValue(popUpText);
		popUp.setLayoutType('outsidebelow', 'startrow');

		suiteletForm.addSubmitButton('Submit and Print');
		suiteletForm.addButton('custpage_button_cancel', 'Cancel', "history.go('-1');");

		return suiteletForm;
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, ['Error Code:= ', error.getCode(), '<br/>Error Details:= ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, ['Error:= ', error.toString()].join(''));
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

/**
 * This function populates the select fields with the hardcoded names of the US States
 *
 * @param {nlobjField}
 *            stateField NetSuite Field Object
 * @returns {nlobjField}
 */
function getUsStates (stateField) {

	var funcName = 'getUsStates';

	var usStates = {
		'Alabama': 'AL',
		'Alaska': 'AK',
		'Arizona': 'AZ',
		'Arkansas': 'AR',
		'Armed Forces Americas': 'AA',
		'Armed Forces Europe': 'AE',
		'Armed Forces Pacific': 'AP',
		'California': 'CA',
		'Colorado': 'CO',
		'Connecticut': 'CT',
		'Delaware': 'DE',
		'District of Columbia': 'DC',
		'Florida': 'FL',
		'Georgia': 'GA',
		'Hawaii': 'HI',
		'Idaho': 'ID',
		'Illinois': 'IL',
		'Indiana': 'IN',
		'Iowa': 'IA',
		'Kansas': 'KS',
		'Kentucky': 'KY',
		'Louisiana': 'LA',
		'Maine': 'ME',
		'Maryland': 'MD',
		'Massachusetts': 'MA',
		'Michigan': 'MI',
		'Minnesota': 'MN',
		'Mississippi': 'MS',
		'Missouri': 'MO',
		'Montana': 'MT',
		'Nebraska': 'NE',
		'Nevada': 'NV',
		'New Hampshire': 'NH',
		'New Jersey': 'NJ',
		'New Mexico': 'NM',
		'New York': 'NY',
		'North Carolina': 'NC',
		'North Dakota': 'ND',
		'Ohio': 'OH',
		'Oklahoma': 'OK',
		'Oregon': 'OR',
		'Pennsylvania': 'PA',
		'Puerto Rico': 'PR',
		'Rhode Island': 'RI',
		'South Carolina': 'SC',
		'South Dakota': 'SD',
		'Tennessee': 'TN',
		'Texas': 'TX',
		'Utah': 'UT',
		'Vermont': 'VT',
		'Virginia': 'VA',
		'Washington': 'WA',
		'West Virginia': 'WV',
		'Wisconsin': 'WI',
		'Wyoming': 'WY',
		'American Samoa': 'AS',
		'Guam': 'GU',
		'Northern Mariana Islands': 'MP',
		'U.S. Minor Outlying Islands': 'UM',
		'U.S. Virgin Islands': 'VI'
	};

	var stateNames = Object.keys(usStates);

	stateField.addSelectOption('-1', '', true);
	for ( var x = 0; x < stateNames.length; x++) {
		var stateName = stateNames[x];
		var stateAbbrev = usStates[stateName];

		stateField.addSelectOption(stateAbbrev, stateName, false);
	}

	return stateField;
}
/**
 * This function generates the ARESTIN Referral Form PDF
 *
 * @param {Object}
 *            patient Custom object containing Patient data that was entered.
 * @param {Object}
 *            referral Custom object containing Referral data that was entered.
 * @param {nlobjRequest}
 *            request Request object
 * @returns
 */
function suitelet_generatePDF (request , response) {

	var funcName = 'suitelet_generatePDF';

	// disable DEBUG logs if Script Log Level is not Debug
	if (executionContext_global.getLogLevel() == 'DEBUG') {
		scriptLogger_global.enableDebug();
	}

	try {
		scriptLogger_global.audit(funcName, '==================== SUITELET START ====================');
		stopWatch_global.start();

		// get the internal id of the patient, referral and prescriber
		var patientId = request.getParameter('patientid');
		var referrallId = request.getParameter('referralid');
		var prescriberid = request.getParameter('prescriberid');
		var debug = request.getParameter('debug');
		var button = request.getParameter('button');

		var pdfXml = processTemplate(patientId, referrallId, prescriberid, button);
		scriptLogger_global.debug(funcName, ['Updated XML Template:= ', nlapiEscapeXML(pdfXml)].join(''));

		if (debug === 'T') {
			response.setContentType('XMLDOC', 'arestin_referral.xml', 'inline');
			response.write(pdfXml);
		} else {

			var pdf = nlapiXMLToPDF(pdfXml);

			// get the patient id and the case number
			var patientNumber = nlapiLookupField('customer', patientId, 'entityid');
			scriptLogger_global.audit(funcName, ['Patient Number:= ', patientNumber].join(''));

			var caseNumber = nlapiLookupField('supportcase', referrallId, 'casenumber');
			scriptLogger_global.audit(funcName, ['Referral Number:= ', caseNumber].join(''));

			// create the file name
			var fileName = [patientNumber, ' - ', caseNumber, '.pdf'].join('');

			var file = nlapiCreateFile(fileName, 'PDF', pdf.getValue());
			file.setFolder(executionContext_global.getSetting('SCRIPT', 'custscript_referral_output_folder'));
			var fileId = nlapiSubmitFile(file);

			// attach the file to the created Referral record
			var support = nlapiLoadRecord('supportcase', referrallId);
			support.setFieldValue('custevent_arestin_referral_pdf', fileId);
			nlapiSubmitRecord(support, true, true);

			scriptLogger_global.debug(funcName, ['File [Internal Id:= ', fileId, '] attached to Referral [Internal Id:=', referrallId, '] record.'].join(''));

			if (button == 'T') {
				nlapiSetRedirectURL('record', 'supportcase', referrallId);
			} else {
				response.setContentType('PDF', referrallId + '.pdf', 'inline');
				response.write(pdf.getValue());
			}
		}

		stopWatch_global.stop();
		scriptLogger_global.audit(funcName, ['=================== SUITELET END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));

	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, ['Error Code:= ', error.getCode(), '<br/>Error Details:= ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, ['Error:= ', error.toString()].join(''));
			throw nlapiCreateError('99999', error.toString());
		}
	}

}
function processTemplate (patientId , referrallId , prescriberid , button) {

	var funcName = 'processTemplate';

	var patientData = getPatientData(patientId, button);
	patientData = removeNull(patientData);
	scriptLogger_global.debug(funcName, ['Patient Data:= ', JSON.stringify(patientData)].join(''));

	var referralData = getReferralData(referrallId, button);
	referralData = removeNull(referralData);
	scriptLogger_global.debug(funcName, ['Referral Data:= ', JSON.stringify(referralData)].join(''));

	if (button != 'T') {
		var prescriberData = JSON.parse(referralData.enteredPrescriberData);

	} else {
		var prescriberData = getPrescriberData(prescriberid);
	}

	prescriberData = removeNull(prescriberData);
	scriptLogger_global.debug(funcName, ['Prescriber Data:= ', JSON.stringify(prescriberData)].join(''));

	// get pdf template from the script parameter
	var pdfTemplate = executionContext_global.getSetting('script', 'custscript_pdf_template');
	var formLogo = executionContext_global.getSetting('script', 'custscript_pdf_logo');
	var formFooter = executionContext_global.getSetting('script', 'custscript_arestin_form_footer');
	var arestinShipping = executionContext_global.getSetting('script', 'custscript_arestin_shipping_info');
	var arestinCheckbox1 = executionContext_global.getSetting('script', 'custscript_arestin_pres_consent_chck1');
	var arestinCheckbox2 = executionContext_global.getSetting('script', 'custscript_arestin_pres_consent_chk2');
  var patient_sec = executionContext_global.getSetting('script', 'custscript_bs_patient_sec_logo');
  var prescriber_sec = executionContext_global.getSetting('script', 'custscript_bs_prescriber_sec_logo');
  var presciber_sig_sec = executionContext_global.getSetting('script', 'custscript_bs_prescriber_sig_sec_logo');
  var prescriber_consent_sec = executionContext_global.getSetting('script', 'custscript_bs_prescriber_consent_sec_log');
  var shipping_logo = executionContext_global.getSetting('script', 'custscript_bs_shipping_logo');
  var insurance_logo = executionContext_global.getSetting('script', 'custscript_bs_insurance_logo');
  var terms_logo = executionContext_global.getSetting('script', 'custscript_bs_terms_logo');
  var terms_lang = executionContext_global.getSetting('script', 'custscript_bs_terms_lang');
  var header2 = executionContext_global.getSetting('script', 'custscript_bs_header2');
  var footer2 = executionContext_global.getSetting('script', 'custscript_bs_footer2');
  var footer1 = executionContext_global.getSetting('script', 'custscript_bs_footer1');
	/**
	 * @type String
	 */
	var xmlTemplate = nlapiLoadFile(pdfTemplate).getValue();
	scriptLogger_global.debug(funcName, ['XML Template:= ', nlapiEscapeXML(xmlTemplate)].join(''));

  // generate the image
  var imageURL = getImageURL(formLogo);

  // insert logo to the template
  xmlTemplate = xmlTemplate.replace(/{arestin_logo}/g, imageURL);

  imageURL = getImageURL(patient_sec);
  // insert logo to the template
  xmlTemplate = xmlTemplate.replace(/{patient_logo}/g, imageURL);

  imageURL = getImageURL(prescriber_sec);
  xmlTemplate = xmlTemplate.replace(/{prescriber_logo}/g, imageURL);

  imageURL = getImageURL(presciber_sig_sec);
  xmlTemplate = xmlTemplate.replace(/{prescriber_sig_logo}/g, imageURL);

  imageURL = getImageURL(prescriber_consent_sec);
  xmlTemplate = xmlTemplate.replace(/{prescriber_consent_logo}/g, imageURL);

  imageURL = getImageURL(shipping_logo);
  xmlTemplate = xmlTemplate.replace(/{shipping_logo}/g, imageURL);

  imageURL = getImageURL(insurance_logo);
  xmlTemplate = xmlTemplate.replace(/{insurance_logo}/g, imageURL);

  imageURL = getImageURL(terms_logo);
  xmlTemplate = xmlTemplate.replace(/{terms_logo}/g, imageURL);

  imageURL = getImageURL(terms_lang);
  xmlTemplate = xmlTemplate.replace(/{terms-conditions}/g, imageURL);

  //  imageURL = constructImageURL(formLogo);
  //  xmlTemplate = xmlTemplate.replace(/{arestin_logo}/g, imageURL);

  imageURL = getImageURL(header2);
  xmlTemplate = xmlTemplate.replace(/{arestin_logo2}/g, imageURL);

  imageURL = getImageURL(footer2);
  xmlTemplate = xmlTemplate.replace(/{arestin_footer_logo2}/g, imageURL);

  imageURL = getImageURL(footer1);
  xmlTemplate = xmlTemplate.replace(/{arestin_footer_logo1}/g, imageURL);
  //
  //  // insert the page footer
  //  xmlTemplate = xmlTemplate.replace(/{arestin_footer}/g, nlapiEscapeXML(formFooter));

  // insert shipping information
  xmlTemplate = xmlTemplate.replace(/{custscript_arestin_shipping_info}/g, arestinShipping);

  // insert checkbox 1 language
  xmlTemplate = xmlTemplate.replace(/{custscript_arestin_pres_consent_chk1}/g, arestinCheckbox1);

  // insert checkbox 2 language
  xmlTemplate = xmlTemplate.replace(/{custscript_arestin_pres_consent_chk2}/g, arestinCheckbox2);

  // insert terms language
  xmlTemplate = xmlTemplate.replace(/{terms-conditions}/g, terms_lang);

	// insert the page footer
	xmlTemplate = xmlTemplate.replace(/{arestin_footer}/g, nlapiEscapeXML(formFooter));

	// insert shipping information
	xmlTemplate = xmlTemplate.replace(/{custscript_arestin_shipping_info}/g, arestinShipping);

	// insert checkbox 1 language
	xmlTemplate = xmlTemplate.replace(/{custscript_arestin_pres_consent_chk1}/g, arestinCheckbox1);

	// insert checkbox 2 language
	xmlTemplate = xmlTemplate.replace(/{custscript_arestin_pres_consent_chk2}/g, arestinCheckbox2);

	// insert the Patient information entered on the referral form
	xmlTemplate = xmlTemplate.replace(/{custpage_patient_lastname}/g, nlapiEscapeXML(patientData.custpage_patient_lastname));
	xmlTemplate = xmlTemplate.replace(/{custpage_patient_firstname}/g, nlapiEscapeXML(patientData.custpage_patient_firstname));
	xmlTemplate = xmlTemplate.replace(/{custpage_primary_phone}/g, nlapiEscapeXML(patientData.custpage_primary_phone));
	xmlTemplate = xmlTemplate.replace(/{custpage_home_addresss}/g, nlapiEscapeXML(patientData.custpage_home_addresss));
	xmlTemplate = xmlTemplate.replace(/{custpage_home_city}/g, nlapiEscapeXML(patientData.custpage_home_city));
	xmlTemplate = xmlTemplate.replace(/{custpage_home_state}/g, nlapiEscapeXML(patientData.custpage_home_state));
	xmlTemplate = xmlTemplate.replace(/{custpage_home_zip}/g, nlapiEscapeXML(patientData.custpage_home_zip));
	xmlTemplate = xmlTemplate.replace(/{custpage_gender}/g, nlapiEscapeXML(patientData.custpage_gender));
	xmlTemplate = xmlTemplate.replace(/{custpage_date_of_birth}/g, nlapiEscapeXML(patientData.custpage_date_of_birth));

	// insert the Prescriber information entered on the referral form
	xmlTemplate = xmlTemplate.replace(/{custpage_practice_name}/g, nlapiEscapeXML(prescriberData.custpage_practice_name));
	if (button != 'T') {
		xmlTemplate = xmlTemplate.replace(/{custpage_prescriber_name}/g, nlapiEscapeXML(prescriberData.custpage_prescriber_name_text));
	} else {
		xmlTemplate = xmlTemplate.replace(/{custpage_prescriber_name}/g, nlapiEscapeXML(prescriberData.custpage_prescriber_name));
	}

	xmlTemplate = xmlTemplate.replace(/{custpage_office_contact_name}/g, nlapiEscapeXML(prescriberData.custpage_office_contact_name));
	xmlTemplate = xmlTemplate.replace(/{custpage_office_contact_email}/g, nlapiEscapeXML(prescriberData.custpage_office_contact_email));
	xmlTemplate = xmlTemplate.replace(/{custpage_com_phone}/g, nlapiEscapeXML(prescriberData.custpage_com_phone));
	xmlTemplate = xmlTemplate.replace(/{custpage_com_fax}/g, nlapiEscapeXML(prescriberData.custpage_com_fax));
	xmlTemplate = xmlTemplate.replace(/{custpage_deliver_to_address}/g, nlapiEscapeXML(prescriberData.custpage_deliver_to_address));
	xmlTemplate = xmlTemplate.replace(/{custpage_deliver_to_city}/g, nlapiEscapeXML(prescriberData.custpage_deliver_to_city));
	xmlTemplate = xmlTemplate.replace(/{custpage_deliver_to_state}/g, nlapiEscapeXML(prescriberData.custpage_deliver_to_state));
	xmlTemplate = xmlTemplate.replace(/{custpage_deliver_to_zip}/g, nlapiEscapeXML(prescriberData.custpage_deliver_to_zip));
	xmlTemplate = xmlTemplate.replace(/{custpage_prescriber_npi}/g, nlapiEscapeXML(prescriberData.custpage_prescriber_npi));
	xmlTemplate = xmlTemplate.replace(/{custpage_date_today}/g, nlapiEscapeXML(nlapiDateToString(new Date(), 'date')));

	// insert insurance information
	xmlTemplate = xmlTemplate.replace(/{custpage_primary_med_insurance}/g, nlapiEscapeXML(referralData.custpage_primary_med_insurance));
	xmlTemplate = xmlTemplate.replace(/{custpage_primary_pbm_name}/g, nlapiEscapeXML(referralData.custpage_primary_pbm_name));
	xmlTemplate = xmlTemplate.replace(/{custpage_policy_holder_name}/g, nlapiEscapeXML(referralData.custpage_policy_holder_name));
	xmlTemplate = xmlTemplate.replace(/{custpage_pbm_cardholder_id}/g, nlapiEscapeXML(referralData.custpage_pbm_cardholder_id));
	xmlTemplate = xmlTemplate.replace(/{custpage_medical_id_number}/g, nlapiEscapeXML(referralData.custpage_medical_id_number));
	xmlTemplate = xmlTemplate.replace(/{custpage_rx_group}/g, nlapiEscapeXML(referralData.custpage_rx_group));
	xmlTemplate = xmlTemplate.replace(/{custpage_medical_group_number}/g, nlapiEscapeXML(referralData.custpage_medical_group_number));
	xmlTemplate = xmlTemplate.replace(/{custpage_rx_bin}/g, nlapiEscapeXML(referralData.custpage_rx_bin));
	xmlTemplate = xmlTemplate.replace(/{custpage_primary_member_phone}/g, nlapiEscapeXML(referralData.custpage_primary_member_phone));
	xmlTemplate = xmlTemplate.replace(/{custpage_rx_pcn}/g, nlapiEscapeXML(referralData.custpage_rx_pcn));
	xmlTemplate = xmlTemplate.replace(/{custpage_pbm_phone_no}/g, nlapiEscapeXML(referralData.custpage_pbm_phone_no));
	xmlTemplate = xmlTemplate.replace(/{custpage_pres_quantity}/g, nlapiEscapeXML(referralData.custpage_pres_quantity));

	return xmlTemplate;
}
function removeNull (dataCollection) {

	for ( var data in dataCollection) {
		if (generalFunctions_global.isNullOrUndefined(dataCollection[data]) === true) {
			dataCollection[data] = '';
		}
	}

	return dataCollection;
}
function getPrescriberData (partnerInternalId) {

	var funcName = 'getPrescriberData';

	try {
		/**
		 * @type nlobjRecord
		 * @record partner
		 */
		var partner = nlapiLoadRecord('partner', partnerInternalId);

		var practiceName = partner.getFieldValue('companyname');
		var prescriberName = partner.getFieldValue('entityid');

		/**
		 * @type Array
		 */
		var prescriberNameArray = prescriberName.split(' ');
		prescriberNameArray.shift();
		prescriberName = prescriberNameArray.join(' ');

		var prescriberPhone = partner.getFieldValue('phone');
		var officeContactName = partner.getFieldValue('custentity_bio_phys_off_cntct');
		var officeContactPhone = partner.getFieldValue('homephone');
		var officeContactExtension = partner.getFieldValue('custentityoff_con_phon_ext');
		var officeContactEmail = partner.getFieldValue('email');
		var preferredMethod = partner.getFieldValue('custentity_bio_phys_md_pref');
		var faxNumber = partner.getFieldValue('fax');
		var prescriberNPI = partner.getFieldValue('custentity_bio_phys_npi');
		var prescriberAddress = partner.getFieldValue('billaddr1');
		var prescriberAddressCity = partner.getFieldValue('billcity');
		var prescriberAddressState = partner.getFieldValue('billstate');
		var prescriberAddressZip = partner.getFieldValue('billzip');

		return {
			'custpage_practice_name': practiceName,
			'custpage_prescriber_name': prescriberName,
			'custpage_com_phone': prescriberPhone,
			'custpage_office_contact_name': officeContactName,
			'officeContactPhone': officeContactPhone,
			'officeContactExtension': officeContactExtension,
			'custpage_office_contact_email': officeContactEmail,
			'preferredMethod': preferredMethod,
			'custpage_com_fax': faxNumber,
			'custpage_prescriber_npi': prescriberNPI,
			'custpage_deliver_to_address': prescriberAddress,
			'custpage_deliver_to_city': prescriberAddressCity,
			'custpage_deliver_to_state': prescriberAddressState,
			'custpage_deliver_to_zip': prescriberAddressZip
		};
	} catch (error) {
		return {
			'custpage_practice_name': '',
			'custpage_prescriber_name': '',
			'custpage_com_phone': '',
			'custpage_office_contact_name': '',
			'officeContactPhone': '',
			'officeContactExtension': '',
			'custpage_office_contact_email': '',
			'preferredMethod': '',
			'custpage_com_fax': '',
			'custpage_prescriber_npi': '',
			'custpage_deliver_to_address': '',
			'custpage_deliver_to_city': '',
			'custpage_deliver_to_state': '',
			'custpage_deliver_to_zip': ''
		};
	}

}
function getPatientData (patientId , button) {

	var funcName = 'getPatientData';

	try {
		/**
		 * @type nlobjRecord
		 * @record customer
		 */
		var patientRec = nlapiLoadRecord('customer', patientId);

		var firstName = patientRec.getFieldValue('firstname');
		var lastName = patientRec.getFieldValue('lastname');

		var primaryPhone = patientRec.getFieldValue('phone');

		if (generalFunctions_global.isEmpty(primaryPhone) === true) {
			var primaryPhone = patientRec.getFieldValue('custentity_bio_phone');
		}

		var dateOfBirthEncrypted = patientRec.getFieldValue('custentity_bio_date_of_birth');
		var dateOfBirth = patientRec.getFieldValue('custentity_bio_hidden_dob');

		var gender = patientRec.getFieldText('custentity_bio_patient_gender');

		// get the address
		var homeAddress = patientRec.getLineItemValue('addressbook', 'addr1', 1);
		var homeCity = patientRec.getLineItemValue('addressbook', 'city', 1);
		var homeState = patientRec.getLineItemValue('addressbook', 'state', 1);
		var homeZip = patientRec.getLineItemValue('addressbook', 'zip', 1);

		var patientData = {
			'custpage_patient_lastname': lastName,
			'custpage_patient_firstname': firstName,
			'custpage_primary_phone': primaryPhone,
			'custpage_home_addresss': homeAddress,
			'custpage_home_city': homeCity,
			'custpage_home_state': homeState,
			'custpage_home_zip': homeZip,
			'custpage_gender': gender,
			/*'custpage_date_of_birth': dateOfBirth,*/
			'custpage_date_of_birth': dateOfBirthEncrypted,
			'custpage_dob': dateOfBirthEncrypted
		};

		var doNotDecrypt = ['custpage_home_state', 'custpage_home_zip', 'custpage_gender'];
		// decrypt the patient data
		for ( var fieldName in patientData) {

			if (generalFunctions_global.inArray(fieldName, doNotDecrypt) === false) {
				var fieldValue = patientData[fieldName];

				if (generalFunctions_global.isEmpty(fieldValue) === false) {
					var decrypted = decryptField(fieldValue);

					patientData[fieldName] = decrypted;

					scriptLogger_global.debug(funcName, ['Field Name:= ', fieldName, ' Field Value:= ', decrypted].join(''));
				}
			}
		}

		return patientData;
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
		}
	}

}
function getReferralData (referralId , button) {

	var funcName = 'getReferralData';

	try {

		if (button != 'T') {
			/**
			 * @type nlobjRecord
			 * @record supportcase
			 */
			var referralRecord = nlapiLoadRecord('supportcase', referralId);

			var primaryMedicalInsurance = referralRecord.getFieldValue('custevent_primary_medical_insurance');
			var medicalIdNumber = referralRecord.getFieldValue('custevent_medical_id_number');
			var medicalGroupNumber = referralRecord.getFieldValue('custevent_medical_group_number');
			var policyHolderName = referralRecord.getFieldValue('custevent_policy_holder_name');
			var primaryMemberPhone = referralRecord.getFieldValue('custevent_medical_services_phone');
			var primaryPbmName = referralRecord.getFieldValue('custevent_primary_pbm_name');
			var pbmCardholderId = referralRecord.getFieldValue('custevent_pbm_cardholder_id');
			var rxGroup = referralRecord.getFieldValue('custevent_rx_group_no');
			var rxBin = referralRecord.getFieldValue('custevent_rx_bin_no');
			var rxPcn = referralRecord.getFieldValue('custevent_rx_pcn_no');
			var pbmPhoneNo = referralRecord.getFieldValue('custevent_pbm_phone_number');
			var prescriptionQty = referralRecord.getFieldValue('custevent_prescription_qty');
			var enteredPrescriberData = referralRecord.getFieldValue('custevent_prescriber_form_data');

		} else {
			// get data from the associated Case Insurance Data record
			var caseInsurance = getCaseInsurance(referralId);

			if (generalFunctions_global.isNullOrUndefined(caseInsurance) === false) {
				var primaryMedicalInsurance = caseInsurance.custpage_primary_med_insurance;// Medical Insurance
				var primaryPbmName = caseInsurance.custpage_primary_pbm_name;// Insured Name
				var prescriptionQty = caseInsurance.quantity;// Quantity
				var pbmCardholderId = caseInsurance.custpage_pbm_cardholder_id;//Drug Cardholder Id
				var medicalIdNumber = caseInsurance.custpage_medical_id_number;// Policy Number
				var rxGroup = caseInsurance.custpage_rx_group;// Group Number
				var medicalGroupNumber = caseInsurance.custpage_medical_group_number;// Group Number
				var primaryMemberPhone = caseInsurance.custpage_primary_member_phone;// Member Services Phone
				var pbmPhoneNo = caseInsurance.custpage_pbm_phone_no;// Phone Number
				var rxBin = caseInsurance.custpage_rx_bin;// Bin #
				var rxPcn = caseInsurance.custpage_rx_pcn; // Rx PCN #

			}
		}

		var referralData = {
			'custpage_primary_med_insurance': primaryMedicalInsurance,
			'custpage_medical_id_number': medicalIdNumber,
			'custpage_medical_group_number': medicalGroupNumber,
			'custpage_policy_holder_name': policyHolderName,
			'custpage_primary_member_phone': primaryMemberPhone,
			'custpage_primary_pbm_name': primaryPbmName,
			'custpage_pbm_cardholder_id': pbmCardholderId,
			'custpage_rx_group': rxGroup,
			'custpage_rx_bin': rxBin,
			'custpage_rx_pcn': rxPcn,
			'custpage_pbm_phone_no': pbmPhoneNo,
			'custpage_pres_quantity': prescriptionQty,
			'enteredPrescriberData': enteredPrescriberData
		};

		if (button == 'T') {
			var doNotDecrypt = ['enteredPrescriberData', 'custpage_pres_quantity', 'custpage_pbm_phone_no', 'custpage_primary_member_phone'];
			// decrypt the patient data
			for ( var fieldName in referralData) {

				if (generalFunctions_global.inArray(fieldName, doNotDecrypt) === false) {
					var fieldValue = referralData[fieldName];

					if (generalFunctions_global.isEmpty(fieldValue) === false) {
						var decrypted = decryptField(fieldValue);

						referralData[fieldName] = decrypted;

						scriptLogger_global.debug(funcName, ['Field Name:= ', fieldName, ' Field Value:= ', decrypted].join(''));
					}
				}
			}
		}

		return referralData;
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
		}
	}
}
function getImageURL(file_id) {
  // Get Netsuite URL
  var netsuiteUrl = getNetsuiteURL(false);

  // Get Logo URL
  /**
   * @type nlobjFile
   */
  var image = nlapiLoadFile(file_id);
  var image_url = nlapiEscapeXML(image.getURL());

  return image_url;
}
function constructImageURL (strLogo) {

	// Get Netsuite URL
	var netsuiteUrl = getNetsuiteURL(false);

	// Get Logo URL
	/**
	 * @type nlobjFile
	 */
	var image = nlapiLoadFile(strLogo);
	var image_url = nlapiEscapeXML(image.getURL());

	var image_src = '<img src=\"{stMainLogoURL}\"/>';
	image_src = image_src.replace(/{stMainLogoURL}/g, netsuiteUrl + image_url);

	return image_src;
}

function getNetsuiteURL (nsdebugger) {

	var linkUrl;
	switch (nlapiGetContext().getEnvironment()) {
		case "PRODUCTION":
			if (nsdebugger === true) {
				linkUrl = 'https://bioprod1-debugger-netsuite-com.biosolutia.com';
			} else {
				linkUrl = 'https://bioprod1-system-netsuite-com.biosolutia.com';
			}

			break;

		case "SANDBOX":
			if (nsdebugger === true) {
				linkUrl = 'https://bioprod1-debugger-sandbox-netsuite-com.biosolutia.com';
			} else {
				linkUrl = 'https://bioprod1-system-sandbox-netsuite-com.biosolutia.com';
			}
			break;

		case "BETA":
			if (nsdebugger === true) {
				linkUrl = 'https://bioprod1-debugger-beta-netsuite-com.biosolutia.com';
			} else {
				linkUrl = 'https://bioprod1-system-beta-netsuite-com.biosolutia.com';
			}
			break;
	}

	return linkUrl;
}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 *
 * @appliedtorecord recordType
 * @returns {Boolean} True to continue save, false to abort save
 */
function saveRecord_arestinValidation () {

	var funcName = 'saveRecord_arestinValidation';

	try {

		// check if 'Medical Insurance' and 'Prescription Insurance' is blank
		var medicalInsurance = nlapiGetFieldValue('custpage_primary_med_insurance');
		var prescriptionInsurance = nlapiGetFieldValue('custpage_primary_pbm_name');

		if (isEmpty(medicalInsurance) === true) {
			alert('Please put "Unknown" if the "Primary (Medical) Insurance" is not available.');
			return false;
		}

		if (isEmpty(prescriptionInsurance) === true) {
			alert('Please put "Unknown" if the "Prescription Insurance" information is not available.');
			return false;
		}

		return true;
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
/**
 * This function gets the data entered by the user in the Suitelet and creates the Patient record
 *
 * @param {nlobjRequest}
 *            request Request object
 */
function createPatientRecord (request) {

	var funcName = 'createPatientRecord';
	try {
		// get the data entered in the Suitelet

		var lastName = request.getParameter('custpage_patient_lastname');
		var firstName = request.getParameter('custpage_patient_firstname');
		var primaryPhone = request.getParameter('custpage_primary_phone');
		var homeAddress = request.getParameter('custpage_home_addresss');
		var homeCity = request.getParameter('custpage_home_city');
		var homeState = request.getParameter('custpage_home_state');
		var homeZip = request.getParameter('custpage_home_zip');
		var gender = request.getParameter('custpage_gender');
		var dateOfBirth = request.getParameter('custpage_date_of_birth');

		var parameters = request.getParameter('custpage_script_parameters');
		var scriptParameters = JSON.parse(parameters);
		var primaryDiagnosis = scriptParameters.primaryDiagnosis;
		var diagnosisGroup = scriptParameters.diagnosisGroup;
		var timeZone = scriptParameters.timeZone;

		var patientData = {
			'custpage_patient_lastname': lastName,
			'custpage_patient_firstname': firstName,
			'custpage_primary_phone': primaryPhone,
			'custpage_home_addresss': homeAddress,
			'custpage_home_city': homeCity,
			'custpage_home_state': homeState,
			'custpage_home_zip': homeZip,
			'custpage_gender': gender,
			'custpage_date_of_birth': dateOfBirth,
			'message': '',
			'patientId': ''
		};

		nlapiLogExecution('DEBUG', funcName, ['Entered Patiend Data:= ', JSON.stringify(patientData)].join(''));

		/**
		 * @type nlobjRecord
		 * @record customer
		 */
		var patientRec = nlapiCreateRecord('customer', {
			recordmode: 'dynamic'
		});
		patientRec.setFieldValue('firstname', encryptField('firstname', firstName));
		patientRec.setFieldValue('lastname', encryptField('lastname', lastName));
		//		patientRec.setFieldValue('phone', encryptField('phone', primaryPhone));
		var phoneEncrypted = encryptField('phone', primaryPhone);
		patientRec.setFieldValue('custentity_bio_phone', phoneEncrypted);
		patientRec.setFieldValue('custentity_bio_hidden_dob', dateOfBirth);
		patientRec.setFieldValue('custentity_bio_date_of_birth', encryptField('custentity_bio_date_of_birth', dateOfBirth));
		patientRec.setFieldValue('custentity_bio_patient_gender', gender);
		patientRec.setFieldValue('custentity_bio_time_zone', timeZone);
		patientRec.setFieldValue('custentity_bio_primary_diagnosis', primaryDiagnosis);
		patientRec.setFieldValue('custentity_bio_diagnosis_group', diagnosisGroup);

		var partnerInternalId = request.getParameter('custpage_prescriber_name');

		// this if for testing purposes only
		if (partnerInternalId == '81100') {
			partnerInternalId = 82492;
		}

		//	patientRec.setFieldValue('partner', nlapiGetUser());
		patientRec.setFieldValue('partner', partnerInternalId);

		// add address
		patientRec.selectNewLineItem('addressbook');
		patientRec.setCurrentLineItemValue('addressbook', 'isresidential', 'T');
		patientRec.setCurrentLineItemValue('addressbook', 'defaultbilling', 'T');
		patientRec.setCurrentLineItemValue('addressbook', 'addr1', encryptField('addr1', homeAddress));
		patientRec.setCurrentLineItemValue('addressbook', 'city', encryptField('city', homeCity));
		patientRec.setCurrentLineItemValue('addressbook', 'state', homeState);
		patientRec.setCurrentLineItemValue('addressbook', 'zip', homeZip);
		patientRec.commitLineItem('addressbook');

		var id = nlapiSubmitRecord(patientRec, true, true);
		patientData.patientId = id;

		return patientData;
	} catch (error) {
		var message = ['There was a problem with creating the Patient record.\nThe system returned the following error:= '];
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			message.push(error.getDetails());
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			message.push(error.toString());
		}
		patientData.message = message.join('');
		return patientData;
	}

}
function createReferralCase (patientId , request) {

	var funcName = 'createReferralCase';
	try {

		var parameters = request.getParameter('custpage_script_parameters');
		var scriptParameters = JSON.parse(parameters);
		// get the values of the script parameters
		var statusCategory = scriptParameters.statusCategory;
		var referralSubject = scriptParameters.referralSubject;
		var referralOrigin = scriptParameters.referralOrigin;
		var referralInfo = scriptParameters.referralInfo;
		var referralStatus = scriptParameters.referralStatus;
		var referralPriority = scriptParameters.referralPriority;
		var referralFeeOption = scriptParameters.referralFeeOption;
		var creditCardAuthForm = scriptParameters.creditCardAuthForm;

		// Set the hidden Insurance Information field values to be used for sourcing in the Case Referral Data custom record
		var primaryMedicalInsurance = request.getParameter('custpage_primary_med_insurance');
		var medicalIdNumber = request.getParameter('custpage_medical_id_number');
		var medicalGroupNumber = request.getParameter('custpage_medical_group_number');
		var policyHolderName = request.getParameter('custpage_policy_holder_name');
		var primaryMemberPhone = request.getParameter('custpage_primary_member_phone');

		var primaryPbmName = request.getParameter('custpage_primary_pbm_name');
		var pbmCardholderId = request.getParameter('custpage_pbm_cardholder_id');
		var rxGroup = request.getParameter('custpage_rx_group');
		var rxBin = request.getParameter('custpage_rx_bin');
		var rxPcn = request.getParameter('custpage_rx_pcn');
		var pbmPhoneNo = request.getParameter('custpage_pbm_phone_no');

		var prescriptionQty = request.getParameter('custpage_pres_quantity');

		// get the preferred form of communication of the partner
		var preferredComm = request.getParameter('custpage_pref_form_com');

		var partnerInternalId = request.getParameter('custpage_prescriber_name');

		var patientPhone = request.getParameter('custpage_primary_phone');

		var referralData = {
			'custpage_primary_med_insurance': primaryMedicalInsurance,
			'custpage_medical_id_number': medicalIdNumber,
			'custpage_medical_group_number': medicalGroupNumber,
			'custpage_policy_holder_name': policyHolderName,
			'custpage_primary_member_phone': primaryMemberPhone,
			'custpage_primary_pbm_name': primaryPbmName,
			'custpage_pbm_cardholder_id': pbmCardholderId,
			'custpage_rx_group': rxGroup,
			'custpage_rx_bin': rxBin,
			'custpage_rx_pcn': rxPcn,
			'custpage_pbm_phone_no': pbmPhoneNo,
			'custpage_pres_quantity': prescriptionQty,
			'internalId': '',
			'partnerIntId': partnerInternalId,
			'message': ''
		};

		// create the Case record
		/**
		 * @type nlobjRecord
		 * @record supportcase
		 */
		var referralRecord = nlapiCreateRecord('supportcase', {
			recordmode: 'dynamic'
		});

		referralRecord.setFieldValue('custevent_bio_referral_status', statusCategory);
		referralRecord.setFieldValue('title', referralSubject);
		referralRecord.setFieldValue('company', patientId);
		referralRecord.setFieldValue('phone', patientPhone);
		referralRecord.setFieldValue('status', referralStatus);
		referralRecord.setFieldValue('priority', referralPriority);
		referralRecord.setFieldValue('custeventbio_ref_origin', referralOrigin);
		referralRecord.setFieldValue('custeventbio_ref_info', referralInfo);
		referralRecord.setFieldValue('custevent_bio_phys_md_pref_referral', preferredComm);
		referralRecord.setFieldValue('custeventbio_fee_for_service', referralFeeOption);
		referralRecord.setFieldValue('custevent_bio_cc_auth_form', creditCardAuthForm);

		// this if for testing purposes only
		if (partnerInternalId == '81100') {
			partnerInternalId = 82492;
		}

		referralRecord.setFieldValue('custevent_bio_physician', partnerInternalId);

		referralRecord.setFieldValue('custevent_primary_medical_insurance', primaryMedicalInsurance);
		referralRecord.setFieldValue('custevent_medical_id_number', medicalIdNumber);
		referralRecord.setFieldValue('custevent_medical_group_number', medicalGroupNumber);
		referralRecord.setFieldValue('custevent_policy_holder_name', policyHolderName);
		referralRecord.setFieldValue('custevent_medical_services_phone', primaryMemberPhone);
		referralRecord.setFieldValue('custevent_primary_pbm_name', primaryPbmName);
		referralRecord.setFieldValue('custevent_pbm_cardholder_id', pbmCardholderId);
		referralRecord.setFieldValue('custevent_rx_group_no', rxGroup);
		referralRecord.setFieldValue('custevent_rx_bin_no', rxBin);
		referralRecord.setFieldValue('custevent_rx_pcn_no', rxPcn);
		referralRecord.setFieldValue('custevent_pbm_phone_number', pbmPhoneNo);
		referralRecord.setFieldValue('custevent_prescription_qty', prescriptionQty);

		var enteredPrescriberData = getEnteredPrescriberData(request);
		scriptLogger_global.debug(funcName, ['Prescriber Data Entered on Form:= ', JSON.stringify(enteredPrescriberData)].join(''));

		referralRecord.setFieldValue('custevent_prescriber_form_data', JSON.stringify(enteredPrescriberData));

		var id = nlapiSubmitRecord(referralRecord, false, true);

		referralData.internalId = id;

		return referralData;
	} catch (error) {
		var message = ['There was a problem with creating the Referral record.\nThe system returned the following error:= '];
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			message.push(error.getDetails());
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			message.push(error.toString());
		}
		referralData.message = message.join('');

		// delete the created patient record
		if (generalFunctions_global.isEmpty(patientId) === false) {
			nlapiDeleteRecord('customer', patientId);
		}
		return referralData;
	}
}
function isEmpty (stValue) {

	if (isNullOrUndefined(stValue) === true) {
		return true;
	}

	if ( (typeof stValue != 'string') && (getObjectName(stValue) != 'String')) {
		throw nlapiCreateError('10000', 'isEmpty should be passed a string value.  The data type passed is ' + typeof stValue + ' whose class name is ' + getObjectName(stValue));
	}

	if (stValue.length == 0) {
		return true;
	}

	return false;
}
function isNullOrUndefined (value) {

	if (value === null) {
		return true;
	}

	if (value === undefined) {
		return true;
	}

	return false;
}
function getObjectName (object) {

	if (isNullOrUndefined(object) === true) {
		return object;
	}

	return /(\w+)\(/.exec(object.constructor.toString())[1];
}
/**
 * @param {nlobjField}
 *            prescriberField
 * @param prescriberId
 * @returns
 */
function getPracticeNames (prescriberField , prescriberId) {

	var funcName = 'getPracticeNames';

	try {
		scriptLogger_global.audit(funcName, '-------------------- Searching Prescribers --------------------');

		var prescriberData = nlapiLookupField('partner', prescriberId, ['parent', 'entityid', 'firstname', 'lastname']);
		scriptLogger_global.debug(funcName, ['Logged in User Data:= ', JSON.stringify(prescriberData)].join(''));

		//		var prescriberName = [prescriberData.entityid, prescriberData.firstname, prescriberData.lastname].join(' ');
		//		scriptLogger_global.debug(funcName, ['Prescriber Name:= ', prescriberName].join(''));
		//
		//		prescriberField.addSelectOption(prescriberId, prescriberName);

		//		if (generalFunctions_global.isEmpty(prescriberData.parent) === false) {
		var searchFilter = [];
		searchFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		//			searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', prescriberData.parent).setOr(true));
		//		searchFilter.push(new nlobjSearchFilter('parent', null, 'anyof', prescriberData.parent));
		searchFilter.push(new nlobjSearchFilter('parent', null, 'anyof', prescriberId));

		var searchColumn = [];
		searchColumn.push(new nlobjSearchColumn('formulatext').setFormula('{altname}'));

		var results = nlapiSearchRecord('partner', null, searchFilter, searchColumn);
		scriptLogger_global.debug(funcName, ['Search Results:= ', JSON.stringify(results)].join(''));

		if (generalFunctions_global.numRows(results) > 0) {
			for ( var x = 0; x < results.length; x++) {
				var id = results[x].getId();
				var name = results[x].getValue('formulatext');

				if (name.indexOf(':') != -1) {
					var nameArray = name.split(':');
					name = nameArray.pop();
				}

				if (id != prescriberId) {
					prescriberField.addSelectOption(id, name);
				}

			}
		}
		//		}

		return prescriberField;
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
		}
		prescriberField.addSelectOption('', '');
		return prescriberField;
	}
}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 *
 * @appliedtorecord recordType
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function fieldChanged_nameChange (type , name , linenum) {

	var funcName = 'fieldChanged_nameChange';

	try {

		if (name != 'custpage_prescriber_name') {
			return true;
		}

		var partnerInternalId = nlapiGetFieldValue(name);
		if (isEmpty(partnerInternalId) === false) {
			nlapiLogExecution('DEBUG', funcName, ['Logged Partner Internal Id:= ', partnerInternalId].join(''));

			// save the name in the hidden field
			var partnerName = nlapiGetFieldText(name);
			nlapiSetFieldValue('custpage_prescriber_name_text', partnerName, false, true);

			// get the relevant partner information
			var partnerData = getPartnerData(partnerInternalId);
			nlapiLogExecution('DEBUG', funcName, ['Partner Data:= ', JSON.stringify(partnerData)].join(''));

			for ( var field in partnerData) {
				if (field != name) {
					var data = partnerData[field];

					if (isEmpty(data) === false) {
						// set the field values
						nlapiSetFieldValue(field, data, false, true);
					}
				}
			}
		}
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 *
 * @appliedtorecord recordType
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @param {nlobjForm}
 *            form NetSuite form object
 * @returns {Void}
 */
function beforeLoad_genPdfButton (type , form) {

	var funcName = 'beforeLoad_genPdfButton';

	if (type != 'view') {
		return true;
	}
	// disable DEBUG logs if Script Log Level is not Debug
	if (executionContext_global.getLogLevel() == 'DEBUG') {
		scriptLogger_global.enableDebug();
	}

	try {
		scriptLogger_global.audit(funcName, '==================== BEFORE LOAD EVENT START ====================');
		stopWatch_global.start();

		scriptLogger_global.audit(funcName, '-------------------- Generate Generate PDF button. --------------------');

		var patientId = nlapiGetFieldValue('company');
		var partnerInternalId = nlapiGetFieldValue('custevent_bio_physician');

		var scriptCall = ["generatePDF('", nlapiGetRecordId(), "','", patientId, "','", partnerInternalId, "')"].join('');
		scriptLogger_global.debug(funcName, ['Script Call:= ', scriptCall].join(''));

		form.setScript('customscript_arestin_validation');
		form.addButton('custpage_gen_pdf', 'Generate Neutrasal PDF', scriptCall);

		stopWatch_global.stop();
		scriptLogger_global.audit(funcName, ['=================== BEFORE LOAD EVENT END ', '[Running Time:= ', stopWatch_global.duration(), '] ==================='].join(''));
	} catch (error) {
		if (error instanceof nlobjError) {
			nlapiLogExecution('ERROR', funcName, [error.getCode(), ': ', error.getDetails()].join(''));
			throw error;
		} else {
			nlapiLogExecution('ERROR', funcName, error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
function generatePDF (recordId , patientId , partnerInternalId) {

	var funcName = 'generatePDF';

	var strSuiteletUrl = nlapiResolveURL('SUITELET', 'customscript_bs_gen_pdf', 'customdeploy_bs_gen_pdf');
	var strParam = ['&patientid=', patientId, '&referralid=', recordId, '&prescriberid=', partnerInternalId, '&button=T'].join('');

	var strFormUrl = [strSuiteletUrl, strParam].join('');

	window.location.replace(strFormUrl);
}
function decryptField (value) {

	var funcName = 'decryptField';

	// get the webservice url from the script
	var webServiceURL = executionContext_global.getSetting('script', 'custscript_webservice_url');
	scriptLogger_global.audit(funcName, ['[Script Parameter]Web Service URL:= ', webServiceURL].join(''));

	var decryptionRequest = createFieldDecryptionRequest(value);
	scriptLogger_global.debug(funcName, ['Value:= ', nlapiEscapeXML(decryptionRequest)].join(''));

	var decryptedValue = sendFieldDecryptionRequest(webServiceURL, decryptionRequest);

	if (decryptedValue.indexOf("'") == 0) {
		decryptedValue = decryptedValue.substr(1);
	}

	return decryptedValue;
}
function getCaseInsurance (referralId) {

	var searchFilters = [];
	searchFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
	searchFilters.push(new nlobjSearchFilter('custrecord_bio_fax_case', null, 'anyof', referralId));

	var searchColumns = [];
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_fax_insur_primary'));// Medical Insurance
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_fax_insur_primary_bpm'));// Insured Name
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_fax_insur_primary_bpm_id'));//Drug Cardholder Id
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_fax_insur_id_no'));// Policy Number
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_fax_insur_bpm_group'));// Group Number
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_fax_insur_grp_no')); // Group Number
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_primary_pbm_phone')); // Phone Number
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_primary_ins_phone')); // Member Services Phone
	searchColumns.push(new nlobjSearchColumn('custrecordpharmacy_bin_number')); // Bin #
	searchColumns.push(new nlobjSearchColumn('custrecord_bio_fax_insur_pbm_npi_cntrl')); // Rx PCN #
	searchColumns.push(new nlobjSearchColumn('custrecordnumb_vial')); // Quantity

	var result = nlapiSearchRecord('customrecord3', null, searchFilters, searchColumns);

	if (generalFunctions_global.numRows(result) > 0) {
		var caseInsurance = {};
		caseInsurance.custpage_primary_med_insurance = result[0].getText('custrecord_bio_fax_insur_primary');// Medical Insurance
		caseInsurance.custpage_primary_pbm_name = result[0].getText('custrecord_bio_fax_insur_primary_bpm');// Insured Name
		caseInsurance.custpage_pbm_cardholder_id = result[0].getValue('custrecord_bio_fax_insur_primary_bpm_id');//Drug Cardholder Id
		caseInsurance.custpage_medical_id_number = result[0].getValue('custrecord_bio_fax_insur_id_no');// Policy Number
		caseInsurance.custpage_rx_group = result[0].getValue('custrecord_bio_fax_insur_bpm_group'); // Group Number
		caseInsurance.custpage_medical_group_number = result[0].getValue('custrecord_bio_fax_insur_grp_no');// Group Number
		caseInsurance.custpage_pbm_phone_no = result[0].getValue('custrecord_bio_primary_pbm_phone');// Phone Number
		caseInsurance.custpage_primary_member_phone = result[0].getValue('custrecord_bio_primary_ins_phone');// Member Services Phone
		caseInsurance.custpage_rx_bin = result[0].getValue('custrecordpharmacy_bin_number');// Bin #
		caseInsurance.custpage_rx_pcn = result[0].getValue('custrecord_bio_fax_insur_pbm_npi_cntrl'); // Rx PCN #
		caseInsurance.quantity = result[0].getValue('custrecordnumb_vial'); // Quantity
	}

	return caseInsurance;
}
function getEnteredPrescriberData (request) {

	// get the Prescriber Data entered in the Referral form

	var prescriberFields = ['custpage_prescriber_name', 'custpage_practice_name', 'custpage_office_contact_name', 'custpage_office_contact_email', 'custpage_pref_form_com', 'custpage_com_phone', 'custpage_com_fax', 'custpage_deliver_to_address', 'custpage_deliver_to_city', 'custpage_deliver_to_state', 'custpage_deliver_to_zip', 'custpage_prescriber_npi', 'custpage_prescriber_name_text'];

	var prescriberData = {};
	// get the Prescriber data entered on the from
	for ( var index in prescriberFields) {

		var fieldName = prescriberFields[index];

		prescriberData[fieldName] = request.getParameter(fieldName);
	}

	return prescriberData;

}
function encryptField (field , value) {

	var funcName = 'encryptField';

	scriptLogger_global.debug(funcName, ['field:= ', field, ' value:= ', value].join(''));

	// get the webservice url from the script
	var webServiceURL = executionContext_global.getSetting('script', 'custscript_webservice_url');
	scriptLogger_global.audit(funcName, ['[Script Parameter]Web Service URL:= ', webServiceURL].join(''));

	var encryptionRequest = createFieldEncryptionRequest(value, field);

	var encryptedValue = sendFieldEncryptionRequest(webServiceURL, encryptionRequest, value);
	scriptLogger_global.debug(funcName, ['Value:= ', encryptedValue].join(''));

	if (generalFunctions_global.isEmpty(encryptedValue) === true) {
		return value;
	} else {
		return encryptedValue;
	}
}

function pageInitHideMenus()
{
	var quickMenu = parent.document.getElementById("ns-header-quick-menu"); if (quickMenu) { quickMenu.style.display="none"; };
	var help = parent.document.getElementsByClassName("ns-help")[0]; if (help) { help.style.display="none"; };
	var home = parent.document.getElementsByClassName("ns-menu uir-menu-home")[0]; if (home) { home.style.display="none"; };
	var shortcuts = parent.document.getElementsByClassName("ns-menu uir-menu-shortcuts")[0]; if (shortcuts) { shortcuts.style.display="none"; };
	var recent = parent.document.getElementsByClassName("ns-menu uir-menu-recent")[0]; if (recent) { recent.style.display="none"; };
	var menuHeaders = parent.document.getElementsByClassName("ns-menuitem ns-header");
	var dataTittle = "";
	for (var i = 0; i < menuHeaders.length; i++) {
		dataTittle = menuHeaders[i].getAttribute("data-title");
		if( dataTittle == "Setup" || dataTittle == "Support") {
			menuHeaders[i].style.display="none";
		}
	}
	var head = parent.document.head || parent.document.getElementsByTagName("head")[0];
	var styleNode = parent.document.createElement("style");
	styleNode.type = "text/css";
	if(!!(window.attachEvent && !window.opera)) {
		styleNode.styleSheet.cssText = "span { color: rgb(255, 0, 0); }";
	} else {
		var styleText = parent.document.createTextNode(".uir-menuitem-viewall{ display: none!important }");
		styleNode.appendChild(styleText);
	}
	parent.document.getElementsByTagName("head")[0].appendChild(styleNode);
}
